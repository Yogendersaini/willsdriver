//
//  EditProfileViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 16/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var tableViewProfile: UITableView!

    let arrCellTitles = ["User Name", "Phone Number", "Email", "Gender", "Birthday"]
    var objEditProfileModel = ProfileDatModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTableviewCells()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localization()
        
        self.navigationController?.navigationBar.isHidden = true
        
    }

    static func getInstance() -> EditProfileViewController {
        
        let storyboard = UIStoryboard(name: "Profile",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
    }
    func localization() {
        
//        self.doneBtn.setTitle((L102Language.AMLocalizedString(key: "done", value: "")), for: .normal)
//       // self.navigationItem.title = (L102Language.AMLocalizedString(key: "profile", value: ""))
//        self.nameTF.lblTitle.text = (L102Language.AMLocalizedString(key: "name", value: ""))
//        self.surNameTF.lblTitle.text = (L102Language.AMLocalizedString(key: "surname", value: ""))
//        self.emailTF.lblTitle.text = (L102Language.AMLocalizedString(key: "email_address", value: ""))

    }
   
    @IBAction func backBtnActinos(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func backDoneActinos(_ sender: UIButton) {
//        navigationController?.popViewController(animated: true)
        
        // getting tabel cell value for update in update profile api
                
        var findNil = false
        var arrVAlues = [String]()
        
        // getting profile image
        let cellImage = (tableViewProfile.cellForRow(at: IndexPath(row: 0, section: 0)) as! TbCellEditProfile).imgData
        let firstName = (tableViewProfile.cellForRow(at: IndexPath(row: 0, section: 0)) as! TbCellEditProfile).txtFirstName.text as? String ?? ""
        let LastName = (tableViewProfile.cellForRow(at: IndexPath(row: 0, section: 0)) as! TbCellEditProfile).txtLastName.text as? String ?? ""
        
        if firstName == "" || LastName == "" {
            self.view.makeToast("Can't leave blank first name or last name")
            return
        }
        
        for i in 0...self.arrCellTitles.count-1 {
            let indPh = IndexPath(row: i, section: 1)
            let cell = tableViewProfile.cellForRow(at: indPh) as! ProfileDetailsTBcell
        
            let value = cell.lblValue.text?.trimmingCharacters(in: .whitespaces)
            if value == "" {
                print("Blamnl vale", value)
                self.view.makeToast("Can't leave blank profile data")
                findNil = true
                break
            } else {
                print("Not blank", value)
                arrVAlues.append(value!)
            }
        }
        print(arrVAlues)
        self.apiCAlling_GetUpdateProfile(imgData: cellImage, email: arrVAlues[2], phone: arrVAlues[1], gender: arrVAlues[3], dob: arrVAlues[4], username: arrVAlues[0], first_name: firstName, last_name: LastName)
    }


}
extension EditProfileViewController : UITableViewDelegate, UITableViewDataSource {

func profileTableviewCells(){
    
    tableViewProfile.register(UINib(nibName: "TbCellEditProfile", bundle: nil), forCellReuseIdentifier: "TbCellEditProfile")
    
    tableViewProfile.register(UINib(nibName: "ProfileDetailsTBcell", bundle: nil), forCellReuseIdentifier: "ProfileDetailsTBcell")

    tableViewProfile.register(UINib(nibName: "HeaderTbCell", bundle: nil), forCellReuseIdentifier: "HeaderTbCell")
}
    

func numberOfSections(in tableView: UITableView) -> Int {
    return 2
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
        return 2
    } else {
        return arrCellTitles.count
    }
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
//    if indexPath.section == 0 {
//        return 190
//    } else {
//        return  74
//    }
    
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    if indexPath.section == 0 {
        if indexPath.row == 0 {
            let distanceCell = tableViewProfile.dequeueReusableCell(withIdentifier: "TbCellEditProfile", for: indexPath) as! TbCellEditProfile
            distanceCell.selectionStyle = .none
            distanceCell.signUpViewCotnt = self
            distanceCell.setupData(obj: self.objEditProfileModel)
            return distanceCell
        } else {
            let headerTbCell = tableViewProfile.dequeueReusableCell(withIdentifier: "HeaderTbCell", for: indexPath) as! HeaderTbCell
            headerTbCell.selectionStyle = .none
            return headerTbCell
        }
        } else  {
            let earnCell = tableViewProfile.dequeueReusableCell(withIdentifier: "ProfileDetailsTBcell", for: indexPath) as! ProfileDetailsTBcell
            earnCell.selectionStyle = .none
            earnCell.setupData(obj: self.objEditProfileModel, arrTitles: self.arrCellTitles, index: indexPath.row, isForEdit: true, vc: self)
            return earnCell
        }
    }
}
extension EditProfileViewController {
    //MARK:- api work
    
    func apiCAlling_GetUpdateProfile( imgData : NSData, email : String, phone : String, gender : String, dob : String, username : String,  first_name : String, last_name : String) {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "email" : email, "phone" : phone, "gender" : gender, "dob" : dob, "username" : username, "first_name" : first_name, "last_name" : last_name]
                
        AppHelper.apiCallingWithImage(apiName: GetUpdateProfile, param: param, viewCont: self, imgData: imgData, successDict: { (successDict) in
            print(successDict)
            
            self.navigationController?.popViewController(animated: true)

            NotificationCenter.default.post(name: NSNotification.Name("isProfileUpdated"), object: nil, userInfo: nil)

            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
        }, messageID: { (messageID, message)  in
            UIAlertController.showAlert(vc: self, title: "", message: message)

        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
}

//
//  ProfileTableViewCell.swift
//  Trekk
//
//  Created by Yogender Saini on 2/24/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData(obj : ProfileDatModel){
        self.lblProfileName.text = obj.full_name
        self.imgProfile.sd_setImage(with: URL(string: obj.image), placeholderImage: UIImage(named: "profile_pic"))
    }
    
}

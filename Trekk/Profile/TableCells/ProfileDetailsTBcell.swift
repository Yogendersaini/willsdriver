//
//  ProfileDetailsTBcell.swift
//  Trekk
//
//  Created by Yogender Saini on 2/24/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class ProfileDetailsTBcell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblValue : UITextField!
    @IBOutlet weak var btnGender : UIButton!
    var viewController = UIViewController()
    var datePicker: UIDatePicker?

    override func awakeFromNib() {
        super.awakeFromNib()
        lblValue.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData(obj : ProfileDatModel, arrTitles : [String], index : Int, isForEdit : Bool, vc : UIViewController){
        viewController = vc
        self.lblTitle.text = arrTitles[index]
        var newINdex = 0
        if isForEdit {
            newINdex = index
            self.lblValue.tag = index
            self.lblTitle.text = arrTitles[index]
            self.lblValue.isUserInteractionEnabled = true
        } else {
            newINdex = index + 1
            self.lblTitle.text = arrTitles[index]
            self.lblValue.isUserInteractionEnabled = false
        }
        
        switch newINdex {
        case 0:
            self.lblValue.text = obj.username
            self.btnGender.isHidden = true
        case 1:
            self.lblValue.text = obj.phone
            self.btnGender.isHidden = true
        case 2:
            self.lblValue.text = obj.email
            self.btnGender.isHidden = true
        case 3:
            self.lblValue.text = obj.gender
            if isForEdit {
                self.btnGender.isHidden = false
            } else {
                self.btnGender.isHidden = true
            }
        case 4:
            self.lblValue.text = obj.dob
            self.btnGender.isHidden = true
        case 5:
            self.lblValue.text = obj.dob    // because both profila and edit profile useing same cell.
            self.btnGender.isHidden = true
        default:
            self.lblTitle.text = ""
        }
    }
       
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if textField.tag == 3 {
            textField.resignFirstResponder()
            self.showAlertButtonTapped(self.btnGender)
        }
        
        //Add DatePicker as inputView
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 300, height: 216))
        self.datePicker?.datePickerMode = .date
        let oneHourAfter = Date(timeIntervalSinceNow:-86400)
        self.datePicker?.maximumDate = oneHourAfter

                
        if textField.tag == 4 {
            self.lblValue.inputView = self.datePicker
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 4 {
            let strDate =  "\(self.datePicker!.date)"
            self.lblValue.text = (strDate as! NSString).substring(to: 11)
        }
    }

    
    
    @IBAction func showAlertButtonTapped(_ sender: UIButton) {

        let alert = UIAlertController(title: "Gender", message: "", preferredStyle: UIAlertController.Style.alert)
        
        let actionMale = UIAlertAction(title: "Male", style: .default) { (action) in
            self.lblValue.text = "male"
        }
        let actionFemale = UIAlertAction(title: "Female", style: .default) { (action) in
            self.lblValue.text = "female"
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .default) { (action) in
        }

        alert.addAction(actionMale)
        alert.addAction(actionFemale)
        alert.addAction(actionCancel)
    
        viewController.present(alert, animated: true, completion: nil)
    }
    
    

    
}


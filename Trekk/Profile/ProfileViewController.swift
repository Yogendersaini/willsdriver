//
//  ProfileViewController.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 27/03/20.
//  Copyright © 2020 Harjit Singh Mac. All rights reserved.
//

import UIKit
import SDWebImage
import AssetsLibrary
import Photos
import AKImageCropperView


class ProfileViewController: UIViewController {
    
    @IBOutlet weak var tableViewProfile: UITableView!

    let arrCellTitles = ["Phone Number", "Email", "Gender", "Birthday"]
    var objProfileModel = ProfileDatModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTableviewCells()
        self.apiCalling_GetProfile()
        NotificationCenter.default.addObserver(self, selector: #selector(apiCalling_GetProfile), name: NSNotification.Name("isProfileUpdated"), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localization()
        
        self.navigationController?.navigationBar.isHidden = true
        
    }

    static func getInstance() -> ProfileViewController {
        
        let storyboard = UIStoryboard(name: "Profile",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
    }
    func localization() {
        
//        self.doneBtn.setTitle((L102Language.AMLocalizedString(key: "done", value: "")), for: .normal)
//       // self.navigationItem.title = (L102Language.AMLocalizedString(key: "profile", value: ""))
//        self.nameTF.lblTitle.text = (L102Language.AMLocalizedString(key: "name", value: ""))
//        self.surNameTF.lblTitle.text = (L102Language.AMLocalizedString(key: "surname", value: ""))
//        self.emailTF.lblTitle.text = (L102Language.AMLocalizedString(key: "email_address", value: ""))

    }
   
    
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    @IBAction func btnEditActinos(_ sender: UIButton) {
        let vc = EditProfileViewController.getInstance()
        vc.objEditProfileModel = self.objProfileModel
        navigationController?.pushViewController(vc, animated: true)
    }

    
}
extension ProfileViewController : UITableViewDelegate, UITableViewDataSource {

func profileTableviewCells(){
    
    tableViewProfile.register(UINib(nibName: "ProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
    
    tableViewProfile.register(UINib(nibName: "ProfileDetailsTBcell", bundle: nil), forCellReuseIdentifier: "ProfileDetailsTBcell")

    tableViewProfile.register(UINib(nibName: "HeaderTbCell", bundle: nil), forCellReuseIdentifier: "HeaderTbCell")
}
    

func numberOfSections(in tableView: UITableView) -> Int {
    return 2
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
        return 2
    } else {
        return self.arrCellTitles.count
    }
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
//    if indexPath.section == 0 {
//        return 190
//    } else {
//        return  74
//    }
    
}
func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    if indexPath.section == 0 {
        if indexPath.row == 0 {
            let distanceCell = tableViewProfile.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            distanceCell.selectionStyle = .none
            distanceCell.setupData(obj: self.objProfileModel)
            return distanceCell
        } else {
            let headerTbCell = tableViewProfile.dequeueReusableCell(withIdentifier: "HeaderTbCell", for: indexPath) as! HeaderTbCell
            headerTbCell.selectionStyle = .none
            return headerTbCell
        }
        } else  {
            let earnCell = tableViewProfile.dequeueReusableCell(withIdentifier: "ProfileDetailsTBcell", for: indexPath) as! ProfileDetailsTBcell
            earnCell.selectionStyle = .none
            earnCell.setupData(obj: self.objProfileModel, arrTitles: self.arrCellTitles, index: indexPath.row,isForEdit: false,vc: self)
                        
            return earnCell
        }
    
}
}

extension ProfileViewController {
    // MARK:- Apiwork
    
    @objc func apiCalling_GetProfile() {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken()]
        
        AppHelper.apiCallingForDict(apiName: GetProfile, param: param, viewCont: self, successDict: { (successDict, jsonDict) in
            print("Get proifle callse")
            print(successDict)
            self.objProfileModel = ProfileDatModel()
            self.objProfileModel = ProfileDatModel.init(with: successDict)
            self.tableViewProfile.reloadData()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
        }, messageID: { (messageID, message) in
            UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
        

    /*
     {
         data =     {
             "dial_code" = "+91";
             dob = "01 Jun, 2020";
             email = "driver1@gmail.com";
             "first_name" = Test;
             "full_name" = Driver1;
             gender = male;
             id = 1;
             image = "http://intellisensetechnologies.com/smartcab/uploads/images/5eb3e217768px-User_icon_2.svg.png";
             "is_approved" = 1;
             "is_verified" = 1;
             lang = "";
             "last_name" = Driver;
             phone = 9928791990;
             token = 7e3edd43eb73a27d389bd233b8b58710;
             username = Test;
         };
         "debug_id" = 7b5bfa93a33fe87dd608719ab84fb9c9;
         message = "Success (RM_00092)";
         "message_id" = 93;
         status = 1;
     }
     */


}

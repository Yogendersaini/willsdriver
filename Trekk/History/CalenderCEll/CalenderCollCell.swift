//
//  CalenderCollCell.swift
//  Trekk
//
//  Created by Yogender Saini on 09/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class CalenderCollCell: UICollectionViewCell {

    @IBOutlet weak var viewBakg : UIView!
    @IBOutlet weak var lblDayNumb : UILabel!
    @IBOutlet weak var lblDay : UILabel!
    var month = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setDate(crrDate : String) {
        self.lblDay.text = (crrDate as? NSString)!.substring(to: 3)
        let daystr = (crrDate as? NSString)!.substring(from: 4)
        self.lblDayNumb.text =  (daystr as? NSString)!.substring(to: 2)
        
        self.month = (crrDate as? NSString)!.substring(from: 7)
       // print("Month:---", self.month)
    }

    func makeSelected(selected : Bool) {
        if selected {
            viewBakg.layer.borderWidth = 1
            viewBakg.layer.borderColor = UIColor.init(red: 255, green: 135/255, blue: 0, alpha: 1).cgColor
            lblDay.textColor = UIColor.init(red: 255, green: 135/255, blue: 0, alpha: 1)
            lblDayNumb.textColor = UIColor.init(red: 255, green: 135/255, blue: 0, alpha: 1)
            viewBakg.layer.backgroundColor = UIColor.white.cgColor
            
        } else {
            viewBakg.layer.borderWidth = 0
            viewBakg.layer.borderColor = UIColor.clear.cgColor
            lblDay.textColor = UIColor.lightGray
            lblDayNumb.textColor = UIColor.lightGray
            viewBakg.layer.backgroundColor = UIColor.init(red: 242/255, green: 242/255, blue: 247/255, alpha: 1).cgColor
        }
    }
    
    
}



//
//  HistoryDetailsTbCell.swift
//  Trekk
//
//  Created by Yogender Saini on 09/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class HistoryDetailsTbCell: UITableViewCell {

    @IBOutlet weak var viewBackg : UIView!
    @IBOutlet weak var viewBackg1 : UIView!
    @IBOutlet weak var viewBackg2 : UIView!
    @IBOutlet weak var viewBackg3 : UIView!
   
    @IBOutlet weak var lblDriveNmae : UILabel!
    @IBOutlet weak var lblPickLoc : UILabel!
    @IBOutlet weak var lblDropLoc : UILabel!
    @IBOutlet weak var lblDistance : UILabel!
    @IBOutlet weak var lblTotalBill : UILabel!
    @IBOutlet weak var imgProfile : UIImageView!
//    @IBOutlet weak var lblDriveNmae : UILabel!
//    @IBOutlet weak var lblDriveNmae : UILabel!
    
    var viewCont = UIViewController()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
     //   viewBackg.dropShadowT(scale: true, color: UIColor.init(red: 233/255, green: 234/255, blue: 236/255, alpha: 1))

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setuViews(viewCont : UIViewController)  {
        self.viewCont = viewCont
        if self.viewCont.isKind(of: HistoryViewController.self) {
            viewBackg1.layer.backgroundColor = UIColor.white.cgColor
            viewBackg2.layer.backgroundColor = UIColor.white.cgColor
            viewBackg3.isHidden = true
        }
        if self.viewCont.isKind(of: ReceiptViewController.self) {
            viewBackg1.layer.backgroundColor = UIColor.init(red: 242/255, green: 242/255, blue: 247/255, alpha: 1).cgColor
            viewBackg2.layer.backgroundColor = UIColor.init(red: 242/255, green: 242/255, blue: 247/255, alpha: 1).cgColor
            viewBackg3.isHidden = false
        }
    }
    
    func setuData(obj  : HistoryDataModel)  {
        self.lblDriveNmae.text = obj.rider_name
        self.lblDropLoc.text = obj.end_location
        self.lblDistance.text = obj.distance
        self.lblTotalBill.text = obj.total_bill
        self.lblPickLoc.text =  obj.start_location
        self.imgProfile.sd_setImage(with: URL(string: obj.rider_image), placeholderImage: UIImage(named: "profile_pic"))

    }
    
}


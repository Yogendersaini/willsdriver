//
//  HistHeaderTbCell.swift
//  Trekk
//
//  Created by Yogender Saini on 08/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class HistHeaderTbCell: UITableViewCell {

    @IBOutlet weak var lblTotalJobs : UILabel!
    @IBOutlet weak var lblTotlaEarnings : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData(jobs : String, earning : String) {
        self.lblTotalJobs.text = jobs
        self.lblTotlaEarnings.text = earning
    }
    
}

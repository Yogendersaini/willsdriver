//
//  ReceiptViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 09/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class ReceiptViewController: UIViewController {
   
    @IBOutlet weak var tblHistory : UITableView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableviewCells()
    }
    
    static func getInstance() -> ReceiptViewController {
        let storyboard = UIStoryboard(name: "History",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ReceiptViewController") as! ReceiptViewController
        
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
         }

}

extension ReceiptViewController : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        
        tblHistory.register(UINib(nibName: "NotesTblCell", bundle: nil), forCellReuseIdentifier: "NotesTblCell")
        tblHistory.register(UINib(nibName: "HistoryDetailsTbCell", bundle: nil), forCellReuseIdentifier: "HistoryDetailsTbCell")
        tblHistory.register(UINib(nibName: "TripFairTbCell", bundle: nil), forCellReuseIdentifier: "TripFairTbCell")

        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
            let historyDetailsTbCell = tblHistory.dequeueReusableCell(withIdentifier: "HistoryDetailsTbCell", for: indexPath) as! HistoryDetailsTbCell
            historyDetailsTbCell.selectionStyle = .none
            historyDetailsTbCell.setuViews(viewCont: self)
            return historyDetailsTbCell

        } else  if indexPath.row == 1 {
            let notesTblCell = tblHistory.dequeueReusableCell(withIdentifier: "NotesTblCell", for: indexPath) as! NotesTblCell
            notesTblCell.selectionStyle = .none
            return notesTblCell
        } else {
            let tripFairTbCell = tblHistory.dequeueReusableCell(withIdentifier: "TripFairTbCell", for: indexPath) as! TripFairTbCell
            tripFairTbCell.selectionStyle = .none
            return tripFairTbCell
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


//
//  HistoryViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 08/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    @IBOutlet weak var tblHistory : UITableView!
    @IBOutlet weak var collHistory : UICollectionView!
    @IBOutlet weak var lblMonthName : UILabel!
    @IBOutlet weak var lblNodata : UILabel!
    @IBOutlet weak var btnBack: UIButton!

    
    var arrayOfDates = NSArray()
    var arrayOfParamDates = NSArray()
    var selectedItem = 0
    var selectedDateForHistory = ""
    var totalJobs = ""
    var earnings = ""
    var pageNo = 1
    var isCallForNextPage = false

    var objHistDataModel = [HistoryDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableviewCells()
        registerCollectionCell()
        self.arrayOfDates = self.getArrayOfDates()
        self.arrayOfParamDates = self.getArrayOfDates1()
        let  str = "\(Date())"
        let substt = (str as! NSString).substring(to: 10)
        self.apiCalling_GetHistoryList(historyDate: substt,pageNo:  "\(self.pageNo)")
        self.loclizatino()
    }
    
    func loclizatino()  {
        self.btnBack.setTitle((L102Language.AMLocalizedString(key: "History_MenuKey", value: "")), for: .normal)
        self.lblNodata.text = (L102Language.AMLocalizedString(key: "No_History_Found", value: ""))
    }
    static func getInstance() -> HistoryViewController {
        let storyboard = UIStoryboard(name: "History",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
        
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
         }

}

extension HistoryViewController : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        
        tblHistory.register(UINib(nibName: "HistHeaderTbCell", bundle: nil), forCellReuseIdentifier: "HistHeaderTbCell")
        tblHistory.register(UINib(nibName: "HistoryDetailsTbCell", bundle: nil), forCellReuseIdentifier: "HistoryDetailsTbCell")
        
        self.perform(#selector(self.scrollCollectionView), with: nil, afterDelay: 1)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return self.objHistDataModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.section == 0 {
            let histHeaderTbCell = tblHistory.dequeueReusableCell(withIdentifier: "HistHeaderTbCell", for: indexPath) as! HistHeaderTbCell
            histHeaderTbCell.selectionStyle = .none
            histHeaderTbCell.setupData(jobs: self.totalJobs, earning: self.earnings)
            return histHeaderTbCell

        } else {
            let historyDetailsTbCell = tblHistory.dequeueReusableCell(withIdentifier: "HistoryDetailsTbCell", for: indexPath) as! HistoryDetailsTbCell
            historyDetailsTbCell.selectionStyle = .none
            historyDetailsTbCell.setuViews(viewCont: self)
            historyDetailsTbCell.setuData(obj: self.objHistDataModel[indexPath.row])
            
            if (self.objHistDataModel.count - 1) == indexPath.row {
                self.pageNo = self.pageNo + 1
                if self.isCallForNextPage {
                self.apiCalling_GetHistoryList(historyDate: self.selectedDateForHistory,pageNo:  "\(self.pageNo)")
                }
            }
            return historyDetailsTbCell

        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let wVC = ReceiptViewController.getInstance()
//        self.navigationController?.pushViewController(wVC, animated: true)
    }
}



extension HistoryViewController : UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout {
    
    func registerCollectionCell() {
        let firstNib = UINib(nibName: "CalenderCollCell", bundle: nil)
        collHistory.register(firstNib, forCellWithReuseIdentifier: "CalenderCollCell")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayOfDates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collHistory.dequeueReusableCell(withReuseIdentifier: "CalenderCollCell", for: indexPath) as! CalenderCollCell
        cell.setDate(crrDate: self.arrayOfDates[indexPath.row] as! String)
        
        if self.selectedItem == indexPath.row {
            cell.makeSelected(selected: true)
        } else {
            cell.makeSelected(selected: false)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 45, height: 65)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        self.selectedItem = indexPath.row
        self.collHistory.reloadData()
        self.objHistDataModel.removeAll()
        self.pageNo = 1
        self.selectedDateForHistory = self.arrayOfParamDates[self.selectedItem] as! String
        print(self.arrayOfParamDates[self.selectedItem] as! String)
        self.apiCalling_GetHistoryList(historyDate: self.arrayOfParamDates[self.selectedItem] as! String,pageNo:  "\(self.pageNo)")
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
                
        if let cell = cell as? CalenderCollCell {
            let month = cell.month
            self.lblMonthName.text = (L102Language.AMLocalizedString(key: "\(month)", value: ""))
            
        }
    }
    
    

//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 1.0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout
//        collectionViewLayout: UICollectionViewLayout,
//                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 1.0
//    }
    
}

extension HistoryViewController {
    
    func getArrayOfDates() -> NSArray {
        let numberOfDays: Int = 365
        var dateComponent = DateComponents()
        
        dateComponent.day = 1
        dateComponent.year = -1
        
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        let startDate = futureDate!
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "EEE dd/MMMM"
        let calendar = Calendar.current
        var offset = DateComponents()
        var dates: [Any] = [formatter.string(from: startDate)]
        for i in 1..<numberOfDays {
            offset.day = i
            let nextDay: Date? = calendar.date(byAdding: offset, to: startDate)
            let nextDayString = formatter.string(from: nextDay!)
            dates.append(nextDayString)
        }
        self.selectedItem = dates.count-1
        return dates as NSArray
    }
    
    func getArrayOfDates1() -> NSArray {
        let numberOfDays: Int = 365
        var dateComponent = DateComponents()
        
        dateComponent.day = 1
        dateComponent.year = -1
        
        let futureDate = Calendar.current.date(byAdding: dateComponent, to: Date())
        let startDate = futureDate!
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
//        2021-05-07
        let calendar = Calendar.current
        var offset = DateComponents()
        var dates: [Any] = [formatter.string(from: startDate)]
        for i in 1..<numberOfDays {
            offset.day = i
            let nextDay: Date? = calendar.date(byAdding: offset, to: startDate)
            let nextDayString = formatter.string(from: nextDay!)
            dates.append(nextDayString)
        }
        self.selectedItem = dates.count-1
        return dates as NSArray
    }
    
    
    
    @objc func scrollCollectionView() {
        DispatchQueue.main.async {
            self.collHistory.scrollToItem(at: IndexPath(item: self.arrayOfDates.count-1, section: 0), at: .right, animated: true)
        }
    }
}
extension HistoryViewController {
    // MARK:- Api work
    
    @objc func apiCalling_GetHistoryList(historyDate : String, pageNo : String) {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "date" : historyDate, "page" : pageNo, "rows" : "10"]
                        
        AppHelper.apiCallingForArray(apiName: GetHistoryList, param: param, viewCont: self, successArray: { (successArray, jsonDict) in
            print(successArray)

            if successArray.count >= 1{
            for i in 0...successArray.count-1 {
                self.objHistDataModel.append(HistoryDataModel(with: successArray[i]))
                self.lblNodata.isHidden=true
            }
                self.isCallForNextPage = true
            } else {
                self.isCallForNextPage = false
            }
            self.totalJobs = jsonDict["total_jobs"] as? String ?? ""
            self.earnings = jsonDict["total_earnings"] as? String ?? ""
            self.tblHistory.reloadData()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            self.lblNodata.isHidden=false
        }, messageID: { (messageID, message) in
            self.lblNodata.isHidden=false
            UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
            self.lblNodata.isHidden=false
        }
        
    }
        

}


//
//  TbCellInvite.swift
//  Trekk
//
//  Created by Yogender Saini on 10/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TbCellInvite: UITableViewCell {

    @IBOutlet weak var imgVw : UIImageView!
    @IBOutlet weak var lblNAmes : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
//    @IBAction func btnSelectAction (_ sender : UIButton){
//
//    }

    func btnSelectAction (val : Bool){
        if val {
            imgVw.image = UIImage(named: "radio_btn")
        } else {
            imgVw.image = UIImage(named: "Ellipse 30")
        }
        
    }

    
}

//
//  InviteFriendsViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 10/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class InviteFriendsViewController: UIViewController , UISearchBarDelegate{

    @IBOutlet weak var tblInvite : UITableView!
    @IBOutlet weak var namesSearch: UISearchBar!

    var searching = false
    var arrSelectedRows:[Int] = [0]
    var searchedNames = [String]()
    var namess = ["Rajesh","Yogesh", "Deepak", "Toshi", "bajesh","gogesh", "meepak", "soshi","hajesh","jogesh", "keepak", "doshi","vajesh","bogesh", "Rajesh","Yogesh", "Deepak", "Toshi", "bajesh","gogesh", "meepak", "soshi","hajesh","jogesh", "keepak", "doshi","vajesh","bogesh", "Rajesh","Yogesh", "Deepak", "Toshi", "bajesh","gogesh", "meepak", "soshi","hajesh","jogesh", "keepak", "doshi","vajesh","bogesh", "Rajesh","Yogesh", "Deepak", "Toshi", "bajesh","gogesh", "meepak", "soshi","hajesh","jogesh", "keepak", "doshi","vajesh","bogesh", "Rajesh","Yogesh", "Deepak", "Toshi", "bajesh","gogesh", "meepak", "soshi","hajesh","jogesh", "keepak", "doshi","vajesh","bogesh", ]

    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableviewCells()
        namesSearch.delegate = self
        
     //   namesSearch.showsCancelButton = true
    //    namesSearch.barTintColor = UIColor.blue
    //    namesSearch.tintColor = UIColor.red

        if let buttonItem = namesSearch.subviews.first?.subviews.last as? UIButton {
            buttonItem.setTitleColor(UIColor.red, for: .normal)
        }
        
    }

    static func getInstance() -> InviteFriendsViewController {
        let storyboard = UIStoryboard(name: "Invite",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InviteFriendsViewController") as! InviteFriendsViewController

    }

    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
    }


}

extension InviteFriendsViewController : UITableViewDelegate, UITableViewDataSource {

    func registerTableviewCells(){
        tblInvite.register(UINib(nibName: "TbCellInvite", bundle: nil), forCellReuseIdentifier: "TbCellInvite")
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching {
            return self.searchedNames.count
                } else {
                    return self.namess.count
                }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let histHeaderTbCell = tblInvite.dequeueReusableCell(withIdentifier: "TbCellInvite", for: indexPath) as! TbCellInvite
            histHeaderTbCell.selectionStyle = .none
        histHeaderTbCell.lblNAmes.text = namess[indexPath.row]

        if self.arrSelectedRows.contains(indexPath.row) {
            histHeaderTbCell.btnSelectAction(val: true)
        } else {
            histHeaderTbCell.btnSelectAction(val: false)
        }

        if searching {
                    histHeaderTbCell.lblNAmes.text = searchedNames[indexPath.row]
                } else {
                    histHeaderTbCell.lblNAmes.text = namess[indexPath.row]
                }

            return histHeaderTbCell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.arrSelectedRows.append(indexPath.row)
        self.tblInvite.reloadData()
    }
}
extension InviteFriendsViewController  {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedNames = namess.filter { $0.range(of: searchText, options: .caseInsensitive) != nil }
            searching = true
        
        if searchText == "" {
            searching = false
        }
        tblInvite.reloadData()
        }

        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searching = false
            searchBar.text = ""
            tblInvite.reloadData()
        }
    
    @IBAction func btnCancelACtion(_ sender : UIButton) {
        searching = false
        self.namesSearch.text = ""
//        searchBar.text = ""
        tblInvite.reloadData()
    }
}

//
//  InviteViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 2/24/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class InviteViewController: BaseVC {

    @IBOutlet weak var lblTitle: UILabel!

        @IBOutlet weak var lblMsg: UILabel!
        @IBOutlet weak var lblShareInviteCodeTitle: UILabel!
        @IBOutlet weak var btnShareNow: UIButton!
        @IBOutlet weak var btnBack: UIButton!

        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.isHidden = true
    }
    
    static func getInstance() -> InviteViewController {
        let storyboard = UIStoryboard(name: "Invite",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loclizatino()
    }
    
    func loclizatino()  {
        
        self.btnBack.setTitle((L102Language.AMLocalizedString(key: "Invite_Friends_MenuKey", value: "")), for: .normal)
        
        let attrStr = NSMutableAttributedString().getAttributedBoldString(str: (L102Language.AMLocalizedString(key: "Invite_Friends_Earn", value: "")), boldTxt: "$150", size: 30)
        self.lblTitle.attributedText = attrStr
        self.lblMsg.text = (L102Language.AMLocalizedString(key: "When_your_friend_sign_up", value: ""))
        self.lblShareInviteCodeTitle.text = (L102Language.AMLocalizedString(key: "SHARE_YOUR_INVITE_CODE", value: ""))
        self.btnShareNow.setTitle((L102Language.AMLocalizedString(key: "SHARE_NOW", value: "")), for: .normal)
        
    }
    

        
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
    }


        
    @IBAction func btnShareNow_Action(_ sender: UIButton) {
        let items: [Any] = ["Let me recommend you this application", URL(string: "https://www.apple.com")!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

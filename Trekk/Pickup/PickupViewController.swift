//
//  PickupViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 13/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class PickupViewController: UIViewController {

    @IBOutlet weak var heightConst : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        heightConst.constant = 140
    }
    

    static func getInstance() -> PickupViewController {
        let storyboard = UIStoryboard(name: "Pickup",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PickupViewController") as! PickupViewController
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
         }
    
    func showView()  {
        heightConst.constant = 600
    }
    func hideView() {
        heightConst.constant = 140
    }
    
    @IBAction func btnShowView(_ sender: UIButton) {
        if heightConst.constant == 600 {
            heightConst.constant = 140
        } else {
            heightConst.constant = 600
        }
    }
    

    
}

//
//  MyTripViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 2/24/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class MyTripViewController: BaseVC {
    
    @IBOutlet weak var tableViewTrip: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tripTableviewCells()
    }
        
    static func getInstance() -> MyTripViewController {
        let storyboard = UIStoryboard(name: "MyTrips",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MyTripViewController") as! MyTripViewController
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension MyTripViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tripTableviewCells(){
        
        tableViewTrip.register(UINib(nibName: "MyTripTableViewCell", bundle: nil), forCellReuseIdentifier: "MyTripTableViewCell")
        tableViewTrip.register(UINib(nibName: "EndTbCell", bundle: nil), forCellReuseIdentifier: "EndTbCell")
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 10
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let distanceCell = tableViewTrip.dequeueReusableCell(withIdentifier: "MyTripTableViewCell", for: indexPath) as! MyTripTableViewCell
            distanceCell.selectionStyle = .none
            return distanceCell
            
        } else {
            let endTbCell = tableViewTrip.dequeueReusableCell(withIdentifier: "EndTbCell", for: indexPath) as! EndTbCell
            endTbCell.selectionStyle = .none
            return endTbCell
            
        }
    }
}


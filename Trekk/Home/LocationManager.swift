//
//  LocationManager.swift
//  Trekk
//
//  Created by Yogender Saini on 28/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//


import Foundation
import MapKit
import CoreLocation

import GoogleMaps
import GooglePlaces
import Alamofire



class LocationManager: NSObject, CLLocationManagerDelegate {

    static let shared = LocationManager()
    var locationManager:CLLocationManager!
    var vc = UIViewController()
    var delegate : locationManagerProtocol?
    var clLocationCoOrdinate = CLLocationCoordinate2D()
    var currentAddress = ""

    override init() {
        super.init()
    }
    
    
    public func getCurrentLocation(vcc : UIViewController){
        self.locationManager = CLLocationManager()
        print("Getting users location...")
        self.vc = vcc
        self.setUpLocationPermissions()
    }
    
    func setUpLocationPermissions() {
                
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        

        if CLLocationManager.locationServicesEnabled() {
            
            switch (CLLocationManager.authorizationStatus()) {
                
            case .notDetermined, .restricted, .denied:
                
                print("BEGIN:Trekk ======> No location service access")
                
                self.requestWhenInUseAuthorization()
                                
            case .authorizedAlways, .authorizedWhenInUse:
                
                print("BEGIN:Trekk ======> Location service access")
                
                determineMyCurrentLocation()
                
            @unknown default:
                
                fatalError()
            }
            
        } else {
            
            print("BEGIN:Trekk ======> Location services are not enabled")
            requestWhenInUseAuthorization()
        }
    }
    
    func requestWhenInUseAuthorization() {
        
        let status: CLAuthorizationStatus = CLLocationManager.authorizationStatus()
        
        if status == .denied {
            
            var title: String
            
            title = (status == .denied) ? "Location Services Off" : ""
            let message = "Turn on Location Services in Settings > Privacy to allow Maps to determine your current location"
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction (title: "Cancel", style: .destructive, handler: nil))
            
            alertController.addAction(UIAlertAction (title: "Settings", style: .default, handler: { (action:UIAlertAction) in
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("BEGIN:Trekk ======> Settings opened: \(success)")
                    })
                }
            }))
            
            self.vc.present(alertController, animated: true, completion: nil)
            
        } else if status == .notDetermined {
            
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let user_Location:CLLocation = locations[0] as CLLocation
        manager.stopUpdatingLocation()
//        delegate?.returnCurrentLocation(newUserLocation: user_Location)
                
        
        // new code start
        clLocationCoOrdinate = CLLocationCoordinate2D(latitude: user_Location.coordinate.latitude, longitude: user_Location.coordinate.longitude)
        self.getAddressFromCoOrdinates(clLocationCoOrdinate, newUserLocation: user_Location)
        self.reverseGeocodeCoordinate(clLocationCoOrdinate)
        // new code end
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
    
    func getAddressFromCoOrdinates(_ coordinate: CLLocationCoordinate2D, newUserLocation : CLLocation) {
            
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
                guard let address = response?.firstResult(), let lines = address.lines ,let subLocality = address.subLocality , let locality = address.locality,let administrativeArea = address.administrativeArea, let postalCode = address.postalCode, let country = address.country else {
                    return
                }
                
                
                let fullAddress = lines.joined()
                print(fullAddress)
                self.currentAddress = fullAddress
                
                print(address)
                let array = fullAddress.components(separatedBy: ",")
                let str = array[0]
                let decimalCharacters = CharacterSet.decimalDigits
                let decimalRange = str.rangeOfCharacter(from: decimalCharacters)

                if decimalRange != nil {
                    print("Numbers found")
                    let array = fullAddress.components(separatedBy: ",")
                    let house = array[0]
                    print(house)
                }else {
                }

                let strt = String(format: "%@ %@,%@,%@,%@", subLocality,locality,administrativeArea,postalCode,country)
                print(strt)
                
                self.delegate?.returnCurrentLocation(newUserLocation: newUserLocation)

                
            }
        }
    
        private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
    
             let geocoder = GMSGeocoder()
             geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
               guard let address = response?.firstResult(), let lines = address.lines else {
                 return
               }
                
                let fullAddress = lines.joined()
                            print(fullAddress)
                            self.currentAddress = fullAddress

                            print(address)
                            
                            
                            let array = fullAddress.components(separatedBy: ",")

                            let str = array[0]
                            let decimalCharacters = CharacterSet.decimalDigits

                            let decimalRange = str.rangeOfCharacter(from: decimalCharacters)

                            if decimalRange != nil {
                                print("Numbers found")
                                let array = fullAddress.components(separatedBy: ",")
                                
                                let house = array[0]
                              //  self.houseNoTF.text = house
                                
                //                self.houseNoTF.text = str

                            }else {
                                
                                
                            }
                self.currentAddress = lines.joined(separator: "\n")

             }
    }
    
}

//
//  CarsCollViewCell.swift
//  Trekk
//
//  Created by Yogender Saini on 2/25/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class CarsCollViewCell: UICollectionViewCell {


    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgViewItem: UIImageView!

    let arrImgesCollItems =  ["time_white","speed_white","order_white"]


    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupData(arrDataVales : [String], index : Int ) {
        
        let arrTitlesCollItem = [(L102Language.AMLocalizedString(key: "HOURSE_ONLINE", value: "")),
                                 (L102Language.AMLocalizedString(key: "TOTAL_DISTANCE", value: "")),
                                 (L102Language.AMLocalizedString(key: "TOTAL_JOBS", value: ""))]
        
        self.imgViewItem.image = UIImage(named: arrImgesCollItems[index])
        self.lblTitle.text = arrTitlesCollItem[index]
        if arrDataVales.count >= 1 {
            self.lblValue.text = arrDataVales[index]
        }
    }

}

//
//  CustViewInvitation.swift
//  Trekk
//
//  Created by Yogender Saini on 05/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class CustViewInvitation: UIView {

    
    // CREATED BUT NOT USING THIS VIEW IN PROJECT.
    
    // CREATED BUT NOT USING THIS VIEW IN PROJECT.
    
    // CREATED BUT NOT USING THIS VIEW IN PROJECT.
    
    // CREATED BUT NOT USING THIS VIEW IN PROJECT.
    
    
    let nibName = "CustViewInvitation"
    var contentView: UIView?
    var viewCont = UIViewController()
    var requestHideTimer : Timer?
    var totalSeconds = 60

    @IBOutlet weak var imgVww: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnSkipNow: UIButton!

    @IBOutlet weak var lblTime: UILabel!

    @IBOutlet weak var viewBackg: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        viewBackg.dropShadowT(scale: false,color: UIColor.lightGray)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        viewBackg.dropShadowT(scale: false,color: UIColor.lightGray)
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func btnSignupAction(_ sender : UIButton) {
       let vc = SignUpVC.getInstanceSignInPhoneVC()
        self.viewCont.navigationController?.pushViewController(vc, animated: true)
   }
    
    func setupDataInInvitationView() {
        self.totalSeconds = 60
        self.requestHideTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updatTime), userInfo: nil, repeats: true)
    }

    @objc func updatTime() {
        if self.totalSeconds >= 10 {
            self.lblTime.text = "00:\(self.totalSeconds)"
        } else {
            self.lblTime.text = "00:0\(self.totalSeconds)"
        }
        if self.totalSeconds == 0 {
            self.requestHideTimer?.invalidate()
        } else {
            self.totalSeconds = self.totalSeconds - 1
        }
    }


}

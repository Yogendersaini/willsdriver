//
//  HomeView.swift
//  Trekk
//
//  Created by Yogender Saini on 05/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps
import GooglePlaces


var RIDE_TIMER_GLOBEL: Timer?

class HomeView: UIView {
    // map view
    @IBOutlet weak var mapVw: GMSMapView!
    
    @IBOutlet weak var viewButtomDetails: UIView!
    @IBOutlet weak var viewInvitation: UIView!
    @IBOutlet weak var viewEndRide: UIView!
    @IBOutlet weak var viewEndRide_Address: UIView!
    @IBOutlet weak var viewRideDetails: UIView!
    @IBOutlet weak var lblOnline: UILabel!
    @IBOutlet weak var switchOnline: UISwitch!
    @IBOutlet weak var viewOffLine: UIView!
    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var viewRating: UIView!
    @IBOutlet weak var collectionViewCars: UICollectionView!
    
    var ride_id = ""
    var vc = UIViewController()
    var objRideModel = RideModel()
    var objAcceptModle = AcceptDataModel()
    var objRequestModel = RequestedDataModel()
    var objStartModel = RideStartedModel()
    var objEndModel = RideEndedModel()
    var objPaymentModel = PaymentDoneModel()
    
    // Off Line View
    @IBOutlet weak var lblYouAreOfflineTitle: UILabel!
    @IBOutlet weak var imgProfile_OfflineView: UIImageView!
    @IBOutlet weak var lblGoOnlineTitle: UILabel!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblLevelType: UILabel!
    @IBOutlet weak var lblBaseFare: UILabel!
    @IBOutlet weak var lblEarned: UILabel!
    var arrOffLineDataVAlues = [String]()
    
    
    // Invitation View
    @IBOutlet weak var lblRiderName_InvitationView: UILabel!
    @IBOutlet weak var imgProfile_InvitationView: UIImageView!
    @IBOutlet weak var lblDistanceFare_InvitationView: UILabel!
    @IBOutlet weak var lblDistance_InvitationView: UILabel!
    @IBOutlet weak var lblPickupLoc_InvitationView: UILabel!
    @IBOutlet weak var lblDropupLoc_InvitationView: UILabel!
    @IBOutlet weak var lblTime_InvitationView: UILabel!
    @IBOutlet weak var lblPickupTitle_InvitationView: UILabel!
    @IBOutlet weak var lblDropupTitle_InvitationView: UILabel!
    @IBOutlet weak var bntIgnore_InvitationView: UIButton!
    @IBOutlet weak var btnAccept_InvitationView: UIButton!
    
    var requestHideTimer : Timer?
    var totalSeconds = 60
    
    // Ride Details View // after accept ride
    @IBOutlet weak var lblRiderName_RideDetailsView: UILabel!
    @IBOutlet weak var imgProfile_RideDetailsView: UIImageView!
    @IBOutlet weak var lblDistanceFare_RideDetailsView: UILabel!
    @IBOutlet weak var lblDistance_RideDetailsView: UILabel!
    @IBOutlet weak var lblPickupLoc_RideDetailsView: UILabel!
    @IBOutlet weak var lblDropupLoc_RideDetailsView: UILabel!
    @IBOutlet weak var lblNotes_RideDetailsView: UILabel!
    
    @IBOutlet weak var lblPickupTitle_RideDetailsView: UILabel!
    @IBOutlet weak var lblDropupTitle_RideDetailsView: UILabel!
    @IBOutlet weak var lblNotesTitle_RideDetailsView: UILabel!
    
    @IBOutlet weak var btnCAll_RideDetailsView:  UILabel!
    @IBOutlet weak var btnMessage_RideDetailsView:  UILabel!
    @IBOutlet weak var btnCancel_RideDetailsView:  UILabel!
    
    @IBOutlet weak var btnStarted_RideDetailsView: UIButton!
    @IBOutlet weak var btnGoToPickup_RideDetailsView: UIButton!
    
    
    
    // End View View // after start ride
    @IBOutlet weak var lblRiderName_EndView: UILabel!
    @IBOutlet weak var imgProfile_EndView: UIImageView!
    @IBOutlet weak var imgtaxi_EndView: UIImageView!
    @IBOutlet weak var lblRideTime_EndView: UILabel!
    @IBOutlet weak var lblDistance_EndView: UILabel!
    @IBOutlet weak var lblNumberPlat_EndView: UILabel!
    @IBOutlet weak var lblBasic_EndView: UILabel!
    
    @IBOutlet weak var lblRideTimeTitle_EndView: UILabel!
    @IBOutlet weak var lblDistanceTitle_EndView: UILabel!
    @IBOutlet weak var btnEndRide_EndView: UIButton!
    
    @IBOutlet weak var lblDestAddTitle_EndView: UILabel!
    @IBOutlet weak var lblDestAddress_EndView: UILabel!
    var timerRideTime_EndView : Timer?
    var second_EndView = 00
    var minuts_EndView = 00
    var hrs_EndView = 00
    
    // Payment View // after end ride
    @IBOutlet weak var lblRiderName_PaymentView: UILabel!
    @IBOutlet weak var imgProfile_PaymentView: UIImageView!
    @IBOutlet weak var lblPickupLoc_PaymentView: UILabel!
    @IBOutlet weak var lblDropupLoc_PaymentView: UILabel!
    @IBOutlet weak var lblNotes_PaymentView: UILabel!
    @IBOutlet weak var lblTotalKms_PaymentView: UILabel!
    @IBOutlet weak var lblTotalMinuts_PaymentView: UILabel!
    @IBOutlet weak var lblStarRideTime_PaymentView: UILabel!
    @IBOutlet weak var lblEndRideTime_PaymentView: UILabel!
    @IBOutlet weak var lblBasicPrice_PaymentView: UILabel!
    @IBOutlet weak var lblRideCose_PaymentView: UILabel!
    @IBOutlet weak var lblServiceTax_PaymentView: UILabel!
    @IBOutlet weak var lblPromoCode_PaymentView: UILabel!
    @IBOutlet weak var lblTotalCost_PaymentView: UILabel!
    
    @IBOutlet weak var lblPickupTitle_PaymentView: UILabel!
    @IBOutlet weak var lblDropupTitle_PaymentView: UILabel!
    @IBOutlet weak var lblNotesTitle_PaymentView: UILabel!
    @IBOutlet weak var lblTotalKmsTitle_PaymentView: UILabel!
    @IBOutlet weak var lblTotalMinutsTitle_PaymentView: UILabel!
    @IBOutlet weak var lblStarRideTimeTitle_PaymentView: UILabel!
    @IBOutlet weak var lblEndRideTimeTitle_PaymentView: UILabel!
    @IBOutlet weak var lblBasicPriceTitle_PaymentView: UILabel!
    @IBOutlet weak var lblRideCoseTitle_PaymentView: UILabel!
    @IBOutlet weak var lblServiceTaxTitle_PaymentView: UILabel!
    @IBOutlet weak var lblPromoCodeTitle_PaymentView: UILabel!
    @IBOutlet weak var lblTotalCostTitle_PaymentView: UILabel!
    @IBOutlet weak var lblPayment_IS_PendingTitle_PaymentView: UILabel!
    @IBOutlet weak var  lblBillingDetails_PaymentView: UILabel!
    
    // Rating View // after payment ride
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    @IBOutlet weak var lblRiderName_RatingView: UILabel!
    @IBOutlet weak var imgProfile_RatingView: UIImageView!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var btnSubmit_RatingView: UIButton!
    
    var starRating = "0"
    
    
    func localization() {
        self.lblYouAreOfflineTitle.text =  (L102Language.AMLocalizedString(key: "You_are_offline", value: ""))
        self.lblGoOnlineTitle.text =  (L102Language.AMLocalizedString(key: "Go_online_to_start_accepting_jobs", value: ""))
    }
    
    
    func setupViewsInitially()  {
        self.viewInvitation.isHidden = true
        self.viewEndRide.isHidden = true
        self.viewRideDetails.isHidden = true
        self.viewEndRide_Address.isHidden = true
    }
    
    func shadowViews() {
        self.viewEndRide.dropShadowT(scale: false, color: UIColor.lightGray)
        self.viewRideDetails.dropShadowT(scale: false, color: UIColor.lightGray)
    }
    
    func offLine(vc : UIViewController) {
        localization()
        self.viewOffLine.isHidden = false
        self.lblOnline.text =  (L102Language.AMLocalizedString(key: "title_Offline", value: ""))
        self.viewButtomDetails.isHidden = false
        RIDE_TIMER_GLOBEL?.invalidate()
        self.apiCalling_GetOnlne(status: "0")
        UserDefaults.standard.set(false, forKey: IS_DRIVER_ONLINE)
        // calling get ride api for update offline data. only for once.
        self.apiCalling_GetRide()
    }
    
    func onLine(vc : UIViewController) {
        localization()
        self.lblOnline.text =  (L102Language.AMLocalizedString(key: "title_Online", value: ""))
        self.viewOffLine.isHidden = true
        self.viewButtomDetails.isHidden = true
        self.apiCalling_GetOnlne(status: "1")
        UserDefaults.standard.set(true, forKey: IS_DRIVER_ONLINE)
    }
    
    func getRideIn_Every3Mints(vc : UIViewController) {
        self.vc = vc
        RIDE_TIMER_GLOBEL = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(apiCalling_GetRide), userInfo: nil, repeats: true)
    }
    
    
    // Mark :- Api Work
    
    @objc func apiCalling_GetRide() {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "ride_id" : self.ride_id, "extended" : "1"]
        
        
        AppHelper.apiCallingForDict_WithoutLoader(apiName: GetRide, param: param, viewCont: self.vc, jsonResponce: { (resultDict) in
            
            print("resultDict---->", resultDict)
            self.objRideModel = RideModel.init(with: resultDict as! [String : Any])
            self.setupData_In_OffLineView(obj: self.objRideModel.dashboard_data)
            
            self.ride_id = self.objRideModel.requested.ride_id
            print("Ride id:------->", self.ride_id)
            self.setCurrentDriverPosition(obj: self.objRideModel)
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message, jsonResponce) in
            
            self.objRideModel = RideModel.init(with: jsonResponce as! [String : Any])
            self.setupData_In_OffLineView(obj: self.objRideModel.dashboard_data)
            self.setCurrentDriverPosition(obj: self.objRideModel)
            
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    @objc func apiCalling_GetOnlne(status : String) {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "status" : status]
        
        AppHelper.apiCallingForDict(apiName: GetupdateOnlineStatus, param: param, viewCont: self.vc, successDict: { (successDict, resultDict) in
            print(successDict)
            
            if status == "1" {
                self.getRideIn_Every3Mints(vc: self.vc)
                print("You are on Line now")
            } else {
                print("You are off Line now")
            }
            
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            
            if messageID == "142" {
                if status == "1" {
                    self.getRideIn_Every3Mints(vc: self.vc)
                    print("You are on Line now")
                } else {
                    print("You are off Line now")
                }
            }
            
            
            //                            UIAlertController.showAlert(vc: self.vc, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    
    
    @objc func apiCalling_GetAccept_Reject(status : String) {
        
        if self.ride_id == "" {
        //    self.makeToast("Ride id is can't be nil")
            return
        }
        
        print(self.ride_id)
        
        let param  = ["client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "ride_status" : status, "ride_id" : self.ride_id]
        
        AppHelper.apiCallingForDict(apiName: GetAcceptAndRejectRide, param: param, viewCont: self.vc, successDict: { (successDict, resultDict) in
            print(successDict)
            self.objAcceptModle = AcceptDataModel(with: successDict)
            self.ride_id = self.objAcceptModle.ride_id
            
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            
            UIAlertController.showAlert(vc: self.vc, title: "", message:(message))
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    
    @objc func apiCalling_GetCancelRide() {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "ride_id" : self.ride_id, "extended" : "1"]
        
        
        AppHelper.apiCallingForDict_WithoutLoader(apiName: GetCancelRide, param: param, viewCont: self.vc, jsonResponce: { (resultDict) in
            
            print("CancelDict ---->", resultDict)
            self.setupData_In_CancelRide()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message, jsonResponce) in
            UIAlertController.showAlert(vc: self.vc, title: "", message:(message))
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    
    
    @objc func apiCalling_GetStartRide() {
        
        let param  = ["client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "ride_id" : self.ride_id]
        
        AppHelper.apiCallingForDict(apiName: GetStartRide, param: param, viewCont: self.vc, successDict: { (successDict, resultDict) in
            print(successDict)
            
            self.objStartModel = RideStartedModel(with: successDict)
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            
            UIAlertController.showAlert(vc: self.vc, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    
    @objc func apiCalling_GetEndRide() {
        
        let param  = ["client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "ride_id" : self.ride_id]
        
        AppHelper.apiCallingForDict(apiName: GetEndRide, param: param, viewCont: self.vc, successDict: { (successDict, resultDict) in
            print(successDict)
            
            self.objEndModel = RideEndedModel(with: successDict)
            self.ride_id = self.objEndModel.ride_id
            
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            
            UIAlertController.showAlert(vc: self.vc, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    
    
    @objc func apiCalling_GetRateRide() {
        
        self.detailTextView.text = self.detailTextView.text.trimmingCharacters(in: .whitespaces)
        if self.detailTextView.text == ""  || self.starRating == "0" {
            self.makeToast("Please use star rating and write some comment")
            return
        }
        
        RIDE_TIMER_GLOBEL?.invalidate()
        
        let param  = ["client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "ride_id" : self.ride_id, "rating" : self.starRating, "comment" : self.detailTextView.text! ]
        
        AppHelper.apiCallingForDict(apiName: GetRateRide, param: param, viewCont: self.vc, successDict: { (successDict, resultDict) in
            print(successDict)
            let msg =  resultDict["message"] as? String ?? ""
            self.makeToast(msg)
            self.ride_id = ""
            self.viewRating.isHidden = true
            
            self.stopRide_reStart_GetRideAPI()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            
            UIAlertController.showAlert(vc: self.vc, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    func stopRide_reStart_GetRideAPI() {
        self.ride_id = ""
        self.detailTextView.text = ""
        self.rating(star: 0)
        self.viewRating.isHidden = true
        RIDE_TIMER_GLOBEL?.invalidate()
        
        RIDE_TIMER_GLOBEL = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(apiCalling_GetRide), userInfo: nil, repeats: true)
        
    }
    
    
    
    
    
    
    
    
    // Mark :- Setup Data From API
    
    // check what is the current position of rider.
    func setCurrentDriverPosition(obj : RideModel)  {
        
        if obj.is_requested == "1" {
            self.viewInvitation.isHidden = false
            self.setupData_In_InvitationView(obj: obj.requested)
        } else {
            self.viewInvitation.isHidden = true
        }
        
        if obj.is_accepted == "1" {
            self.viewRideDetails.isHidden = false
            self.setupData_In_RideDetails(obj: obj.accepted)
        } else {
            self.viewRideDetails.isHidden = true
        }
        
        if obj.is_ride_started == "1" {
            self.viewEndRide.isHidden = false
            self.viewEndRide_Address.isHidden = false
            self.setupData_In_EndView(obj: obj.ride_started)
        } else {
            self.viewEndRide.isHidden = true
            self.viewEndRide_Address.isHidden = true
        }
        
        if obj.is_ride_ended == "1" {
            self.viewPayment.isHidden = false
            self.setupData_In_PaymentView(obj: obj.ride_ended)
        } else {
            self.viewPayment.isHidden = true
        }
        
        if obj.is_payment_done == "1" {
            //            self.detailTextView.text = ""
            self.viewRating.isHidden = false
            self.setupData_In_RatingView(obj: obj.payment_done)
        } else {
            self.viewRating.isHidden = true
        }
        
        
        //"is_accepted" = 0;
        //"is_online" = 1;
        //"is_payment_done" = 1;
        //"is_requested" = 0;
        //"is_ride_ended" = 0;
        //"is_ride_started" = 0;
        
    }
    
    //when driver in off line.
    @objc func setupData_In_OffLineView( obj : DashboardDataModel) {
        self.imgProfile_OfflineView.sd_setImage(with: URL(string: obj.image), placeholderImage: UIImage(named: "profile_pic"))
        self.lblDriverName.text = obj.full_name
        self.lblEarned.text = obj.total_earning
        self.lblBaseFare.text = ""
        self.lblLevelType.text = (L102Language.AMLocalizedString(key: "Basic_Level", value: ""))        
        self.arrOffLineDataVAlues.removeAll()
        self.arrOffLineDataVAlues.insert(obj.total_online_hours, at: 0)
        self.arrOffLineDataVAlues.insert(obj.total_distance, at: 1)
        self.arrOffLineDataVAlues.insert(obj.total_jobs, at: 2)
        self.collectionViewCars.reloadData()
        
    }
    
    // accept or ignor ride
    func setupData_In_InvitationView(obj : RequestedDataModel) {
        self.ride_id = obj.ride_id
        self.lblRiderName_InvitationView.text = obj.rider_name
        self.imgProfile_InvitationView.sd_setImage(with: URL(string: obj.rider_image), placeholderImage: UIImage(named: "profile_pic"))
        self.lblDistanceFare_InvitationView.text = obj.distance_fare
        self.lblDistance_InvitationView.text = obj.distance
        self.lblPickupLoc_InvitationView.text = obj.start_location
        self.lblDropupLoc_InvitationView.text = obj.end_location
        self.viewInvitation.isHidden = false
        
        self.lblPickupTitle_InvitationView.text = (L102Language.AMLocalizedString(key: "PICK_UP", value: ""))
        self.lblDropupTitle_InvitationView.text = (L102Language.AMLocalizedString(key: "DROP_OFF", value: ""))
        
        self.bntIgnore_InvitationView.setTitle((L102Language.AMLocalizedString(key: "Ignore_Button", value: "")), for: .normal)
        self.btnAccept_InvitationView.setTitle((L102Language.AMLocalizedString(key: "Accept_Button", value: "")), for: .normal)
        
        // check weather timer is already running for timer left request
        if self.totalSeconds == 60 || !self.requestHideTimer!.isValid {
            self.requestHideTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updatRequestLeftTime), userInfo: nil, repeats: true)
        }
        
        (L102Language.AMLocalizedString(key: "Chat_Screen", value: ""))
        
        
    }
    
    // timer for Invitation view to check request/Invitation is only for 60 seconds.
    @objc func updatRequestLeftTime() {
        if self.totalSeconds >= 10 {
            self.lblTime_InvitationView.text = "00:\(self.totalSeconds)"
        } else {
            self.lblTime_InvitationView.text = "00:0\(self.totalSeconds)"
        }
        if self.totalSeconds == 0 {
            self.apiCalling_GetAccept_Reject(status: "reject")
            self.requestHideTimer?.invalidate()
            self.totalSeconds = 60
            self.viewInvitation.isHidden = true
        } else {
            self.totalSeconds = self.totalSeconds - 1
        }
    }
    
    // after accept ride
    func setupData_In_RideDetails(obj : AcceptDataModel) {
        self.ride_id = obj.ride_id
        self.viewInvitation.isHidden = true
        self.lblRiderName_RideDetailsView.text = obj.rider_name
        self.imgProfile_RideDetailsView.sd_setImage(with: URL(string: obj.rider_image), placeholderImage: UIImage(named: "profile_pic"))
        self.lblDistanceFare_RideDetailsView.text = obj.distance_fare
        self.lblDistance_RideDetailsView.text = obj.distance
        self.lblPickupLoc_RideDetailsView.text = obj.start_location
        self.lblDropupLoc_RideDetailsView.text = obj.end_location
        self.lblNotes_RideDetailsView.text = obj.notes
        
        self.lblPickupTitle_RideDetailsView.text = (L102Language.AMLocalizedString(key: "PICK_UP", value: ""))
        self.lblDropupTitle_RideDetailsView.text = (L102Language.AMLocalizedString(key: "DROP_OFF", value: ""))
        self.lblNotesTitle_RideDetailsView.text = (L102Language.AMLocalizedString(key: "NOTED", value: ""))
        
        self.btnCAll_RideDetailsView.text = (L102Language.AMLocalizedString(key: "Call_Button", value: ""))
        
        self.btnMessage_RideDetailsView.text = (L102Language.AMLocalizedString(key: "Message_Button", value: ""))
        
        self.btnCancel_RideDetailsView.text = (L102Language.AMLocalizedString(key: "Cancel_Button", value: ""))
        
        self.btnStarted_RideDetailsView.setTitle((L102Language.AMLocalizedString(key: "TAP_WHEN_STARTED_Button", value: "")), for: .normal)
        self.btnGoToPickup_RideDetailsView.setTitle((L102Language.AMLocalizedString(key: "GO_TO_PICK_UP_Button", value: "")), for: .normal)
        
        self.viewRideDetails.isHidden = false
        
        self.fetchRoute(orgLet: obj.start_lat, orgLogn: obj.start_lon, destLat: obj.end_lat, destLong: obj.end_lon)
        
    }
    
    
    // after start ride
    func setupData_In_EndView(obj : RideStartedModel) {
        self.ride_id = obj.ride_id
        self.viewRideDetails.isHidden = true
        self.lblRiderName_EndView.text = obj.rider_name
        self.imgProfile_EndView.sd_setImage(with: URL(string: obj.rider_image), placeholderImage: UIImage(named: "profile_pic"))
        self.imgtaxi_EndView.sd_setImage(with: URL(string: obj.taxi_image), placeholderImage: UIImage(named: "profile_pic"))
        self.lblDistance_EndView.text = obj.distance
        self.lblBasic_EndView.text = (L102Language.AMLocalizedString(key: "Basic_Level", value: ""))
        self.lblNumberPlat_EndView.text = obj.taxi_number
        
        
        self.lblRideTimeTitle_EndView.text = (L102Language.AMLocalizedString(key: "Total_Ride_Time", value: ""))
        self.lblDistanceTitle_EndView.text = (L102Language.AMLocalizedString(key: "Total_Distance", value: ""))
        self.btnEndRide_EndView.setTitle((L102Language.AMLocalizedString(key: "End_Ride_Button", value: "")), for: .normal)
        
        self.lblDestAddTitle_EndView.text =  (L102Language.AMLocalizedString(key: "Destination_Address", value: ""))
        self.lblDestAddress_EndView.text = obj.end_location
        
        
        self.viewEndRide.isHidden = false
        self.viewEndRide_Address.isHidden = false
        
        self.updateRideTime_In_EndView()
        
        self.fetchRoute(orgLet: obj.start_lat, orgLogn: obj.start_lon, destLat: obj.end_lat, destLong: obj.end_lon)

        
    }
    
    func updateRideTime_In_EndView() {
        
        //        if self.second_EndView == 0 {
        self.second_EndView = self.second_EndView + 3
        //        }
        if self.second_EndView == 60 {
            self.minuts_EndView = self.minuts_EndView + 1
            self.second_EndView = 0
        }
        if self.minuts_EndView == 60 {
            self.hrs_EndView = self.hrs_EndView + 1
            self.minuts_EndView = 0
        }
        
        let sc = (self.second_EndView >= 10) ? "\(self.second_EndView)" : "0\(self.second_EndView)"
        let mnt = (self.minuts_EndView >= 10) ? "\(self.minuts_EndView)" : "0\(self.minuts_EndView)"
        let hr = (self.hrs_EndView >= 10) ? "\(self.hrs_EndView)" : "0\(self.hrs_EndView)"
        
        self.lblRideTime_EndView.text = hr + ":" + mnt + ":" + sc
        //"\(self.hrs_EndView) : \(self.minuts_EndView) : \(self.second_EndView)"
        
    }
    
    
    
    // after end ride
    func setupData_In_PaymentView(obj : RideEndedModel)  {
        self.mapVw.clear()
        self.ride_id = obj.ride_id
        self.viewEndRide.isHidden = true
        self.viewEndRide_Address.isHidden = true
        self.lblRiderName_PaymentView.text = obj.rider_name
        self.imgProfile_PaymentView.sd_setImage(with: URL(string: obj.rider_image), placeholderImage: UIImage(named: "profile_pic"))
        self.lblPickupLoc_PaymentView.text = obj.start_location
        self.lblDropupLoc_PaymentView.text = obj.end_location
        self.lblNotes_PaymentView.text = obj.notes
        self.lblTotalKms_PaymentView.text = obj.distance
        self.lblTotalMinuts_PaymentView.text = obj.duration
        self.lblStarRideTime_PaymentView.text = obj.pickup_time
        self.lblEndRideTime_PaymentView.text = obj.drop_time
        self.lblBasicPrice_PaymentView.text = obj.base_fare
        self.lblRideCose_PaymentView.text = obj.ride_cost
        self.lblServiceTax_PaymentView.text = obj.tax
        self.lblPromoCode_PaymentView.text = "0"
        self.lblTotalCost_PaymentView.text = obj.total_bill
        self.viewPayment.isHidden = false
        
        // setupTittle as per language
        self.lblPickupTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "PICK_UP", value: ""))
        self.lblDropupTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "DROP_OFF", value: ""))
        self.lblNotesTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "NOTED", value: ""))
        
        self.lblTotalKmsTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Total_Kms", value: ""))
        self.lblTotalMinutsTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Total_Minutes", value: ""))
        
        self.lblStarRideTimeTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Stat_Ride_Time", value: ""))
        self.lblEndRideTimeTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "End_Ride_Time", value: ""))
        self.lblBasicPriceTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Base_Price", value: ""))
        
        self.lblRideCoseTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Ride_Cost", value: ""))
        self.lblServiceTaxTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Service_Tax", value: ""))
        self.lblPromoCodeTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Promo_Code", value: ""))
        
        self.lblTotalCostTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Total_Cost", value: ""))
        self.lblPayment_IS_PendingTitle_PaymentView.text = (L102Language.AMLocalizedString(key: "Payment_is_pending", value: ""))
        
        self.lblBillingDetails_PaymentView.text = (L102Language.AMLocalizedString(key: "Billing_Details", value: ""))
        
        
        
    }
    
    // after payment
    func setupData_In_RatingView(obj : PaymentDoneModel)  {
        self.ride_id = obj.ride_id
        self.lblRiderName_RatingView.text = obj.rider_name
        self.imgProfile_RatingView.sd_setImage(with: URL(string: obj.rider_image), placeholderImage: UIImage(named: "profile_pic"))
        self.lblPlaceholder.text = (L102Language.AMLocalizedString(key: "Write_your_comment", value: ""))
        self.btnSubmit_RatingView.setTitle((L102Language.AMLocalizedString(key: "Submit_Button", value: "")), for: .normal)
    }
    
    // cancel ride
    func setupData_In_CancelRide()  {
        self.viewRideDetails.isHidden = true
        self.ride_id = ""
    }
    
    func setupData_In_ChatView(vc : UIViewController) {
        let wVC = AboutViewController.getInstance()
        wVC.btnBackTitle = (L102Language.AMLocalizedString(key: "Chat_Screen", value: ""))
        wVC.index = 3
        wVC.rider_ID = self.objAcceptModle.rider_id
        vc.navigationController?.pushViewController(wVC, animated: true)
    }
    
    func setupData_In_CallView() {
        //                let number = "+918619077641"
        let number = self.objAcceptModle.rider_phone
        if number == "" {
            self.makeToast( "Unable to call, Invalid phone number")
        } else {
            let arr = (number as! NSString).replacingOccurrences(of: " ", with: "")
            let formatedNumber = arr
            let phoneUrl = "tel://\(formatedNumber)"
            let url:NSURL = NSURL(string: phoneUrl)!
            UIApplication.shared.openURL(url as URL)
        }
        
    }
    
    
    func rating(star : Int)  {
        if star == 1 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_-1")
            imgRating3.image = UIImage(named: "ic_star_-1")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
            starRating = "1"
        }
        else if star == 2 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_-1")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
            starRating = "2"
        }
        else if star == 3 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_24px")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
            starRating = "3"
        }
        else if star == 4 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_24px")
            imgRating4.image = UIImage(named: "ic_star_24px")
            imgRating5.image = UIImage(named: "ic_star_-1")
            starRating = "4"
        }
        else if star == 5 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_24px")
            imgRating4.image = UIImage(named: "ic_star_24px")
            imgRating5.image = UIImage(named: "ic_star_24px")
            starRating = "5"
        }
        else  {
            imgRating1.image = UIImage(named: "ic_star_-1")
            imgRating2.image = UIImage(named: "ic_star_-1")
            imgRating3.image = UIImage(named: "ic_star_-1")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
            starRating = "0"
        }
        
    }
    
    func open_GoogleMaps_App() {
        // to open google direction app
        let lat = self.objRideModel.accepted.end_lat
        let lon = self.objRideModel.accepted.end_lon
        
        print("----------------------------------------------------")
        print(lat)
        print(lon)
        
        print(self.objRideModel.accepted.start_lat)
        print(self.objRideModel.accepted.start_lon)
        print("----------------------------------------------------")
        
        let urlString = "http://maps.google.com/?daddr=\(lat),\(lon)&directionsmode=driving"
        
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            UIApplication.shared.open(URL(string: urlString)!, options: [:], completionHandler: nil)
        } else {
            print("Can't use comgooglemaps://")
            UIApplication.shared.open(URL(string: "http://maps.google.com/maps?q=loc:\(lat),\(lon)&zoom=14&views=traffic")!, options: [:], completionHandler: nil)
        }
    }
    
    
    func fetchRoute(orgLet : String, orgLogn : String, destLat  : String, destLong : String) {
        
        let originAddressLat = orgLet //26.9059
        let originAddressLng =  orgLogn //75.7727
        let destinationAddressLat = destLat //26.9546
        let destinationAddressLong = destLong //75.7456
                        
        let session = URLSession.shared
        
//        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(originAddressLat),\(originAddressLng)&destination=\(destinationAddressLat),\(destinationAddressLong)&sensor=false&mode=driving&key=AIzaSyBM2QjQa_pwKfqy1gnS2ztu7_S2guQqcJw")!
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(originAddressLat),\(originAddressLng)&destination=\(destinationAddressLat),\(destinationAddressLong)&sensor=false&mode=driving&key=\(MAP_DirectionKey)")!
        
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            
            
            guard let jsonResult = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any] else {
                print("error in JSONSerialization")
                return
            }
            
            
            print(jsonResult)
            
            if (jsonResult["status"] as? String ) == "REQUEST_DENIED" {
                
                UIAlertController.showAlert(vc: self.vc, title: ALERT_TITLE, message: jsonResult["error_message"] as? String ?? "")
                return
            }
            
            guard let routes = jsonResult["routes"] as? [Any] else {
                return
            }
            print(routes)
            if routes.count < 1 {
                return
            }
            guard let route = routes[0] as? [String: Any] else {
                return
            }
            guard let overview_polyline = route["overview_polyline"] as? [String: Any] else {
                return
            }
            guard let polyLineString = overview_polyline["points"] as? String else {
                return
            }
            
            //  Call this method to draw path on map
            self.drawPath(from: polyLineString)
        })
        task.resume()
    }
    
    func drawPath(from polyStr: String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = mapVw // Google MapView
        
    }
    
    
    
}

//    status = complete;
//
//status = "driver_assigned";
//status = picked;
//status = dropped;
//status = complete;
//
//"is_accepted" = 0;
//"is_online" = 1;
//"is_payment_done" = 1;
//"is_requested" = 0;
//"is_ride_ended" = 0;
//"is_ride_started" = 0;


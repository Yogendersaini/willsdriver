//
//  HomeViewController.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 11/04/20.
//  Copyright © 2020 Harjit Singh Mac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

import LGSideMenuController
import FittedSheets
import IQKeyboardManagerSwift
import Toast_Swift
import CoreLocation
import Alamofire
import CoreTelephony

protocol locationManagerProtocol {
    func returnCurrentLocation(newUserLocation : CLLocation)
}


class HomeViewController: UIViewController, GMSMapViewDelegate, UITextViewDelegate{
    
    
    @IBOutlet weak var Bookanotherbtn: UIButton!
    @IBOutlet weak var viewHome: HomeView!
    @IBOutlet weak var viewTop: UIView!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController!
    var gmsAutoSearch = Bool()
    var imageScooter = String()
    
    
    // Collection View Details
//    @IBOutlet weak var collectionViewCars: UICollectionView!
    
    
    //MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.Bookanotherbtn.addTarget(self, action: #selector(onTapMenuBtn), for: .touchUpInside)
        self.registerCollectionCell()
        self.viewHome.shadowViews()

//        resultsViewController = GMSAutocompleteResultsViewController()
//        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
        
        self.viewHome.setupViewsInitially()
        
        LocationManager.shared.getCurrentLocation(vcc: self)
        LocationManager.shared.delegate = self

    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkWeather_UserLeaveOnlineOrOffLine()
    }
    
    
    func checkWeather_UserLeaveOnlineOrOffLine() {
        
        if UserDefaults.standard.bool(forKey: IS_DRIVER_ONLINE) == true {
            self.viewHome.onLine(vc: self)
            self.viewHome.switchOnline.isOn = true
            self.viewHome.collectionViewCars.reloadData()
        } else{
            self.viewHome.offLine(vc: self)
            self.viewHome.switchOnline.isOn = false
//            self.viewHome.collectionViewCars.reloadData() // to update 
        }

    }
    @IBAction func btn_dummy(_ sender : UIButton){
        
//        self.viewHome.apiCAlling_GoogleMapDirection()
        let vc = SignUpVC.getInstanceSignInPhoneVC()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btn_Reject(_ sender : UIButton){
        self.viewHome.apiCalling_GetAccept_Reject(status: "reject")
    }
    
    @IBAction func btn_Accept(_ sender : UIButton){
        self.viewHome.apiCalling_GetAccept_Reject(status: "accept")
    }
    
    @IBAction func btn_Start(_ sender : UIButton){
        self.viewHome.apiCalling_GetStartRide()
    }
    
    @IBAction func btn_End(_ sender : UIButton){
        self.viewHome.apiCalling_GetEndRide()
    }
    
    @IBAction func btn_Goto_Pickup_Action(_ sender : UIButton){
        self.viewHome.open_GoogleMaps_App()
    }
    
    @IBAction func btn_Close_TextView_Keyboard(_ sender : UIButton){
        self.viewHome.detailTextView.resignFirstResponder()
    }
    
    @IBAction func btn_Hide_RatingView(_ sender : UIButton){
        self.viewHome.viewRating.isHidden = true
    }
    
//    @IBAction func btn_Rate(_ sender : UIButton){
//        self.viewHome.apiCalling_GetRateRide()
//    }
    @IBAction func btn_CancelRide(_ sender : UIButton){
        self.viewHome.apiCalling_GetCancelRide()
    }
    @IBAction func btn_Calling_Rider(_ sender : UIButton){
        self.viewHome.setupData_In_CallView()
    }

    @IBAction func btn_MessageRide(_ sender : UIButton){
        self.viewHome.setupData_In_ChatView(vc: self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        if self.viewHome.detailTextView.text.isEmpty {
            self.viewHome.lblPlaceholder.isHidden = false
        } else {
            self.viewHome.lblPlaceholder.isHidden = true
        }
    }
    
    static func getInstance() -> HomeViewController {
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
    }
    
    
    
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
  
    
    @objc func onTapMenuBtn() {
        
        DispatchQueue.main.async {
            
            self.showLeftViewAnimated(nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        print(self.Bookanotherbtn.frame)
        print(self.viewTop.frame)
    }
    
    @IBAction func btn_CurrentLocation(_ sender : UIButton){
       // self.searchLocation()
    }
    
    
    @IBAction func btn_I_am_going(_ sender : UIButton){
      //  self.searchLocation()
    }

    
    @IBAction func btnSwitchONline(_ sender : UISwitch){
        if sender.isOn {
            self.viewHome.onLine(vc: self)
        } else {
            self.viewHome.offLine(vc: self)
        }
    }
    
    
    @IBAction func btnStarRating_Action(_ sender : UIButton){
        self.viewHome.rating(star: sender.tag)
    }
    
    @IBAction func btnSubmitRating_Action(_ sender : UIButton){
        self.viewHome.apiCalling_GetRateRide()
    }

    
}
extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource,  UICollectionViewDelegateFlowLayout {
    
    func registerCollectionCell() {
        let firstNib = UINib(nibName: "CarsCollViewCell", bundle: nil)
        self.viewHome.collectionViewCars.register(firstNib, forCellWithReuseIdentifier: "CarsCollViewCell")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.viewHome.collectionViewCars.dequeueReusableCell(withReuseIdentifier: "CarsCollViewCell", for: indexPath) as! CarsCollViewCell
        cell.setupData(arrDataVales: self.viewHome.arrOffLineDataVAlues, index: indexPath.row)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 116, height: 120)
    }

//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 1.0
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout
//        collectionViewLayout: UICollectionViewLayout,
//                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 1.0
//    }
    
}

extension HomeViewController {
    
//    @objc func searchLocation() {
//
//        gmsAutoSearch = true
//
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//
//        let searchBarTextAttributes: [NSAttributedString.Key : AnyObject] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.black, NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): UIFont.systemFont(ofSize: UIFont.systemFontSize)]
//        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
//        //or
//        //UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//
//        //UInt(GMSPlaceField.name.rawValue)
//
//        // Specify the place data types to return.
//        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.coordinate.rawValue) |
//            UInt(GMSPlaceField.formattedAddress.rawValue))!
//        autocompleteController.placeFields = fields
//
//        // Specify a filter.
//        let filter = GMSAutocompleteFilter()
//        filter.type = .address
//        filter.type = .city
//        filter.type = .geocode
//        autocompleteController.autocompleteFilter = filter
//
//        print("Check =====>\(filter)")
//
//        // Display the autocomplete view controller.
//        autocompleteController.modalPresentationStyle = .fullScreen
//        present(autocompleteController, animated: true, completion: nil)
//
//    }
//
//    func setButtonsINSearchController() {
//        let button = UIButton(type: UIButton.ButtonType.custom)
//        button.setTitle("Cancel", for: UIControl.State.normal)
//        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
//        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
//        button.addTarget(self, action: #selector(cancelAction), for: UIControl.Event.touchUpInside)
//        let barButton = UIBarButtonItem(customView: button)
//        //self.navigationItem.rightBarButtonItem = barButton
//        self.navigationItem.leftBarButtonItem = barButton
//
//    }
//
//    @objc func cancelAction(sender: UIButton!) {
//
//        navigationController?.dismiss(animated: true, completion: nil)
//    }
}


extension HomeViewController : locationManagerProtocol {
    
    // getting current location from delegate.
    func returnCurrentLocation(newUserLocation userLocation: CLLocation) {
        print("Return locatino")
        
        print(userLocation.coordinate.longitude)
        print(userLocation.coordinate.latitude)
        
        
        let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude, zoom: 12.0)
        //               let mapVw = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        //               self.view.addSubview(mapVw)
        
        self.viewHome.mapVw.camera = camera
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        marker.title = "Sydney"
        marker.snippet = "Australia"
        marker.map = self.viewHome.mapVw
        
    }
    
        
    
}


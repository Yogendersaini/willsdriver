//
//  SideMenuViewController.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 10/04/20.
//  Copyright © 2020 Harjit Singh Mac. All rights reserved.
//

import UIKit
import LGSideMenuController

class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var sideMenuTblVw: UITableView!
    @IBOutlet weak var kmDataLbl: UILabel!
    @IBOutlet weak var welcomenameLbl: UILabel!
    @IBOutlet weak var kmLbl: UILabel!
    @IBOutlet weak var ridesLbl: UILabel!
    @IBOutlet weak var ridesDataLbl: UILabel!
    @IBOutlet weak var earnMoneyTrekkLbl: UILabel!
    
    var titlesArr = [String]()
    var imagesArr = [String]()
    var tokenKey = String()
    var kiloMeter = String()
    var rides = String()
    var profileDataDict12 : [String: Any] = [:]
    var welCome = String()
    var dashBoardData = [String: Any]()
    var arrayTableViewOptions = ["Home_MenuKey", "Home_MenuKey", "History_MenuKey", "Notifications_MenuKey", "Invite_Friends_MenuKey", "Choose_Language_MenuKey",  "Settings_MenuKey", "Logout_MenuKey"]
    
    var arrayImgTableViewOptions = ["homeSM", "walletSM", "historySM" , "notificationSM", "invitefriendsSM" , "global","settingsSM", "logoutSM"]

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layoutDesign()
        self.dashBoardData = [String: Any]()

        self.apiCalling_GetRide()
        NotificationCenter.default.addObserver(self, selector: #selector(apiCalling_GetRide), name: NSNotification.Name("isProfileUpdated"), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        nsNotifications()
        
        if let data = UserDefaults.standard.value(forKey: "LOGIN_RESPONSE") {
            let loginData = data as! NSDictionary
            tokenKey = loginData ["token_key"] as? String ?? ""
        }
        
        sidemenuTableviewCells()
        layoutDesign()
        
    }
    override func viewDidLayoutSubviews() {
        // Enable scrolling based on content height
        sideMenuTblVw.isScrollEnabled = sideMenuTblVw.contentSize.height > sideMenuTblVw.frame.size.height
    }
    //    override func viewDidLayoutSubviews() {
    //        sideMenuTblVw.frame.size = sideMenuTblVw.contentSize
    //    }
    
    //MARK: LAYOUT DESIGN
    @objc func layoutDesign() {
                
//        var arrayTableViewOptions = ["profile picture", "MY PROFILE", "CHANGE LANGUAGE", "BOOK SMART CAB", "MY TRIPS", "PAYMENTS", "WALLET", "EMERGENCY CONTACTS", "REFER AND EARN", "NOTIFICATIONS", "HELP", "ABOUT THE WILLS SMART CAB APP", "LOGOUT"]

        titlesArr = [L102Language.AMLocalizedString(key: "wallet", value: ""), L102Language.AMLocalizedString(key: "ride_history", value: ""),  L102Language.AMLocalizedString(key: "how_to_ride", value: ""), L102Language.AMLocalizedString(key: "help", value: ""), L102Language.AMLocalizedString(key: "About_Us", value: ""), L102Language.AMLocalizedString(key: "settings", value: ""), L102Language.AMLocalizedString(key: "logout", value: "my_wallet"),]
        

        
        
        sideMenuTblVw.separatorStyle = .none
        
        sideMenuTblVw.delegate = self
        sideMenuTblVw.dataSource = self
        sideMenuTblVw.reloadData()
    }
    
    func sidemenuTableviewCells(){
        
        sideMenuTblVw.register(UINib(nibName: "OptionsTbCell", bundle: nil), forCellReuseIdentifier: "OptionsTbCell")
        
        sideMenuTblVw.register(UINib(nibName: "ProfileInfoTbCell", bundle: nil), forCellReuseIdentifier: "ProfileInfoTbCell")
        sideMenuTblVw.bounces = false
                
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayTableViewOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 215
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let distanceCell = sideMenuTblVw.dequeueReusableCell(withIdentifier: "ProfileInfoTbCell", for: indexPath) as! ProfileInfoTbCell
            distanceCell.selectionStyle = .none
            distanceCell.setupData(dict: self.dashBoardData)
            return distanceCell
        } else {
            let earnCell = sideMenuTblVw.dequeueReusableCell(withIdentifier: "OptionsTbCell", for: indexPath) as! OptionsTbCell
            earnCell.selectionStyle = .none
            earnCell.lblOption.text = L102Language.AMLocalizedString(key: self.arrayTableViewOptions[indexPath.row], value: "")
                
            earnCell.imageOption.image = UIImage(named: self.arrayImgTableViewOptions[indexPath.row])
            return earnCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            
            let mainViewController = self.sideMenuController
            
            if indexPath.row == 0 {
                let pVC = ProfileViewController.getInstance()
                self.navigationController?.pushViewController(pVC, animated: true)

            } else if indexPath.row == 1 {
                
//            } else if indexPath.row == 2 {
//                let wVC = MyWalletViewController.getInstance()
//                self.navigationController?.pushViewController(wVC, animated: true)
//
            } else if indexPath.row == 2 {
                let wVC = HistoryViewController.getInstance()
                self.navigationController?.pushViewController(wVC, animated: true)

            } else if indexPath.row == 3 {
                let tVc = NotificationViewCont.getInstance()
                self.navigationController?.pushViewController(tVc, animated: true)

            } else if indexPath.row == 4 {
                let pVc = InviteViewController.getInstance()
                self.navigationController?.pushViewController(pVc, animated: true)
                             
            } else if indexPath.row == 5 {
                let pVc = LanguageViewController.getInstance()
                self.navigationController?.pushViewController(pVc, animated: true)
                
            } else if indexPath.row == 6 {
                
                let wVC = SettingssViewController.getInstance()
                self.navigationController?.pushViewController(wVC, animated: true)

            }
            else if indexPath.row == 7 {
                self.logout()                
            }
            
            mainViewController?.hideLeftView(animated: true, completionHandler: nil)
        }
            
    }
    
    
    @objc func profileData(){
        
        if let data1 = UserDefaults.standard.value(forKey: "PROFILE_RESPONSE1") {
            
            let profileData = data1 as! NSDictionary
            //  print("PROFILEDATA: =====> \(profileData)")
            rides = profileData["no_of_rides"]  as? String ?? ""
            
            kiloMeter = profileData["no_of_kilometres"] as? String ?? ""
            welCome = profileData["name"] as? String ?? ""
            print("KILOMETER: ====> \(self.kiloMeter)")
            print(("rides: =====>\(self.rides)"))
            sideMenuTblVw.reloadData()
            
        }
    }
    
    @objc func nsNotifications() {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(profileData), name: NSNotification.Name(REFRESHPROFILEDATA), object: nil)
    }
    
    //    func localization() {
    //
    //        self.ridesDataLbl.text = (L102Language.AMLocalizedString(key: "rides", value: ""))
    //        self.kmDataLbl.text = (L102Language.AMLocalizedString(key: "kilometers", value: ""))
    //        self.earnMoneyTrekkLbl.text = (L102Language.AMLocalizedString(key: "earn_with_treek", value: ""))
    //
    //    }
    static func getInstance() -> SideMenuViewController {
        
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
    }
//    @objc func setFirstLable() {
//        let text = String(format: "%@ Rides", rides)
//        let attributedText = NSMutableAttributedString.getAttributedString(fromString: text)
//        attributedText.apply(color: .red, subString: "%@")
//         attributedText.apply(color: .darkGray, onRange: NSMakeRange(5, 4)) //Range of substring "is a"
//        attributedText.apply(color: .black, subString: "Rides")
//
//        self.ridesLbl.attributedText = attributedText
//    }
    
    func logout() {
        
        let alert = UIAlertController(title:(L102Language.AMLocalizedString(key: "logout", value: "")) , message: (L102Language.AMLocalizedString(key: "want_to_logout", value: "")),preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "no", value: "")), style: UIAlertAction.Style.default, handler: { _ in
            
            
        }))
        
        alert.addAction(UIAlertAction(title: (L102Language.AMLocalizedString(key: "yes", value: "")),
                                      style: UIAlertAction.Style.destructive,
                                      handler: {(_: UIAlertAction!) in
                                        UserDefaults.standard.set(false, forKey: "IS_LOGGED_IN")
                                        UserDefaults.standard.synchronize()
                                        

                                        RIDE_TIMER_GLOBEL?.invalidate()
                                        self.apiCalling_Logout()
                                        
                                        let appDelegate = UIApplication.shared.delegate
                                        appDelegate?.window??.rootViewController = UIStoryboard(name: "Login", bundle: Bundle.main).instantiateInitialViewController()

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func apiCalling_Logout() {
        
        let params = [ "token" : AppHelper.getUserToken(), "client_token" : AppHelper.getClientToken()]
        
        AppHelper.apiCallingForDict(apiName: Logout, param: params, viewCont: self, successDict: { (successDict, resultDict) in
            
            self.resetUserDefaults()
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = UIStoryboard(name: "Login", bundle: Bundle.main).instantiateInitialViewController()
            
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            if messageID == "157" {
                
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
                
            } else if messageID == "129" {
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
                
            } else {
                
                UIAlertController.showAlert(vc: self, title: "", message: message)
                
            }
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    @objc func apiCalling_GetRide() {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "ride_id" : "", "extended" : "1"]
        
                
        AppHelper.apiCallingForDict_WithoutLoader(apiName: GetRide, param: param, viewCont: self, jsonResponce: { (resultDict) in
            
            self.dashBoardData = resultDict["dashboard_data"] as! [String : Any]
            let objRideModel = RideModel.init(with: resultDict as [String : Any])
            PROFILE_DATA_GLOBEL = objRideModel.dashboard_data
            
            self.sideMenuTblVw.reloadData()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message, jsonResponce) in
            
            if let dashBoard =  jsonResponce["dashboard_data"] as? [String : Any] {
                self.dashBoardData = jsonResponce["dashboard_data"] as! [String : Any]
                let objRideModel = RideModel.init(with: jsonResponce as! [String : Any])
                PROFILE_DATA_GLOBEL = objRideModel.dashboard_data

            }
////            self.dashBoardData = jsonResponce["dashboard_data"] as! [String : Any]
//            let objRideModel = RideModel.init(with: jsonResponce as! [String : Any])
//            PROFILE_DATA_GLOBEL = objRideModel.dashboard_data

            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    
    @objc func apiCalling_GetProfile() {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken()]
        
        AppHelper.apiCallingForDict_WithoutLoader(apiName: GetProfile, param: param, viewCont: self, jsonResponce: { (successDict) in
            print("Get proifle callse")
            print(successDict)
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message, jsonResponce) in
            
//            if messageID == "157" {
                //                        UIAlertController.showAlert(vc: self.vc, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
//            } else if messageID == "129" {
                //                        UIAlertController.showAlert(vc: self.vc, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
//            } else {
                    UIAlertController.showAlert(vc: self, title: "", message: message)
//            }
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    

}

/*
 {
     data =     {
         "dial_code" = "+91";
         dob = "01 Jun, 2020";
         email = "driver1@gmail.com";
         "first_name" = Test;
         "full_name" = Driver1;
         gender = male;
         id = 1;
         image = "http://intellisensetechnologies.com/smartcab/uploads/images/5eb3e217768px-User_icon_2.svg.png";
         "is_approved" = 1;
         "is_verified" = 1;
         lang = "";
         "last_name" = Driver;
         phone = 9928791990;
         token = 7e3edd43eb73a27d389bd233b8b58710;
         username = Test;
     };
     "debug_id" = 7b5bfa93a33fe87dd608719ab84fb9c9;
     message = "Success (RM_00092)";
     "message_id" = 93;
     status = 1;
 }
 */

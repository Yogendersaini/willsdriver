//
//  OptionsTbCell.swift
//  WillsSmartCabRider
//
//  Created by Yogender Saini on 2/20/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class OptionsTbCell: UITableViewCell {

    @IBOutlet weak var imageOption : UIImageView!
    @IBOutlet weak var lblOption : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

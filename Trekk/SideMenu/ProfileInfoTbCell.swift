//
//  ProfileInfoTbCell.swift
//  WillsSmartCabRider
//
//  Created by Yogender Saini on 2/20/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class ProfileInfoTbCell: UITableViewCell {


    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblLevelType: UILabel!
    @IBOutlet weak var lblhrsOnline: UILabel!
    @IBOutlet weak var lblTotalDist: UILabel!
    @IBOutlet weak var lblJobs: UILabel!
    
    @IBOutlet weak var lblTitleHrs: UILabel!
    @IBOutlet weak var lblTitleDist: UILabel!
    @IBOutlet weak var lblTitleJobs: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData( dict : [String : Any])  {
        

        // making array in local because when language is chenged that take reload automtically.
        
        let arrTitlesCollItem = [(L102Language.AMLocalizedString(key: "HOURSE_ONLINE", value: "")),
        (L102Language.AMLocalizedString(key: "TOTAL_DISTANCE", value: "")),
        (L102Language.AMLocalizedString(key: "TOTAL_JOBS", value: ""))]


        self.imgProfile.sd_setImage(with: URL(string: dict["image"] as? String ?? ""), placeholderImage: UIImage(named: "profile_pic"))
        self.lblDriverName.text = dict["full_name"] as? String ?? ""
        self.lblhrsOnline.text = dict["total_online_hours"] as? String ?? ""
        self.lblTotalDist.text = dict["total_distance"] as? String ?? ""
        self.lblJobs.text = dict["total_jobs"] as? String ?? ""
        
        self.lblTitleHrs.text = arrTitlesCollItem[0]
        self.lblTitleDist.text = arrTitlesCollItem[1]
        self.lblTitleJobs.text =  arrTitlesCollItem[2]
        
    }
    
}


/*
 "dashboard_data": {
     "full_name" = "Test Driver";
     image = "http://intellisensetechnologies.com/smartcab/uploads/images/60a1f7492d167image.png";
     "is_online" = 0;
     "total_distance" = "7.1 km";
     "total_earning" = "$21.40";
     "total_jobs" = 17;
     "total_online_hours" = "8415.11";
 }
 */

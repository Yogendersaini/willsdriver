//
//  HistoryDataModel.swift
//  Trekk
//
//  Created by Yogender Saini on 11/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class HistoryDataModel: NSObject {

    var created_at   : String = ""
    var driver_id   : String = ""
    var distance   : String = ""
    
    var end_location   : String = ""
    var id   : String = ""
    var ride_id   : String = ""
    
    var rider_id   : String = ""
    var ride_number   : String = ""
    var ride_running_time   : String = ""

    var rider_image   : String = ""
    var rider_name   : String = ""
    var rider_phone   : String = ""

    var start_location   : String = ""
    var status   : String = ""
    var total_bill   : String = ""

    override init() {
        super.init()
    }
    
    init(with dict: [String: Any]) {
        super.init()
        created_at = dict["created_at"] as? String ?? ""
        driver_id  = dict["driver_id"] as? String ?? ""
        distance = dict["distance"] as? String ?? ""
        
        end_location   = dict["end_location"] as? String ?? ""
        id = dict["id"] as? String ?? ""
        ride_id = dict["ride_id"] as? String ?? ""
        
        rider_id = dict["rider_id"] as? String ?? ""
        ride_number = dict["ride_number"] as? String ?? ""
        ride_running_time = dict["ride_running_time"] as? String ?? ""

        rider_image = dict["rider_image"] as? String ?? ""
        rider_name = dict["rider_name"] as? String ?? ""
        rider_phone = dict["rider_phone"] as? String ?? ""

        start_location = dict["start_location"] as? String ?? ""
        status = dict["status"] as? String ?? ""
        total_bill = dict["total_bill"] as? String ?? ""

    }
    
}

/*
 {
     "created_at" = "07-May-2021";
     distance = "0 km";
     "driver_id" = 1;
 
     "end_location" = "Jhotwara, Jaipur, Rajasthan";
     id = 34;
     "ride_number" = 2560C4;
 
     "ride_running_time" = "00:00:00";
     "rider_id" = 93;
    "rider_number" = 1C304A;

     "rider_image" = "http://intellisensetechnologies.com/smartcab/uploads/images/60656323562a1image.png";
     "rider_name" = "Anaya Singh";

     "rider_phone" = "+918619077641";
     "start_location" = "468/1, 4 Number Dispensary, Shanti Nagar, Civil Lines, Jaipur, Rajasthan 302006, India";
     status = complete;
     "total_bill" = "$12.00";
 }
 */

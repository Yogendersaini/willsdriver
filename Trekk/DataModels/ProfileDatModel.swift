//
//  ProfileDatModel.swift
//  Trekk
//
//  Created by Yogender Saini on 10/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class ProfileDatModel: NSObject {
    
    var dial_code   : String = ""
    var dob   : String = ""
    var email   : String = ""
    var first_name   : String = ""
    var full_name   : String = ""
    var gender   : String = ""
    var id   : String = ""
    var image   : String = ""
    var is_approved   : String = ""
    var is_verified   : String = ""
    var lang   : String = ""
    var last_name   : String = ""
    var phone   : String = ""
    var token   : String = ""
    var username   : String = ""
    
    override init() {
        super.init()
    }
    
    init(with dict: [String: Any]) {
        super.init()
        dial_code = dict["dial_code"] as? String ?? ""
        dob  = dict["dob"] as? String ?? ""
        email = dict["email"] as? String ?? ""
        first_name   = dict["first_name"] as? String ?? ""
        full_name = dict["full_name"] as? String ?? ""
        gender = dict["gender"] as? String ?? ""
        id = dict["id"] as? String ?? ""
        image = dict["image"] as? String ?? ""
        is_approved = dict["is_approved"] as? String ?? ""
        is_verified = dict["is_verified"] as? String ?? ""
        lang = dict["lang"] as? String ?? ""
        last_name = dict["last_name"] as? String ?? ""
        phone = dict["phone"] as? String ?? ""
        token = dict["token"] as? String ?? ""
        username = dict["username"] as? String ?? ""

    }
    
}


/*
{
    data =     {
        "dial_code" = "+91";
        dob = "01 Jun, 2020";
        email = "driver1@gmail.com";
        "first_name" = Test;
        "full_name" = Driver1;
        gender = male;
        id = 1;
        image = "http://intellisensetechnologies.com/smartcab/uploads/images/5eb3e217768px-User_icon_2.svg.png";
        "is_approved" = 1;
        "is_verified" = 1;
        lang = "";
        "last_name" = Driver;
        phone = 9928791990;
        token = 7e3edd43eb73a27d389bd233b8b58710;
        username = Test;
    };
    "debug_id" = 7b5bfa93a33fe87dd608719ab84fb9c9;
    message = "Success (RM_00092)";
    "message_id" = 93;
    status = 1;
}
*/

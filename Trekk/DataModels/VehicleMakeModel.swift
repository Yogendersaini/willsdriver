//
//  VehicleMakeModel.swift
//  Trekk
//
//  Created by Yogender Saini on 12/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class VehicleMakeModel: NSObject {

    var id   : String = ""
    var make_name   : String = ""
    var image   : String = ""

    override init() {
        super.init()
    }
    
    init(with dict: [String: Any]) {
        super.init()
        image  = dict["image"] as? String ?? ""
        id = dict["id"] as? String ?? ""
        make_name = dict["make_name"] as? String ?? ""
    }
    
}


/*
 BEGIN Trekk======>URLSTRINGhttp://intellisensetechnologies.com/smartcab/api/driver/get_vehicle_make
 BEGIN Trekk=====Parameters>["client_token": "abcdefghijklmnopqrstuvwxyz", "token": "083aa89db5c3fede3749f8d1033efbd3"]
 Upload Progress: 1.0
 Success!
 {
     "current_page" = 0;
     data =     (
                 {
             id = 3;
             image = "http://intellisensetechnologies.com/smartcab/uploads/images/5e885f7fth (4).jpg";
             "make_name" = Bmw;
         },
                 {
             id = 2;
             image = "http://intellisensetechnologies.com/smartcab/uploads/images/5e885faeth (5).jpg";
             "make_name" = Toyota;
         }
     );
     "debug_id" = 963d2a3b6d3d1f1722c3443759c79b55;
     "fetched_rows_count" = 2;
     message = "Success (RM_00035)";
     "message_id" = 36;
     "pages_count" = 0;
     "per_page_rows_count" = 0;
     status = 1;
     "total_rows_count" = 4;
 }
 
 */

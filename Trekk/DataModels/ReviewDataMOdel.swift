//
//  ReviewDataMOdel.swift
//  Trekk
//
//  Created by Yogender Saini on 11/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class ReviewDataMOdel: NSObject {

    var created_at   : String = ""
    var rating   : String = ""
    var review   : String = ""
    
    var id   : String = ""
    var rider_image   : String = ""
    var rider_name   : String = ""
    
    override init() {
        super.init()
    }
    
    init(with dict: [String: Any]) {
        super.init()
        created_at = dict["created_at"] as? String ?? ""
        rating  = dict["rating"] as? String ?? ""
        review = dict["review"] as? String ?? ""
        
        id = dict["id"] as? String ?? ""
        rider_image = dict["rider_image"] as? String ?? ""
        rider_name = dict["rider_name"] as? String ?? ""

    }
    
}

/*
 {
     "created_at" = "2021-05-08 16:35:27";
     id = 45;
     rating = 4;
     review = "";
     "rider_image" = "http://intellisensetechnologies.com/smartcab/uploads/images/60656323562a1image.png";
     "rider_name" = "Anaya Singh";
 },
 */

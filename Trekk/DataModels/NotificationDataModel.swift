//
//  NotificationDataModel.swift
//  Trekk
//
//  Created by Yogender Saini on 10/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

public enum rideStatus : String {
    case ride_completed = "ride_completed"
    case ride_accepted = "ride_accepted"
    case ride_request = "ride_request"
    case ride_cancelled = "ride_cancelled"
}


class NotificationDataModel: NSObject {

    var created_at   : String = ""
    var driver_id   : String = ""
    var is_read   : String = ""
    var message   : String = ""
    var notification_id   : String = ""
    
    var ride_id   : String = ""
    var rider_id   : String = ""
    var title   : String = ""
    var type   : String = ""
            
    override init() {
        super.init()
    }
    
    init(with dict: [String: Any]) {
        super.init()
        created_at = dict["created_at"] as? String ?? ""
        driver_id  = dict["driver_id"] as? String ?? ""
        is_read = dict["is_read"] as? String ?? ""
        message   = dict["message"] as? String ?? ""
        notification_id = dict["notification_id"] as? String ?? ""
        
        ride_id = dict["ride_id"] as? String ?? ""
        rider_id = dict["rider_id"] as? String ?? ""
        title = dict["title"] as? String ?? ""
        type = dict["type"] as? String ?? ""
        
    }
    
}


/*
 "created_at" = "08-May-2021";
 "driver_id" = 1;
 "is_read" = 0;
 message = "Thank you! Your transaction has been completed";
 "notification_id" = 2171;
 "ride_id" = 36;
 "rider_id" = 0;
 title = "Payment Successfull";
 type = "ride_completed";
 */

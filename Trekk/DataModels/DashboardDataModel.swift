//
//  DashboardDataModel.swift
//  Trekk
//
//  Created by Yogender Saini on 06/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class DashboardDataModel: NSObject {
    
    var full_name   : String = ""
    var image   : String = ""
    var is_online   : String = ""
    var total_distance   : String = ""
    var total_earning   : String = ""
    var total_jobs   : String = ""
    var total_online_hours   : String = ""
    
    
    override init() {
        super.init()
    }
    
    
    init(with dict: [String: Any])
    {
        super.init()
        full_name = dict["full_name"] as? String ?? ""
        image = dict["image"] as? String ?? ""
        is_online = dict["is_online"] as? String ?? ""
        total_distance = dict["total_distance"] as? String ?? ""
        total_earning = dict["total_earning"] as? String ?? ""
        total_jobs = dict["total_jobs"] as? String ?? ""
        total_online_hours = dict["total_online_hours"] as? String ?? ""
    }
    
    //    full_name
    //    image
    //    is_online
    //    total_distance
    //    total_earning
    //    total_jobs
    //    total_online_hours
    
}

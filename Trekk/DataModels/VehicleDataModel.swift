//
//  VehicleDataModel.swift
//  Trekk
//
//  Created by Yogender Saini on 12/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class VehicleDataModel: NSObject {

    var booking_type   : String = ""
    var color   : String = ""
    var id   : String = ""
    
    var is_activated   : String = ""
    var license_plate_no   : String = ""
    var make_name   : String = ""

    var mfg_year   : String = ""
    var model_name   : String = ""
    var rider_name   : String = ""

    var size   : String = ""
    var type_name   : String = ""
    var vehicle_model_id   : String = ""

    override init() {
        super.init()
    }
    
    init(with dict: [String: Any]) {
        super.init()
        booking_type = dict["booking_type"] as? String ?? ""
        color  = dict["color"] as? String ?? ""
        id = dict["id"] as? String ?? ""
        
        is_activated = dict["is_activated"] as? String ?? ""
        license_plate_no = dict["license_plate_no"] as? String ?? ""
        make_name = dict["make_name"] as? String ?? ""

        mfg_year = dict["mfg_year"] as? String ?? ""
        model_name = dict["model_name"] as? String ?? ""
        size = dict["size"] as? String ?? ""

        type_name = dict["type_name"] as? String ?? ""
        vehicle_model_id = dict["vehicle_model_id"] as? String ?? ""

    }
    
}


/*
 "booking_type" = car;
 color = 123456;
 id = 13;
 "is_activated" = 0;
 
 "license_plate_no" = "red\n";
 "make_name" = Bmw;
 "mfg_year" = 1900;
 
 "model_name" = "BMW Car";
 size = Medium;
 "type_name" = Car;
 "vehicle_model_id" = 5;

 */

//
//  VehicleTypeModel.swift
//  Trekk
//
//  Created by Yogender Saini on 12/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class VehicleTypeModel: NSObject {

    var distance_unit   : String = ""
    var image   : String = ""
    var id   : String = ""
    var model_name   : String = ""
    var size   : String = ""
    var type_name   : String = ""
    var vehicle_fare   : String = ""

    override init() {
        super.init()
    }
    
    init(with dict: [String: Any]) {
        super.init()
        distance_unit = dict["distance_unit"] as? String ?? ""
        image  = dict["image"] as? String ?? ""
        id = dict["id"] as? String ?? ""
                
        model_name = dict["model_name"] as? String ?? ""
        size = dict["size"] as? String ?? ""

        type_name = dict["type_name"] as? String ?? ""
        vehicle_fare = dict["vehicle_fare"] as? String ?? ""

    }
    
}



/*
 
 BEGIN Trekk======>URLSTRINGhttp://intellisensetechnologies.com/smartcab/api/driver/get_vehicle_models
 BEGIN Trekk=====Parameters>["vehicle_brand_id": "1", "client_token": "abcdefghijklmnopqrstuvwxyz", "token": "083aa89db5c3fede3749f8d1033efbd3"]
 
 Success!
 {
     "current_page" = 0;
     data =     (
                 {
             "distance_unit" = km;
             id = 4;
             image = "http://intellisensetechnologies.com/smartcab/uploads/images/5ebd08fbSuzuki-Swift-(2).jpg";
             "model_name" = Swift;
             size = Small;
             "type_name" = Car;
             "vehicle_fare" = "10.00";
         },
                 {
             "distance_unit" = km;
             id = 1;
             image = "http://intellisensetechnologies.com/smartcab/uploads/images/5eb3e06fth (7).jpg";
             "model_name" = "Crane Model";
             size = Small;
             "type_name" = Crane;
             "vehicle_fare" = "150.00";
         }
     );
     "debug_id" = b523f18463ad1b77bc415bb61ed5ac70;
     "fetched_rows_count" = 2;
     message = "Success (RM_00035)";
     "message_id" = 36;
     "pages_count" = 0;
     "per_page_rows_count" = 0;
     status = 1;
     "total_rows_count" = 0;
 }
 */

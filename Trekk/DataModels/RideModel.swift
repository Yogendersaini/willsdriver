//
//  RideModel.swift
//  Trekk
//
//  Created by Yogender Saini on 06/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class RideModel: NSObject {
    
    var accepted = AcceptDataModel()
    var dashboard_data = DashboardDataModel()
    var payment_done = PaymentDoneModel()
    var requested = RequestedDataModel()
    var ride_ended = RideEndedModel()
    var ride_started = RideStartedModel()
    
    var debug_id   : String = ""
    var is_accepted   : String = ""
    var is_online  : String = ""
    var is_payment_done  : String = ""
    var is_requested  : String = ""
    var is_ride_ended  : String = ""
    var is_ride_started  : String = ""
    var message  : String = ""
    var message_id  : String = ""
    var request_time_left  : String = ""
    var status  : String = ""
            
    override init() {
        super.init()
    }
    
    
    init(with dict: [String: Any]) {
        super.init()
        
        accepted =  AcceptDataModel.init(with: (dict["accepted"] as? [String : Any]  ?? [:]))
        dashboard_data = DashboardDataModel.init(with: dict["dashboard_data"] as? [String : Any] ?? [:])
        payment_done = PaymentDoneModel.init(with: dict["payment_done"]  as? [String : Any]  ?? [:])
        requested = RequestedDataModel.init(with: dict["requested"]  as? [String : Any]  ?? [:])
        ride_ended = RideEndedModel.init(with: dict["ride_ended"]  as? [String : Any]  ?? [:])
        ride_started = RideStartedModel.init(with: dict["ride_started"]  as? [String : Any]  ?? [:])
        
        debug_id = dict["debug_id"] as? String ?? ""
        is_accepted = dict["is_accepted"] as? String ?? ""
        is_online = dict["is_online"] as? String ?? ""
        is_payment_done = dict["is_payment_done"] as? String ?? ""
        is_requested = dict["is_requested"] as? String ?? ""
        is_ride_ended = dict["is_ride_ended"] as? String ?? ""
        is_ride_started = dict["is_ride_started"] as? String ?? ""
        message = dict["message"] as? String ?? ""
        message_id = dict["message_id"] as? String ?? ""
        request_time_left = dict["request_time_left"] as? String ?? ""
        status = dict["status"] as? String ?? ""
        
    }
    
    
}



/*
 
 Success!
 {
     accepted =     {
     };
     "dashboard_data" =     {
         "full_name" = Driver1;
         image = "http://intellisensetechnologies.com/smartcab/uploads/images/5eb3e217768px-User_icon_2.svg.png";
         "is_online" = 1;
         "total_distance" = "4.1 km";
         "total_earning" = "$9.20";
         "total_jobs" = 7;
         "total_online_hours" = "8122.55";
     };
     "debug_id" = 79f37e74e5d5bf215574af35e43fa291;
     "is_accepted" = 0;
     "is_online" = 1;
     "is_payment_done" = 0;
     "is_requested" = 1;
     "is_ride_ended" = 0;
     "is_ride_started" = 0;
     message = "Success (RM_00065)";
     "message_id" = 66;
     "payment_done" =     {
     };
     "request_time_left" = "00:00:06";
     requested =     {
         "base_fare" = "$10.00";
         "created_at" = "06-May-2021 03:55 PM";
         distance = "3.4 km";
         "distance_fare" = "$0.00";
         "driver_id" = 1;
         "driver_rating" = "3.59";
         "driver_rating_comment" = "";
         "driver_rating_date" = "";
         "drop_time" = "05:30 AM";
         duration = "13 min:10 sec";
         "end_lat" = "26.90158000";
         "end_location" = "Sodala, Jaipur, Rajasthan";
         "end_lon" = "75.77157000";
         "est_distance_meters" = 0;
         "est_distance_text" = "";
         "est_duration_secs" = 0;
         "est_duration_text" = "";
         id = 23;
         notes = Xhxjcjxjxkck;
         "pickup_time" = "05:30 AM";
         "raw_created_at" = "2021-05-06 15:55:23";
         "ride_cost" = "$0.00";
         "ride_number" = 2560BF;
         "ride_running_time" = "";
         "rider_driver_distance" = "0 km";
         "rider_driver_duration" = "2 sec";
         "rider_id" = 93;
         "rider_image" = "http://intellisensetechnologies.com/smartcab/uploads/images/60656323562a1image.png";
         "rider_name" = "Anaya Singh";
         "rider_number" = 1C304A;
         "rider_phone" = "+918619077641";
         "rider_rating" = "2.81";
         "rider_rating_comment" = "";
         "rider_rating_date" = "";
         "start_lat" = "26.91268964";
         "start_location" = "33, Shanti Nagar, Civil Lines, Jaipur, Rajasthan 302006, India";
         "start_lon" = "75.77560993";
         status = pending;
         tax = "$0.00";
         "time_fare" = "$0.00";
         "total_bill" = "$0.00";
     };
     "ride_ended" =     {
     };
     "ride_started" =     {
     };
     status = 1;
 }
 
 */

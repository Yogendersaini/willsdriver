//
//  RequestedDataModel.swift
//  Trekk
//
//  Created by Yogender Saini on 06/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class RequestedDataModel: NSObject {
    
    var base_fare   : String = ""
    var created_at  : String = ""
    var distance  : String = ""
    var distance_fare  : String = ""
    var driver_id  : String = ""
    var driver_rating  : String = ""
    var driver_rating_comment   : String = ""
    var driver_rating_date  : String = ""
    var drop_time  : String = ""
    var duration  : String = ""
    var end_lat  : String = ""
    var end_location  : String = ""
    var end_lon  : String = ""
    var est_distance_meters  : String = ""
    var est_distance_text  : String = ""
    var est_duration_secs  : String = ""
    var est_duration_text  : String = ""
    var ride_id  : String = ""
    var notes  : String = ""
    var pickup_time  : String = ""
    var raw_created_at  : String = ""
    var ride_cost  : String = ""
    var ride_number  : String = ""
    var ride_running_time  : String = ""
    var rider_driver_distance  : String = ""
    var rider_driver_duration  : String = ""
    var rider_id  : String = ""
    var rider_image  : String = ""
    var rider_name  : String = ""
    var rider_number  : String = ""
    var rider_phone  : String = ""
    var rider_rating  : String = ""
    var rider_rating_comment  : String = ""
    var rider_rating_date  : String = ""
    var start_lat  : String = ""
    var start_location  : String = ""
    var start_lon  : String = ""
    var status  : String = ""
    var tax  : String = ""
    var time_fare  : String = ""
    var total_bill  : String = ""
    var taxi_image  : String = ""
    var taxi_number  : String = ""
    


    
    
    override init() {
        super.init()
    }
    
    
    init(with dict: [String: Any])
    {
        super.init()
        base_fare  = dict["base_fare"] as? String ?? ""
        created_at = dict["created_at"] as? String ?? ""
        distance = dict["distance"] as? String ?? ""
        distance_fare = dict["distance_fare"] as? String ?? ""
        driver_id = dict["driver_id"] as? String ?? ""
        driver_rating = dict["driver_rating"] as? String ?? ""
        driver_rating_comment = dict["driver_rating_comment"] as? String ?? ""
        driver_rating_date = dict["driver_rating_date"] as? String ?? ""
        drop_time = dict["drop_time"] as? String ?? ""
        duration = dict["duration"] as? String ?? ""
        end_lat = dict["end_lat"] as? String ?? ""
        end_location = dict["end_location"] as? String ?? ""
        end_lon = dict["end_lon"] as? String ?? ""
        est_distance_meters = dict["est_distance_meters"] as? String ?? ""
        est_distance_text = dict["est_distance_text"] as? String ?? ""
        est_duration_secs = dict["est_duration_secs"] as? String ?? ""
        est_duration_text = dict["est_duration_text"] as? String ?? ""
        ride_id = dict["id"] as? String ?? ""
        notes = dict["notes"] as? String ?? ""
        pickup_time = dict["pickup_time"] as? String ?? ""
        raw_created_at = dict["raw_created_at"] as? String ?? ""
        ride_cost = dict["ride_cost"] as? String ?? ""
        ride_number = dict["ride_number"] as? String ?? ""
        ride_running_time = dict["ride_running_time"] as? String ?? ""
        rider_driver_distance = dict["rider_driver_distance"] as? String ?? ""
        rider_driver_duration = dict["rider_driver_duration"] as? String ?? ""
        rider_id = dict["rider_id"] as? String ?? ""
        rider_image = dict["rider_image"] as? String ?? ""
        rider_name = dict["rider_name"] as? String ?? ""
        rider_number = dict["rider_number"] as? String ?? ""
        rider_phone = dict["rider_phone"] as? String ?? ""
        rider_rating = dict["rider_rating"] as? String ?? ""
        rider_rating_comment = dict["rider_rating_comment"] as? String ?? ""
        rider_rating_date = dict["rider_rating_date"] as? String ?? ""
        start_lat = dict["start_lat"] as? String ?? ""
        start_location = dict["start_location"] as? String ?? ""
        start_lon = dict["start_lon"] as? String ?? ""
        status = dict["status"] as? String ?? ""
        tax = dict["tax"] as? String ?? ""
        time_fare = dict["time_fare"] as? String ?? ""
        total_bill = dict["total_bill"] as? String ?? ""
        
        
        taxi_image = dict["taxi_image"] as? String ?? ""
        taxi_number = dict["taxi_number"] as? String ?? ""

    }
    
        
}

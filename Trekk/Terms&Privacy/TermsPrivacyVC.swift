//
//  TermsPrivacyVC.swift
//  Trekk
//
//  Created by Yogender Saini on 17/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TermsPrivacyVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> TermsPrivacyVC {
        let storyboard = UIStoryboard(name: "TermsPrivacy",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "TermsPrivacyVC") as! TermsPrivacyVC

    }

    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAddActinos(_ sender: UIButton) {
        let vc = AddNewVechileVC.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)

    }

}

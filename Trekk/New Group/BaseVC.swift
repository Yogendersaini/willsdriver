//
//  BaseVC.swift
//  Wills Smart Cab Rider
//
//  Created by Sukhpreet Singh on 20/11/20.
//

import UIKit
//import JGProgressHUD

class BaseVC: UIViewController {
    
    var backButton = UIButton()
//    func leftBarItem(title: String, image: UIImage) {}
//
//    var leftBarTitles = [String]()
//    var leftBarImages = [UIImage]()
//
//    var barTitles = [String]()
//    var barImages = [UIImage]()
//
//    var stackView: UIStackView = {
//        let stack = UIStackView()
//        stack.axis = .horizontal
//        stack.alignment = .center
//        stack.distribution = .fill
//        return stack
//    }()
//
    var singleTap: UITapGestureRecognizer {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(resignKeyboard(_:)))
        tapGesture.delegate = self
        return tapGesture
    }
    
//    var hud: JGProgressHUD = JGProgressHUD(style: .dark)
    
    //MARK:- life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
                
//        if !(navigationController?.viewControllers.first == self) {
//            leftBarImages = [#imageLiteral(resourceName: "back"), #imageLiteral(resourceName: "Logo")]
//            navigationItem.leftBarButtonItems = leftBarItems()
//            navigationItem.leftBarButtonItems?.forEach {
//                (leftItem) in
//                   if let button = leftItem.customView as? UIButton, button.currentImage == #imageLiteral(resourceName: "back") {
//                       button.addTarget(self, action: #selector(backTouched(_:)), for: .touchUpInside)
//                   }
//            }
//        } else {
//            leftBarImages = [#imageLiteral(resourceName: "Logo")]
//            navigationItem.leftBarButtonItems = leftBarItems()
//        }
        
        view.addGestureRecognizer(singleTap)
        backButton.frame = CGRect(x: 314, y: 45, width: 100, height: 30)
        backButton.setImage(UIImage(named: "back"), for: .normal)
        self.view.addSubview(backButton)
        backButton.addTarget(self, action: #selector(backTouched(_:)), for: .touchUpInside)
        backButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    //MARK:- Touch Actions
    
    @objc func backTouched(_ sender: UIBarButtonItem?) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func resignKeyboard(_ gesture: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
}

extension BaseVC: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
//        if gestureRecognizer is UITapGestureRecognizer {
//            if let tableView = view.subviews(ofType: UITableView.self).first {
//                let location = touch.location(in: tableView)
//                return (tableView.indexPathForRow(at: location) == nil)
//            } else if let collectionView = view.subviews(ofType: UICollectionView.self).first {
//                let location = touch.location(in: collectionView)
//                return (collectionView.indexPathForItem(at: location) == nil)
//            }
//        }
        return true
    }
    
    func new() {
        
    }
}


//extension BaseVC: JPProgress {}

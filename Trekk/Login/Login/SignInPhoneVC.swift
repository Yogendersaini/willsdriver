//
//  SignInPhoneVC.swift
//  WillsSmartCabRider
//
//  Created by Yogender Saini on 2/18/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit
import ADCountryPicker
import FirebaseAuth
import CoreTelephony

class SignInPhoneVC: BaseVC {

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var lblFlagEmoji: UILabel!
    @IBOutlet weak var imgViewFlag : UIImageView!
    @IBOutlet weak var viewPhone : custViewTextField!
    
    var phoneNumberVerified = String()
    var VerificationID = String()

    let picker = ADCountryPicker(style: .grouped)
    var dailingCode = String()
    var dataDict : [String: Any] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.isHidden = false
        self.setupViews()
        self.localization()
     
        
    }
    
    
    
    func localization() {
        let btnTitle = (L102Language.AMLocalizedString(key: "login_Button", value: ""))
        self.btnLogin.setTitle(btnTitle, for: .normal)
        let attrStr = NSMutableAttributedString().getAttributedBoldString(str: (L102Language.AMLocalizedString(key: "Login_with_your_phone_number", value: "")), boldTxt: (L102Language.AMLocalizedString(key: "login_text", value: "")), size: 30)
        self.lblTitle.attributedText = attrStr
    }
    
    func setupViews()  {
        let paddingView3: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 130, height: 20))
        self.viewPhone.textField.leftView = paddingView3
        self.viewPhone.textField.leftViewMode = .always
        self.viewPhone.textField.keyboardType = .decimalPad
        countryLbl.text = "\(AppHelper.getCountryCode().1) \(AppHelper.getCountryCode().0)+"
        dailingCode = "+" + AppHelper.getCountryCode().0
        print("Counrty code :- - -- - - - - - - - - - - - - - - ->", AppHelper.getCountryCode())
        
        self.lblFlagEmoji.text = AppHelper.countryFlag(countryCode: AppHelper.getCountryCode().1)
    }

    static func getInstanceSignInPhoneVC() -> SignInPhoneVC {
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SignInPhoneVC") as! SignInPhoneVC
    }
    
         
    @IBAction func onTapCountryBtn(_ sender: UIButton) {
        self.picker.delegate = self
        picker.showCallingCodes = true
        picker.pickerTitle = (L102Language.AMLocalizedString(key: "select_country", value: ""))
        //picker.defaultCountryCode = "YT"
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.navigationController?.pushViewController(picker, animated: true)
        
    }
    
    @IBAction func btnSigninAction(_ sender : UIButton) {
        self.signIN()
    }

    func moveToHomePage() {
        let hvc = MainViewController.getInstance()
        hvc.setup(type: 1)
        self.navigationController?.pushViewController(hvc, animated: true)
    }
    
}
extension SignInPhoneVC: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        
        countryLbl.text = String(format: "%@ %@", code,dialCode)
        
        // countryImg.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode
        
        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")
        
        isCountrySelectedForRegister = true
        
        self.imgViewFlag.image = self.picker.getFlag(countryCode: code)
    
        self.navigationController?.popViewController(animated: true)
    }
}
extension SignInPhoneVC {
    
    func signIN()  {
        
        self.hideKeyBoard()
        self.viewPhone.textField.text = self.viewPhone.textField.text?.trimmingCharacters(in: .whitespaces)
        
        
        if self.viewPhone.textField.hasText {
        } else {
            self.view.makeToast ((L102Language.AMLocalizedString(key: "Mobile_number_validations", value: "")), duration: 2.0, position: .bottom, style: style)
            return
        }
        
        self.apiCalling_Login(phoneNumber: self.viewPhone.textField.text!)
    }
    
    
    func mobileNumberAuthenticationWithFirebase() {
           phoneNumberVerified =  String(format: "%@%@",dailingCode,self.viewPhone.textField.text ?? "")
           print("Register Mobile Number--------->>>>>\(phoneNumberVerified)")
           
           PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumberVerified, uiDelegate: nil) { (verificationID, error) in
               
               if (error != nil) {
               
                   ERROR_MESSAGE = error?.localizedDescription ?? ""
                   UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: ERROR_MESSAGE)
                   return
               }
               else {
               
                   if verificationID != nil {
                   
                        self.VerificationID = verificationID ?? ""
                        print("Verification Id From Firebase : \(self.VerificationID)")
                       let vc = OTPVerificationViewController.getInstance()
                       vc.fireVerificationId = self.VerificationID
                    vc.user_ID = self.dataDict["id"] as? String ?? ""
                    vc.getMobileNumberStr = self.viewPhone.textField.text!
                    vc.dialCode = self.dailingCode
                    vc.isFromForgot = true
                       
                       self.navigationController?.pushViewController(vc, animated: true)
                   }
               }
           }
       }

    
    
    func apiCalling_Login(phoneNumber : String){
            
        let fcmToken = UserDefaults.standard.value(forKey: "FCM_REGISTRATION_TOKEN") as? String ?? ""
        print("FCM from messginig ", fcmToken)

        let params = [ "phone":phoneNumber, "device_type" : "ios", "fcm_id" : fcmToken, "dial_code" : self.dailingCode,
                       "client_token" : AppHelper.getClientToken()
            ]
        
        AppHelper.apiCallingForDict(apiName: SignIn, param: params, viewCont: self, successDict: { (successDict, resultDict) in
            self.dataDict = successDict
            UserDefaults.standard.set(self.dataDict, forKey: "LOGIN_RESPONSE")
            print(self.dataDict["id"] as? String ?? "")
            self.mobileNumberAuthenticationWithFirebase()
                        
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            if messageID == "157" {
                
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
                
            } else if messageID == "129" {
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
                
            } else {
                
                UIAlertController.showAlert(vc: self, title: "", message: message)
                
            }
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
}


/*
 Success!
 {
     data =     {
         "dial_code" = "+91";
         dob = "01 Jun, 2020";
         email = "driver1@gmail.com";
         "first_name" = Test;
         "full_name" = Driver1;
         gender = male;
         id = 1;
         image = "http://intellisensetechnologies.com/smartcab/uploads/images/5eb3e217768px-User_icon_2.svg.png";
         "is_approved" = 1;
         "is_verified" = 1;
         lang = "";
         "last_name" = Driver;
         phone = 9928791990;
         token = ee625c7cfb6e0f417d2660fed7d3f86c;
         username = Test;
     };
     "debug_id" = 1c838d77f6706ff5e7d5d3d6e8f77673;
     message = "Successfully logged in";
     "message_id" = 19;
     status = 1;
 }
 */


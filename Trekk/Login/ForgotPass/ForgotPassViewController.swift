//
//  ForgotPassViewController.swift
//  WillsSmartCabRider
//
//  Created by Yogender Saini on 2/20/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//


import UIKit
import ADCountryPicker

class ForgotPassViewController: BaseVC {

    @IBOutlet weak var viewPhone : custViewTextField!
       @IBOutlet weak var countryLbl: UILabel!
       @IBOutlet weak var imgViewFlag : UIImageView!
       
       let picker = ADCountryPicker(style: .grouped)
       var dailingCode = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.backButton.isHidden = false
        let paddingView3: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 20))
        self.viewPhone.textField.leftView = paddingView3
        self.viewPhone.textField.leftViewMode = .always
        countryLbl.text = "IN 91+"
        dailingCode = "91+"

    }
    
    static func getInstanceSignInPhoneVC() -> ForgotPassViewController {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ForgotPassViewController") as! ForgotPassViewController
    }

    @IBAction func onTapCountryBtn(_ sender: UIButton) {
        self.picker.delegate = self
        picker.showCallingCodes = true
        picker.pickerTitle = (L102Language.AMLocalizedString(key: "select_country", value: ""))
        //picker.defaultCountryCode = "YT"
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.navigationController?.pushViewController(picker, animated: true)
        
    }

}
//MARK: - COUNTRYPICKER DELEGATE
extension ForgotPassViewController: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        
        countryLbl.text = String(format: "%@ %@", code,dialCode)
        
        // countryImg.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode
        
        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")
        
        isCountrySelectedForRegister = true
        
        self.imgViewFlag.image = self.picker.getFlag(countryCode: code)
    
        navigationController?.popViewController(animated: true)
    }
}

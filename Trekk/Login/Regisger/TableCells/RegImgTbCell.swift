//
//  RegImgTbCell.swift
//  Trekk
//
//  Created by Yogender Saini on 02/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class RegImgTbCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitle1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.localization()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func localization() {
        
       // print((L102Language.AMLocalizedString(key: "Sign_up_with_email", value: "")))
                let attrStr = NSMutableAttributedString().getAttributedBoldString(str: (L102Language.AMLocalizedString(key: "Sign_up_with_email", value: "")), boldTxt: (L102Language.AMLocalizedString(key: "signup", value: "")), size: 30)
        
                self.lblTitle.attributedText = attrStr

        
    }
    
        
}

//
//  RegChooseLangTbCell.swift
//  Trekk
//
//  Created by Yogender Saini on 02/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class RegChooseLangTbCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var imgRadioHindi : UIImageView!
    @IBOutlet weak var imgRadioEnglish : UIImageView!
    @IBOutlet weak var imgRadioSpanish : UIImageView!
    @IBOutlet weak var imgRadioFrench : UIImageView!
    @IBOutlet weak var imgRadioProtugese : UIImageView!
    @IBOutlet weak var imgRadioSwahil : UIImageView!
    @IBOutlet weak var viewBirthday : custViewTextField!

    @IBOutlet weak var lblEnglish : UILabel!
    @IBOutlet weak var lblHindi : UILabel!
    @IBOutlet weak var lblPortugies : UILabel!
    @IBOutlet weak var lblSpanish : UILabel!
    @IBOutlet weak var lblFrench : UILabel!
    @IBOutlet weak var lblSwhile : UILabel!
    @IBOutlet weak var lblChooseLanguage : UILabel!
    
    var datePicker: UIDatePicker?
    var selectedLang = "en"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewBirthday.textField.delegate = self
        //default language set.
        ARR_SIGNUP_INFO_BOOL[10].updateValue(true, forKey: "lang")
        ARR_SIGNUP_INFO_STRING[10].updateValue(self.selectedLang, forKey: "value")

        self.localization()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    

    func localization() {
        
        self.lblChooseLanguage.text = (L102Language.AMLocalizedString(key: "Choose_Language_MenuKey", value: ""))
        
        self.lblEnglish.text = (L102Language.AMLocalizedString(key: "English", value: ""))
        self.lblHindi.text = (L102Language.AMLocalizedString(key: "Hindi", value: ""))
        self.lblPortugies.text = (L102Language.AMLocalizedString(key: "Portuguese", value: ""))
        self.lblSpanish.text = (L102Language.AMLocalizedString(key: "Spanish", value: ""))
        self.lblFrench.text = (L102Language.AMLocalizedString(key: "French", value: ""))
        self.lblSwhile.text = (L102Language.AMLocalizedString(key: "Swahili", value: ""))
    }
    
    @IBAction func btnSwahilAction(_ sender : UIButton){
        self.checkLanguage(sender: sender.tag)
    }
    
    func checkLanguage(sender : Int) {
        
        switch sender {
        case 0:
            self.updateUnselect()
            self.imgRadioEnglish.image = UIImage(named: "radio_btn")
            selectedLang = "en"
            ARR_SIGNUP_INFO_BOOL[10].updateValue(true, forKey: "lang")
            ARR_SIGNUP_INFO_STRING[10].updateValue(self.selectedLang, forKey: "value")

        case 1:
            self.updateUnselect()
            self.imgRadioSpanish.image = UIImage(named: "radio_btn")
            selectedLang = "es"
            ARR_SIGNUP_INFO_BOOL[10].updateValue(true, forKey: "lang")
            ARR_SIGNUP_INFO_STRING[10].updateValue(self.selectedLang, forKey: "value")

        case 2:
            self.updateUnselect()
            self.imgRadioFrench.image = UIImage(named: "radio_btn")
            selectedLang = "fr"
            ARR_SIGNUP_INFO_BOOL[10].updateValue(true, forKey: "lang")
            ARR_SIGNUP_INFO_STRING[10].updateValue(self.selectedLang, forKey: "value")

        case 3:
            self.updateUnselect()
            self.imgRadioHindi.image = UIImage(named: "radio_btn")
            selectedLang = "hi"
            ARR_SIGNUP_INFO_BOOL[10].updateValue(true, forKey: "lang")
            ARR_SIGNUP_INFO_STRING[10].updateValue(self.selectedLang, forKey: "value")

        case 4:
            self.updateUnselect()
            self.imgRadioProtugese.image = UIImage(named: "radio_btn")
            selectedLang = "pt"
            ARR_SIGNUP_INFO_BOOL[10].updateValue(true, forKey: "lang")
            ARR_SIGNUP_INFO_STRING[10].updateValue(self.selectedLang, forKey: "value")

        case 5:
            self.updateUnselect()
            self.imgRadioSwahil.image = UIImage(named: "radio_btn")
            selectedLang = "sw"
            ARR_SIGNUP_INFO_BOOL[10].updateValue(true, forKey: "lang")
            ARR_SIGNUP_INFO_STRING[10].updateValue(self.selectedLang, forKey: "value")

        default:
            print("")
        }
        
    }
    
    
    func updateUnselect()  {
        self.imgRadioEnglish.image = UIImage(named: "Ellipse 30")
        self.imgRadioSpanish.image = UIImage(named: "Ellipse 30")
        self.imgRadioFrench.image = UIImage(named: "Ellipse 30")
        self.imgRadioHindi.image = UIImage(named: "Ellipse 30")
        self.imgRadioProtugese.image = UIImage(named: "Ellipse 30")
        self.imgRadioSwahil.image = UIImage(named: "Ellipse 30")
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //Add DatePicker as inputView
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: 300, height: 216))
        self.datePicker?.datePickerMode = .date
                
        if self.viewBirthday.textField.placeholder == "Birthday" {
            print("Text field for birthday")
           self.viewBirthday.textField.inputView = self.datePicker
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if self.viewBirthday.textField.placeholder == "Birthday" {
            let strDate =  "\(self.datePicker!.date)"
            self.viewBirthday.textField.text = (strDate as! NSString).substring(to: 11)

            // updating array to check weather text is empty or not
            let placeholder = self.viewBirthday.textField.placeholder
            for i in 0...ARR_SIGNUP_INFO_BOOL.count-1{
                let dict = ARR_SIGNUP_INFO_BOOL[i]
                let keys = dict.keys
                
                let key1 = keys.first as! String
                if key1 == placeholder {
                    if textField.text == "" {
                        ARR_SIGNUP_INFO_BOOL[i].updateValue(false, forKey: placeholder!)
                        ARR_SIGNUP_INFO_STRING[i].updateValue("", forKey: "value")
                    } else {
                        ARR_SIGNUP_INFO_BOOL[i].updateValue(true, forKey: placeholder!)
                        ARR_SIGNUP_INFO_STRING[i].updateValue(self.viewBirthday.textField.text!, forKey: "value")
                    }
                }
            }
            
        }

    }
    
}



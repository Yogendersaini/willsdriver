//
//  RegSubmitTbCell.swift
//  Trekk
//
//  Created by Yogender Saini on 02/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class RegSubmitTbCell: UITableViewCell {

    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var btnSignup : UIButton!
    
    var viewCont = SignUpVC()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.localization()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        let attrStr = NSMutableAttributedString().getAttributedBoldString(str: "Already have an accunt? Sign In", boldTxt: "Sign In",size: 14)
//        self.btnBack.setAttributedTitle(attrStr, for: .normal)

    }
    
    func localization() {

        let attrStr = NSMutableAttributedString().getAttributedBoldString(str: (L102Language.AMLocalizedString(key: "already", value: "")), boldTxt: (L102Language.AMLocalizedString(key: "login_text", value: "")),size: 14)
        self.btnBack.setAttributedTitle(attrStr, for: .normal)

        let signup = (L102Language.AMLocalizedString(key: "signup", value: ""))
        self.btnSignup.setTitle(signup, for: .normal)

    }
    
    @IBAction func btnSigninAction(_ sender : UIButton) {
        let vc = SignInPhoneVC.getInstanceSignInPhoneVC()
        self.viewCont.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignupAction(_ sender : UIButton) {
//     let hvc = MainViewController.getInstance()
//     hvc.setup(type: 1)
//        self.viewCont.navigationController?.pushViewController(hvc, animated: true)
        self.viewCont.btnSignupAction()

    }
    
}


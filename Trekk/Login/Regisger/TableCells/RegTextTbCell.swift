//
//  RegTextTbCell.swift
//  Trekk
//
//  Created by Yogender Saini on 02/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit
import ADCountryPicker

class RegTextTbCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var viewText : custViewTextField!
    @IBOutlet weak var mainVviewPhone : UIView!
    
    var viewCont = SignUpVC()
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var lblFlagEmoji: UILabel!
    @IBOutlet weak var imgViewFlag : UIImageView!
    @IBOutlet weak var viewPhone : custViewTextField!
    @IBOutlet weak var btnGender : UIButton!
    
//    var datePicker: UIDatePicker?

    let picker = ADCountryPicker(style: .grouped)
    var dailingCode = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let paddingView3: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 110, height: 20))
        self.viewPhone.textField.leftView = paddingView3
        self.viewPhone.textField.leftViewMode = .always
        viewPhone.textField.delegate = self
        viewText.textField.delegate = self
        
        
        countryLbl.text = "\(AppHelper.getCountryCode().1) \(AppHelper.getCountryCode().0)+"
        dailingCode = "+" + AppHelper.getCountryCode().0
        print("Counrty code :- - -- - - - - - - - - - - - - - - ->", AppHelper.getCountryCode())
        
        self.lblFlagEmoji.text = AppHelper.countryFlag(countryCode: AppHelper.getCountryCode().1)

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
        
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        func textFieldDidEndEditing(_ textField: UITextField) {

        // updating array to check weather text is empty or not
        let placeholder = self.viewText.textField.placeholder
        let txtValue = textField.text as? String
        print("\(placeholder) value is -----------------------------> \(txtValue)")
        for i in 0...ARR_SIGNUP_INFO_BOOL.count-1{
            let dict = ARR_SIGNUP_INFO_BOOL[i]
            let keys = dict.keys
            
            let key1 = keys.first as! String
            if key1 == placeholder {
                if textField.text == "" {
                    ARR_SIGNUP_INFO_BOOL[i].updateValue(false, forKey: placeholder!)
                    ARR_SIGNUP_INFO_STRING[i].updateValue("", forKey: "value")
                } else {
                    ARR_SIGNUP_INFO_BOOL[i].updateValue(true, forKey: placeholder!)
                    ARR_SIGNUP_INFO_STRING[i].updateValue(txtValue!, forKey: "value")
                }
            }
        }
    }

    
    
    @IBAction func onTapCountryBtn(_ sender: UIButton) {
        self.picker.delegate = self
        picker.showCallingCodes = true
        picker.pickerTitle = (L102Language.AMLocalizedString(key: "select_country", value: ""))
        //picker.defaultCountryCode = "YT"
        picker.hidesNavigationBarWhenPresentingSearch = false
        self.viewCont.navigationController?.pushViewController(picker, animated: true)
        
    }
    
}
extension RegTextTbCell: ADCountryPickerDelegate {
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        
        countryLbl.text = String(format: "%@ %@", code,dialCode)
        
        // countryImg.image = picker.getFlag(countryCode: code)
        dailingCode = dialCode
        DIALING_CODE = dailingCode
        print("Dialing Code : \(dailingCode)")
        print("code : \(code)")
        print("Dial code : \(dialCode)")
        
        isCountrySelectedForRegister = true
        
        self.imgViewFlag.image = self.picker.getFlag(countryCode: code)
    
        self.viewCont.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func showAlertButtonTapped(_ sender: UIButton) {

        let alert = UIAlertController(title: "Gender", message: "", preferredStyle: UIAlertController.Style.alert)
        
        let actionMale = UIAlertAction(title: "Male", style: .default) { (action) in
            self.viewText.textField.text = "Male"
            // updating array to check weather text is empty or not
            let placeholder = self.viewText.textField.placeholder
            ARR_SIGNUP_INFO_BOOL[5].updateValue(true, forKey: placeholder!)
            ARR_SIGNUP_INFO_STRING[5].updateValue("Male", forKey: "value")
            
        }
        let actionFemale = UIAlertAction(title: "Female", style: .default) { (action) in
            self.viewText.textField.text = "Female"
            // updating array to check weather text is empty or not
            let placeholder = self.viewText.textField.placeholder
            ARR_SIGNUP_INFO_BOOL[5].updateValue(false, forKey: placeholder!)
            ARR_SIGNUP_INFO_STRING[5].updateValue("Female", forKey: "value")

        }
        
        alert.addAction(actionMale)
        alert.addAction(actionFemale)

        viewCont.present(alert, animated: true, completion: nil)
    }

    
}

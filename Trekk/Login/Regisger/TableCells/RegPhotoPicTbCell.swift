//
//  RegPhotoPicTbCell.swift
//  Trekk
//
//  Created by Yogender Saini on 02/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit
import SDWebImage
import AssetsLibrary
import Photos
import AKImageCropperView


protocol cropImageCustomDelegats {
    func passImageAfterCrop(img : UIImage)
}

class RegPhotoPicTbCell: UITableViewCell {

    @IBOutlet weak var btnChooseFile : UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblMsg : UILabel!
    @IBOutlet weak var imgViewPhoto : UIImageView!
    @IBOutlet weak var profileImgVw: UIImageView!
    var imgData = NSData()
    var signUpViewCotnt = SignUpVC()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.localization()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupDAta(vc : SignUpVC, index : Int) {
        
        self.signUpViewCotnt = vc
        
        if index == 0 {
            self.lblTitle.text = (L102Language.AMLocalizedString(key: "Road_Car_Insurance", value: ""))
        }
        if index == 1 {
            self.lblTitle.text = (L102Language.AMLocalizedString(key: "Driving_License", value: ""))
        }
        if index == 2 {
            self.lblTitle.text = (L102Language.AMLocalizedString(key: "Road_Worthiness", value: ""))
        }
    }
    
    func localization() {
        let btnChooseFile = (L102Language.AMLocalizedString(key: "Choose_file_button", value: ""))
        self.btnChooseFile.setTitle(btnChooseFile, for: .normal)
        self.lblMsg.text  = (L102Language.AMLocalizedString(key: "Please_upload_Image", value: ""))
    }
    
}
extension  RegPhotoPicTbCell : UIImagePickerControllerDelegate, UINavigationControllerDelegate, cropImageCustomDelegats {
    
    @IBAction func onTapProfileBtn(_ sender: UIButton) {
        isForImageBool = true
        self.checkCameraPermissions()
    }
    
    func checkCameraPermissions () {
        
        var alertController = UIAlertController()
        
        let alertTitle = (L102Language.AMLocalizedString(key: "choose_image", value: ""))
        
        if IS_IPAD {
            alertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
        }
        else {
            alertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
        }
        
        alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "camera", value: "")), style: .default, handler: { (action:UIAlertAction) in
            
            let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            
            switch (status) {
                
            case .authorized:
                
                self.camera()
                
            case .notDetermined:
                
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
                    
                    if (granted) {
                        
                        self.camera()
                        
                    } else {
                        
                        self.cameraDenied()
                    }
                }
                
            case .denied:
                
                self.cameraDenied()
                
            case .restricted:
                
                let alert = UIAlertController(title: "Camera Services Restricted",
                                              message: "You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.",
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction (title: BUTTON_TITLE, style: .default, handler: nil))
                
                self.signUpViewCotnt.present(alert, animated: true, completion: nil)
                
            @unknown default:
                fatalError()
            }
        }))
        
        alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "photo_library", value: "")), style: .default, handler: { (action:UIAlertAction) in
            self .photoLibrary()
        }))
        
        alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "cancel", value: "    ")), style: .destructive, handler: { (action:UIAlertAction) in
            
        }))
        
        self.signUpViewCotnt.present(alertController, animated: true, completion: nil)
    }
    
    func cameraDenied() {
         
        DispatchQueue.main.async {
            var alertText = "Turn on Camera Services in Settings > Privacy to allow Camera to take pictures"
            
            var alertButton = "Ok"
            var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)
            
            if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
            {
                alertText = "Turn on Camera Services in Settings > Privacy to allow Camera to take pictures"
                
                alertButton = "Settings"
                
                goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                })
            }
            
            let alert = UIAlertController(title: "Camera Services Off", message: alertText, preferredStyle: .alert)
            alert.addAction(goAction)
            self.signUpViewCotnt.present(alert, animated: true, completion: nil)
        }
    }
    
    func camera() {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            DispatchQueue.main.async {
                
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self;
                imagePickerController.sourceType = .camera
                imagePickerController.modalPresentationStyle = .fullScreen
                self.signUpViewCotnt.present(imagePickerController, animated: true, completion: nil)

            }
            
        } else {
            
            UIAlertController.showAlert(vc: self.signUpViewCotnt, title: ALERT_TITLE, message: "Camera not available !")
        }
    }
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            DispatchQueue.main.async {
                
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self;
                imagePickerController.sourceType = .photoLibrary
                
                let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue]
                UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
                
                imagePickerController.modalPresentationStyle = .fullScreen
                self.signUpViewCotnt.present(imagePickerController, animated: true, completion: nil)

            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil {
            
            let chosenImage: UIImage? = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
            let data: NSData? = chosenImage!.jpegData(compressionQuality: 0.2)! as NSData
            
             profileImgVw.image = chosenImage
//            backProfileImgVw.image = chosenImage
            
                imgData = data!
            
            let cropperViewController = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "CropperViewController") as! CropperViewController
            cropperViewController.image = chosenImage
            cropperViewController.delegate = self
            self.signUpViewCotnt.navigationController?.pushViewController(cropperViewController, animated: true)
            picker.dismiss(animated: true, completion: nil)
                

              
        } else {
            
            print("BEGIN:YPRRA BUSINESS ======> Something went wrong")
        }
        
       // self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        self.signUpViewCotnt.dismiss(animated: true, completion: nil)
    }
    
    func passImageAfterCrop(img: UIImage) {
        profileImgVw.image = img
        
        // updating array to check weather text is empty or not
        if profileImgVw.image != nil {
            let placeholder = self.lblTitle.text
            if self.lblTitle.text == "Road Car Insurance" {
                ARR_SIGNUP_INFO_BOOL[7].updateValue(true, forKey: placeholder!)
                ARR_SIGNUP_INFO_STRING[7].updateValue(img, forKey: "value")
            }
            if self.lblTitle.text == "Upload Driving License" {
                ARR_SIGNUP_INFO_BOOL[8].updateValue(true, forKey: placeholder!)
                ARR_SIGNUP_INFO_STRING[8].updateValue(img, forKey: "value")
            }
            if self.lblTitle.text == "Road Worthiness" {
                ARR_SIGNUP_INFO_BOOL[9].updateValue(true, forKey: placeholder!)
                ARR_SIGNUP_INFO_STRING[9].updateValue(img, forKey: "value")
            }

        }
    }

    
}




//
//  SignUpVC.swift
//  Wills Smart Cab Rider
//
//  Created by Sukhpreet on 12/02/21.
//

import UIKit
import UIKit
import ADCountryPicker
import FBSDKLoginKit
import GoogleSignIn


// below arrar is used to check weather table view cells are empty values or not.
var ARR_SIGNUP_INFO_BOOL  = [["First Name" : false], ["Last Name"  : false], ["Username"  : false], ["Email-Id" : false] , ["Mobile Number"  : false], ["Gender"  : false], ["Birthday"  : false], ["Road Car Insurance"  : false], ["Upload Driving License" : false], ["Road Worthiness"  : false], ["lang"  : false]]

var ARR_SIGNUP_INFO_STRING = [["value" : ""], ["value" : ""], ["value" : ""], ["value" : ""] , ["value" : ""], ["value" : ""], ["value" : ""], ["value" : UIImage.self], ["value" : UIImage.self], ["value" : UIImage.self], ["value" : ""] ]

var DIALING_CODE = "+91"


class SignUpVC: BaseVC {

    // IBOutlets
    @IBOutlet weak var viewEmail : custViewTextField!
    @IBOutlet weak var viewPassword : custViewTextField!
    @IBOutlet weak var viewName : custViewTextField!
    @IBOutlet weak var viewPhone : custViewTextField!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var imgViewFlag : UIImageView!
    
    @IBOutlet weak var tableViewRegister : UITableView!
    
    var arrPlaceholder = [String]()

    
    let picker = ADCountryPicker(style: .grouped)
//    var dailingCode = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableviewCells()
     
        // update array for placeholders.
        self.arrPlaceholder = [(L102Language.AMLocalizedString(key: "first_name", value: "")),
        (L102Language.AMLocalizedString(key: "last_name", value: "")),
        (L102Language.AMLocalizedString(key: "username", value: "")),
        (L102Language.AMLocalizedString(key: "emailId", value: "")),
        (L102Language.AMLocalizedString(key: "mobile_number", value: "")),
        (L102Language.AMLocalizedString(key: "Gender", value: ""))]
        
        
    }
    
    
    static func getInstanceSignInPhoneVC() -> SignUpVC {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
    }
    

}
extension SignUpVC {
    // MARK: - ButtonActions
       
    func btnSignupAction() {
        
        
        var noError = true
        self.view.endEditing(true)
        
        let placeholder = false
        for i in 0...ARR_SIGNUP_INFO_BOOL.count-1{
            let dict = ARR_SIGNUP_INFO_BOOL[i]
            let keys = dict.keys
            let key1 = keys.first!
            let valFirstKey = dict[key1]!
            if valFirstKey == placeholder {
                self.showToast_Message(key: key1)
                noError = false
                break
            }
        }
        
        if noError {
            self.apiCalled_Signup()
        }
    }
    
    func showToast_Message(key : String) {
        var keyStr = ""
        
        switch key {
            
            case "First Name" :
             keyStr = (L102Language.AMLocalizedString(key: "First_Name_validations", value: ""))
            case "Last Name"  :
             keyStr = (L102Language.AMLocalizedString(key: "Last_Name_validations", value: ""))
            case "Username" :
             keyStr = (L102Language.AMLocalizedString(key: "User_Name_validations", value: ""))
            case "Email-Id" :
             keyStr = (L102Language.AMLocalizedString(key: "EmailId_validations", value: ""))
            case "Mobile Number" :
             keyStr = (L102Language.AMLocalizedString(key: "Mobile_number_validations", value: ""))
            case "Gender"  :
             keyStr = (L102Language.AMLocalizedString(key: "Gender_validations", value: ""))
            case "Birthday"  :
             keyStr = (L102Language.AMLocalizedString(key: "Birthday_validations", value: ""))
            case "Road Car Insurance"  :
             keyStr = (L102Language.AMLocalizedString(key: "Road_Car_Insurance", value: ""))
            case "Upload Driving License" :
             keyStr = (L102Language.AMLocalizedString(key: "Driving_Licence_validations", value: ""))
            case "Road Worthiness"  :
             keyStr = (L102Language.AMLocalizedString(key: "Road_Worthiness_validations", value: ""))
            case "lang"  :
             keyStr = (L102Language.AMLocalizedString(key: "Language_validations", value: ""))
        default:
            keyStr = "No key get"
        }
        
        let msg = keyStr
        self.view.makeToast(msg)

        
    }
    
}
//MARK: - COUNTRYPICKER DELEGATE

extension SignUpVC : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        
        tableViewRegister.register(UINib(nibName: "RegChooseLangTbCell", bundle: nil), forCellReuseIdentifier: "RegChooseLangTbCell")

        tableViewRegister.register(UINib(nibName: "RegPhotoPicTbCell", bundle: nil), forCellReuseIdentifier: "RegPhotoPicTbCell")

        tableViewRegister.register(UINib(nibName: "RegImgTbCell", bundle: nil), forCellReuseIdentifier: "RegImgTbCell")

        tableViewRegister.register(UINib(nibName: "RegTextTbCell", bundle: nil), forCellReuseIdentifier: "RegTextTbCell")

        tableViewRegister.register(UINib(nibName: "RegSubmitTbCell", bundle: nil), forCellReuseIdentifier: "RegSubmitTbCell")
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else if section == 1 {
            return arrPlaceholder.count
        }
        else if section == 2 {
            return 1
        }
        else if section == 3 {
            return 3
        }
        else  {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if indexPath.section == 0 {
            let regImgTbCell = tableViewRegister.dequeueReusableCell(withIdentifier: "RegImgTbCell", for: indexPath) as! RegImgTbCell
            regImgTbCell.selectionStyle = .none
            return regImgTbCell
        }
        
        else if indexPath.section == 1 {
            let regTextTbCell = tableViewRegister.dequeueReusableCell(withIdentifier: "RegTextTbCell", for: indexPath) as! RegTextTbCell
            regTextTbCell.selectionStyle = .none
            regTextTbCell.viewText.textField.placeholder = self.arrPlaceholder[indexPath.row]
            regTextTbCell.viewCont = self
                        
            
            if indexPath.row == 4 {
                regTextTbCell.viewText.isHidden=true
                regTextTbCell.mainVviewPhone.isHidden=false
            } else {
                regTextTbCell.viewText.isHidden=false
                regTextTbCell.mainVviewPhone.isHidden=true
            }
            if indexPath.row == 5 {
                regTextTbCell.btnGender.isHidden = false
            } else {
                regTextTbCell.btnGender.isHidden = true
            }
            
            
            return regTextTbCell
        }

        else if indexPath.section == 2 {
            let regChooseLangTbCell = tableViewRegister.dequeueReusableCell(withIdentifier: "RegChooseLangTbCell", for: indexPath) as! RegChooseLangTbCell
            regChooseLangTbCell.selectionStyle = .none
            regChooseLangTbCell.checkLanguage(sender: 0)
            regChooseLangTbCell.viewBirthday.textField.placeholder = (L102Language.AMLocalizedString(key: "Birthday", value: ""))
            return regChooseLangTbCell
        }

        else if indexPath.section == 3 {
            let regPhotoPicTbCell = tableViewRegister.dequeueReusableCell(withIdentifier: "RegPhotoPicTbCell", for: indexPath) as! RegPhotoPicTbCell
            regPhotoPicTbCell.selectionStyle = .none
//            regPhotoPicTbCell.lblTitle.text = arrTitles[indexPath.row]
//            regPhotoPicTbCell.signUpViewCotnt = self
            regPhotoPicTbCell.setupDAta(vc: self, index: indexPath.row)
            return regPhotoPicTbCell
        }

        else {
            let regSubmitTbCell = tableViewRegister.dequeueReusableCell(withIdentifier: "RegSubmitTbCell", for: indexPath) as! RegSubmitTbCell
            regSubmitTbCell.selectionStyle = .none
            regSubmitTbCell.viewCont = self
            
            return regSubmitTbCell
        }

    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension SignUpVC {
    
    func apiCalled_Signup() {

        
        let param =  ["client_token" : AppHelper.getClientToken(), "first_name" : (ARR_SIGNUP_INFO_STRING[0] as [String : Any])["value"] as! String, "last_name"  : (ARR_SIGNUP_INFO_STRING[1] as [String : Any])["value"] as! String, "username"  : (ARR_SIGNUP_INFO_STRING[2] as [String : Any])["value"] as! String, "email"  : (ARR_SIGNUP_INFO_STRING[3] as [String : Any])["value"] as! String, "phone"  : (ARR_SIGNUP_INFO_STRING[4] as [String : Any])["value"] as! String, "gender"  : (ARR_SIGNUP_INFO_STRING[5] as [String : Any])["value"] as! String, "dob"  : (ARR_SIGNUP_INFO_STRING[6] as [String : Any])["value"] as! String, "lang"  : (ARR_SIGNUP_INFO_STRING[10] as [String : Any])["value"] as! String, "dial_code" : DIALING_CODE]
        
        print(param)
        
        let dataLicImage = ((ARR_SIGNUP_INFO_STRING[7] as [String : Any])["value"] as! UIImage).jpegData(compressionQuality: 0.2)! as NSData
        
        let dataInsImage = ((ARR_SIGNUP_INFO_STRING[8] as [String : Any])["value"] as! UIImage).jpegData(compressionQuality: 0.2)! as NSData
        
        let datarwImage = ((ARR_SIGNUP_INFO_STRING[9] as [String : Any])["value"] as! UIImage).jpegData(compressionQuality: 0.2)! as NSData
        
        AppHelper.apiCallingWithSignUp(apiName: Signup, param: param, viewCont: self, dl_image: dataLicImage, ins_image: dataInsImage, rw_image: datarwImage, successDict: { (succDict) in
            print(succDict)
            
            let msgId = succDict["message_id"] as? String ?? ""
            if msgId == "132" {
                UIAlertController.showAlertWithOkAction(self, title: "Sign Up", message: succDict["message"] as? String ?? "", buttonTitle: "Ok", buttonAction: self.backToRoot)
            }
                        
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            if messageID == "157" {
                
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
                
            } else if messageID == "129" {
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
                
            } else {
                UIAlertController.showAlert(vc: self, title: "", message: message)
            }
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    @objc func backToRoot() {
    }
        
}

        
        //        "client_token"
        //        "email"
        //        "dial_code"
        //        "phone"
        //        "first_name"
        //        "last_name"
        //        "gender"
        //        "dob"
        //        "username"
        //
        ////        in driver/signup,these more keys are there
        //        "dl_image"
        //        "ins_image"
        //        "rw_image"
        //        "lang"
        //
        //        And for driver image,that is in :driver/update_profile
        //        key:image
        

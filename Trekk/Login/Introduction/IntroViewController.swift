//
//  IntroViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 03/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit


class IntroViewController: UIViewController {
    
    @IBOutlet weak var bgVw: UIView!
    @IBOutlet weak var scrollVw: UIScrollView!
    {
        didSet {
            scrollVw.delegate = self
        }
    }
    
   
    @IBOutlet weak var pageControl: UIPageControl!
    
     var slides = 4
     var index     = 0

    let arrImages = [UIImage(named: "screen_1"), UIImage(named: "screen-2"), UIImage(named: "screen-3"), UIImage(named: "screen-4"),UIImage(named: "screen-5"), UIImage(named: "help-screen")
    ]
    let arrColoress : [UIColor] = [UIColor.red, UIColor.green, UIColor.blue, UIColor.red, UIColor.green, UIColor.yellow]
    var isFromHome : Bool = false
    var isFromSideMenu : Bool = false
    
    let arrViewTitles = ["Accept a Job", "Tracking Realtime", "Earn Money", "Enable Your Location"]
    let arrViewDesc = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac vestibulum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac vestibulum", "Choose your location to start find the request around you.", "Choose your location to start find the request around you."]
    let arrViewImages = ["phone_hand", "location_illustration-1", "earn_money", "location_illustration"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpScrollView()  
    }
    
    override func viewDidLayoutSubviews() {
        for view in view.subviews{
            if view is UIScrollView{
                view.frame = UIScreen.main.bounds
            }else if view is UIPageControl{
                view.backgroundColor = UIColor.clear
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true

    }
    
    static func getInstance() -> IntroViewController {
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
    }
    
    func setUpScrollView() {
        
        scrollVw.alwaysBounceHorizontal = false
        scrollVw.alwaysBounceVertical = false
        scrollVw.bounces = false
        
        setupSlideScrollView()
        
        pageControl.numberOfPages = slides
        pageControl.currentPage = 0
        pageControl.layer.backgroundColor = UIColor.clear.cgColor
        pageControl.backgroundColor = UIColor.clear
        bgVw.bringSubviewToFront(pageControl)
        
    }

    @IBAction func onTapArrowBackBtn(_ sender: UIButton) {
        if isFromSideMenu {
            self.navigationController?.popViewController(animated: true)
        }
        self.dismiss(animated: true) {
        }
    }
    
    @IBAction func onTapGetStartedBtn(_ sender: UIButton) {

        if isFromSideMenu {
            self.navigationController?.popViewController(animated: true)
            
        } else {
            
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name("openScanViewCont"), object: nil)
            }
        }
        
    }
    
    @IBAction func onTapBackBtn(_ sender: UIButton) {
        let pageIndex = round(scrollVw.contentOffset.x/view.frame.width)
              pageControl.currentPage = Int(pageIndex)
            //  backBtn.isUserInteractionEnabled = true

        if pageIndex == 0 {
            return
        }
              let currentPage = pageControl.currentPage - 1
              let offset = CGPoint(x: CGFloat(currentPage) * scrollVw.frame.size.width, y: 0)
              scrollVw.setContentOffset(offset, animated: true)
              if case 0 = pageIndex {
                  
              // backBtn.isUserInteractionEnabled = true
               // backBtn.isUserInteractionEnabled = false
               
                
              } else if case 1 = pageIndex {
                
              }
          
    }
    
     func onTapSignupBtn() {

        let vc = SignUpVC.getInstanceSignInPhoneVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
extension IntroViewController : UIScrollViewDelegate {
    
    
    //MARK: -  UIScrollView Delegate
    
    func setupSlideScrollView() {
        
        scrollVw.contentSize = CGSize(width:  (UIScreen.main.bounds.size.width) * CGFloat(slides), height: (UIScreen.main.bounds.size.height - 100))
        scrollVw.isPagingEnabled = true
        
        for i in 0 ..< slides {
            
            let frm  = CGRect(x:  (UIScreen.main.bounds.size.width) * CGFloat(i), y: 0, width:  (UIScreen.main.bounds.size.width), height: (UIScreen.main.bounds.size.height))

            let s1 : CustomSlideView = CustomSlideView.init(frame: frm)
            s1.imgVww.image = UIImage(named: arrViewImages[i])
            s1.lblTitle.text = arrViewTitles[i]
            s1.lblDesc.text = arrViewDesc[i]
            s1.viewCont = self
            
//            if i == 2 || i == 3{
            if i == 3{
                s1.btnSkip.isHidden = true
                s1.btnSignup.isHidden = false
                s1.btnSkipNow.isHidden = true
                if i == 2 {
                  //  s1.btnSkipNow.isHidden = true
                }
            } else {
                s1.btnSkip.isHidden = false
                s1.btnSignup.isHidden = true
                s1.btnSkipNow.isHidden = true
            }
            scrollVw.addSubview(s1)
            
        }
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        if case 0 = pageIndex {
        }
    }
  }


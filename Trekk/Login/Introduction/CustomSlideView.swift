//
//  CustomSlideView.swift
//  Trekk
//
//  Created by Yogender Saini on 1/27/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//


import UIKit
import Foundation

class CustomSlideView: UIView {
    
    let nibName = "CustomSlideView"
    var contentView: UIView?
    var viewCont = UIViewController()
    
    @IBOutlet weak var imgVww: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnSkipNow: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    @IBAction func btnSignupAction(_ sender : UIButton) {
       let vc = SignUpVC.getInstanceSignInPhoneVC()
        self.viewCont.navigationController?.pushViewController(vc, animated: true)
   }


}



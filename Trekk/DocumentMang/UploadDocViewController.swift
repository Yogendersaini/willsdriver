//
//  UploadDocViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 16/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//


import UIKit
import SDWebImage
import AssetsLibrary
import Photos
import AKImageCropperView

class UploadDocViewController: UIViewController {

    @IBOutlet weak var profileImgVw: UIImageView!
    @IBOutlet weak var profileImgVw2: UIImageView!
    @IBOutlet weak var profileImgVw3: UIImageView!
    @IBOutlet weak var btnBack: UIButton!

    @IBOutlet weak var lblDrLic: UILabel!
    @IBOutlet weak var lblRoadInsur: UILabel!
    @IBOutlet weak var lblRoadWorth: UILabel!
    
    
    var imgData1 = NSData()
    var imgData2 = NSData()
    var imgData3 = NSData()
    var sender = 0
    let arrType = ["d_lic","d_insur","road_worth"]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.apiCalling_GetAllVehicleDocs()
                
        self.btnBack.setTitle((L102Language.AMLocalizedString(key: "Document_Management", value: "")), for: .normal)
        self.lblDrLic.text = (L102Language.AMLocalizedString(key: "Driving_License", value: ""))
        self.lblRoadInsur.text = (L102Language.AMLocalizedString(key: "Road_Car_Insurance", value: ""))
        self.lblRoadWorth.text = (L102Language.AMLocalizedString(key: "Road_Worthiness", value: ""))
                
    }
    
    static func getInstance() -> UploadDocViewController {
        let storyboard = UIStoryboard(name: "Document",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "UploadDocViewController") as! UploadDocViewController

    }

    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
    }
}


extension  UploadDocViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate, cropImageCustomDelegats {

@IBAction func onTapProfileBtn(_ sender: UIButton) {
    isForImageBool = true
    self.checkCameraPermissions()
    self.sender = sender.tag
}

func checkCameraPermissions () {
    
    var alertController = UIAlertController()
    
    let alertTitle = (L102Language.AMLocalizedString(key: "choose_image", value: ""))
    
    if IS_IPAD {
        alertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
    }
    else {
        alertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
    }
    
    alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "camera", value: "")), style: .default, handler: { (action:UIAlertAction) in
        
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        
        switch (status) {
            
        case .authorized:
            
            self.camera()
            
        case .notDetermined:
            
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
                
                if (granted) {
                    
                    self.camera()
                    
                } else {
                    
                    self.cameraDenied()
                }
            }
            
        case .denied:
            
            self.cameraDenied()
            
        case .restricted:
            
            let alert = UIAlertController(title: "Camera Services Restricted",
                                          message: "You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.",
                                          preferredStyle: .alert)
            
            alert.addAction(UIAlertAction (title: BUTTON_TITLE, style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        @unknown default:
            fatalError()
        }
    }))
    
    alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "photo_library", value: "")), style: .default, handler: { (action:UIAlertAction) in
        self .photoLibrary()
    }))
    
    alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "cancel", value: "    ")), style: .destructive, handler: { (action:UIAlertAction) in
        
    }))
    
    self.present(alertController, animated: true, completion: nil)
}

func cameraDenied() {
     
    DispatchQueue.main.async {
        var alertText = "Turn on Camera Services in Settings > Privacy to allow Camera to take pictures"
        
        var alertButton = "Ok"
        var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)
        
        if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
        {
            alertText = "Turn on Camera Services in Settings > Privacy to allow Camera to take pictures"
            
            alertButton = "Settings"
            
            goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            })
        }
        
        let alert = UIAlertController(title: "Camera Services Off", message: alertText, preferredStyle: .alert)
        alert.addAction(goAction)
        self.present(alert, animated: true, completion: nil)
    }
}

func camera() {
    
    if UIImagePickerController.isSourceTypeAvailable(.camera) {
        
        DispatchQueue.main.async {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .camera
            imagePickerController.modalPresentationStyle = .fullScreen
            self.present(imagePickerController, animated: true, completion: nil)

        }
        
    } else {
        
        UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Camera not available !")
    }
}

func photoLibrary() {
    
    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
        
        DispatchQueue.main.async {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self;
            imagePickerController.sourceType = .photoLibrary
            
            let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue]
            UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
            
            imagePickerController.modalPresentationStyle = .fullScreen
            self.present(imagePickerController, animated: true, completion: nil)

        }
    }
}

func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

    if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil {
        
        let chosenImage: UIImage? = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        let data: NSData? = chosenImage!.jpegData(compressionQuality: 0.2)! as NSData
        


        
        if self.sender == 1 {
            imgData1 = data!
            profileImgVw.image = chosenImage
        } else if self.sender == 2 {
            imgData2 = data!
            profileImgVw2.image = chosenImage
        } else {
            imgData3 = data!
            profileImgVw3.image = chosenImage
        }

        
        let cropperViewController = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "CropperViewController") as! CropperViewController
        cropperViewController.image = chosenImage
        cropperViewController.delegate = self
        self.navigationController?.pushViewController(cropperViewController, animated: true)
        picker.dismiss(animated: true, completion: nil)
            

          
    } else {
        
        print("BEGIN:YPRRA BUSINESS ======> Something went wrong")
    }
    
   // self.dismiss(animated: true, completion: nil)
    
}

func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    
    let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
    UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
    
    self.dismiss(animated: true, completion: nil)
}

func passImageAfterCrop(img: UIImage) {
    
    if self.sender == 1 {
        profileImgVw.image = img
        self.apiCalling_GetUpdateVehicleDocs(type: arrType[0] , imgData: self.imgData1)
    } else if self.sender == 2 {
        profileImgVw2.image = img
        self.apiCalling_GetUpdateVehicleDocs(type: arrType[1] , imgData: self.imgData2)
    } else {
        profileImgVw3.image = img
        self.apiCalling_GetUpdateVehicleDocs(type: arrType[2] , imgData: self.imgData3)
    }

}
}
extension UploadDocViewController {
    //MARK:-API Wprl


    func apiCalling_GetUpdateVehicleDocs(type  : String, imgData : NSData)  {

//    client_token:
//    token
//    image
//    type: d_insur/d_lic/road_worth
    
       let params = ["token" : AppHelper.getUserToken(), "client_token" : AppHelper.getClientToken(), "type" : type]
              
       AppHelper.apiCallingWithImage(apiName: GetUpdateVehicleDocs, param: params, viewCont: self, imgData: imgData, successDict: { (successDict) in

        self.view.makeToast("Image update successfully")
//        UIAlertController.showAlertWithOkAction(self, title: "Sign Up", message: "Vehicle added successfully", buttonTitle: "Ok", buttonAction: self.backToRoot)
           
       }, fail: { (failErr) in
           print("Login Error:\(failErr.localizedDescription)")
           
       }, messageID: { (messageID, message) in
           if messageID == "157" {
               
               UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
               
           } else if messageID == "129" {
               UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
               
           } else {
               
               UIAlertController.showAlert(vc: self, title: "", message: message)
               
           }
           
       }) { (alertMsg) in
           UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
       }
       
   }
    
    

        func apiCalling_GetAllVehicleDocs()  {

    //    client_token:
    //    token
        
           let params = ["token" : AppHelper.getUserToken(), "client_token" : AppHelper.getClientToken()]
               
            AppHelper.apiCallingForDict(apiName: GetVehicleDocs, param: params, viewCont: self, successDict: { (successDict, jsonDict) in
                print(successDict)
               
                let dl_image = successDict["dl_image"] as? String ?? ""
                let ins_image = successDict["ins_image"] as? String ?? ""
                let rw_image = successDict["rw_image"] as? String ?? ""
                
                self.profileImgVw.sd_setImage(with: URL(string: dl_image), placeholderImage: UIImage(named: ""))

                self.profileImgVw2.sd_setImage(with: URL(string: ins_image), placeholderImage: UIImage(named: ""))

                self.profileImgVw3.sd_setImage(with: URL(string: rw_image), placeholderImage: UIImage(named: ""))

                /*
                 data =     {
                     "dl_image" = "http://intellisensetechnologies.com/smartcab/uploads/docs/dl/609cd69b1e47fimage.png";
                     "ins_image" = "";
                     "rw_image" = "";
                 };
                 */
                
           }, fail: { (failErr) in
               print("Login Error:\(failErr.localizedDescription)")
               
           }, messageID: { (messageID, message) in
               if messageID == "157" {
                   
                   UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
                   
               } else if messageID == "129" {
                
                   UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
                   
               } else {
                   
                   UIAlertController.showAlert(vc: self, title: "", message: message)
                   
               }
           }) { (alertMsg) in
               UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
           }
           
       }
        
    
    
}




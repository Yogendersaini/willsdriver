//
//  LanguageViewController.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 27/03/20.
//  Copyright © 2020 Harjit Singh Mac. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {

    @IBOutlet weak var languageChangeVw: UIView!

//    @IBOutlet weak var btnEnglish: UIButton!
//    @IBOutlet weak var btnHindi: UIButton!
//    @IBOutlet weak var btnSpanish: UIButton!
//    @IBOutlet weak var btnFrench: UIButton!
//    @IBOutlet weak var btnPortuguese: UIButton!
//    @IBOutlet weak var btnSwahili: UIButton!

    @IBOutlet weak var imgEnglish: UIImageView!
    @IBOutlet weak var imgHindi: UIImageView!
    @IBOutlet weak var imgSpanish: UIImageView!
    @IBOutlet weak var imgFrench: UIImageView!
    @IBOutlet weak var imgPortuguese: UIImageView!
    @IBOutlet weak var imgSwahili: UIImageView!

    @IBOutlet weak var lblEnglish: UILabel!
    @IBOutlet weak var lblHindi: UILabel!
    @IBOutlet weak var lblSwhile: UILabel!
    @IBOutlet weak var lblFrench: UILabel!
    @IBOutlet weak var lblSpanish: UILabel!
    @IBOutlet weak var lblPortugies: UILabel!
    @IBOutlet weak var lblSelectLang: UILabel!
    @IBOutlet weak var btnBAck: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!


    
    override func viewDidLoad() {
        self.localization()
    }
    
    static func getInstance() -> LanguageViewController {
        
        let storyboard = UIStoryboard(name: "Language", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "LanguageViewController") as! LanguageViewController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    func localization()  {
        
        self.lblEnglish.text = (L102Language.AMLocalizedString(key: "English", value: ""))
        self.lblHindi.text = (L102Language.AMLocalizedString(key: "Hindi", value: ""))
        self.lblSwhile.text = (L102Language.AMLocalizedString(key: "Swahili", value: ""))
        
        self.lblFrench.text = (L102Language.AMLocalizedString(key: "French", value: ""))
        self.lblSpanish.text = (L102Language.AMLocalizedString(key: "Spanish", value: ""))
        self.lblPortugies.text = (L102Language.AMLocalizedString(key: "Portuguese", value: ""))
        
        self.lblSelectLang.text = (L102Language.AMLocalizedString(key: "Please_select_Language", value: ""))
        self.btnBAck.setTitle((L102Language.AMLocalizedString(key: "Language", value: "")), for: .normal)
        self.btnSubmit.setTitle((L102Language.AMLocalizedString(key: "Submit_Button", value: "")), for: .normal)
        
    
    }
    
    
    func popVc() {
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
}
extension LanguageViewController {
//MARK: - IB ACTIONS
@IBAction func onTapSpanishBtn(_ sender: UIButton) {
    
    self.updateLanguage(btnVal: 7)
    L102Language.setAppleLAnguageTo(lang: "es")
    
//    NotificationCenter.default.post(name: Notification.Name(LOCALIZATIONREFRESH), object: nil)
//
//    self.languageChangeVw.isHidden = false
//    self.navigationController?.setNavigationBarHidden(true, animated: true)
//    self.languageChangeLbl.text = (L102Language.AMLocalizedString(key: "please_wait", value: ""))
//
//    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
//        DispatchQueue.main.async {
//            self.popVc()
//
//        }
//    }
    
}

@IBAction func onTapEnglishBtn(_ sender: UIButton) {
    self.updateLanguage(btnVal: 5)
    L102Language.setAppleLAnguageTo(lang: "en")
    
//    NotificationCenter.default.post(name: Notification.Name(LOCALIZATIONREFRESH), object: nil)
//
//    self.languageChangeVw.isHidden = false
//    self.navigationController?.setNavigationBarHidden(true, animated: true)
//    self.languageChangeLbl.text = (L102Language.AMLocalizedString(key: "please_wait", value: ""))
//    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
//        DispatchQueue.main.async {
//            self.popVc()
//
//        }
//    }
}

@IBAction func onTapFrenchBtn(_ sender: UIButton) {
    self.updateLanguage(btnVal: 1)
    L102Language.setAppleLAnguageTo(lang: "fr")

//    self.languageChangeVw.isHidden = false
//    self.languageChangeLbl.text = (L102Language.AMLocalizedString(key: "please_wait", value: ""))
//
//    self.navigationController?.setNavigationBarHidden(true, animated: true)
//    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
//        DispatchQueue.main.async {
//            self.popVc()
//        }
//    }
}

@IBAction func onTapSwahiliBtn(_ sender: UIButton) {
    self.updateLanguage(btnVal: 3)
    L102Language.setAppleLAnguageTo(lang: "de")
    self.languageChangeVw.isHidden = false
    
//    self.languageChangeLbl.text = (L102Language.AMLocalizedString(key: "please_wait", value: ""))
//    self.navigationController?.setNavigationBarHidden(true, animated: true)
//
//    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
//        DispatchQueue.main.async {
//
//            self.popVc()
//        }
//    }
}

    @IBAction func onTapHindiBtn(_ sender: UIButton) {
        self.updateLanguage(btnVal: 2)
        L102Language.setAppleLAnguageTo(lang: "hi")

//        self.languageChangeVw.isHidden = false
//        self.languageChangeLbl.text = (L102Language.AMLocalizedString(key: "please_wait", value: ""))
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
//            DispatchQueue.main.async {
//
//                self.popVc()
//            }
//        }
    }
    
    @IBAction func onTapPortugueseBtn(_ sender: UIButton) {
        self.updateLanguage(btnVal: 4)
        L102Language.setAppleLAnguageTo(lang: "de")
        
//        self.languageChangeVw.isHidden = false
//        self.languageChangeLbl.text = (L102Language.AMLocalizedString(key: "please_wait", value: ""))
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
//
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+3.0) {
//            DispatchQueue.main.async {
//
//                self.popVc()
//            }
//        }
    }
    
    func updateLanguage(btnVal : Int)  {
    
    self.imgEnglish.image = UIImage(named: "Ellipse 30")
    self.imgHindi.image = UIImage(named: "Ellipse 30")
    self.imgSpanish.image = UIImage(named: "Ellipse 30")
    self.imgFrench.image = UIImage(named: "Ellipse 30")
    self.imgPortuguese.image = UIImage(named: "Ellipse 30")
    self.imgSwahili.image = UIImage(named: "Ellipse 30")
    
    if let imgView = self.view.viewWithTag(btnVal) as? UIImageView {
        imgView.image = UIImage(named: "radio_btn")
    }
        
    }
    
    @IBAction func onTap_submitBtn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

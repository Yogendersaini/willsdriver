//
//  AboutViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 08/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit
import WebKit


class AboutViewController: UIViewController {
    
    @IBOutlet weak var btnBack : UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var index = 0
    var rider_ID = ""
    var btnBackTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // for reopen ride if user come from ride status/ or ride is going on
        //IS_FROM_COMPLAIN_RESEND_REQUEST_CHAT = false
        
        var langCode = ""
        let currentLang = UserDefaults.standard.value(forKey: "CurrentLanguage")
        if !(currentLang != nil) {
            langCode = "en"
        } else {
            langCode = currentLang as! String
        }

        var url = ""
        if index == 1 {
            url = "\(BaseURL)\(GetPrivacyPolicyWebPage)"
        } else if index == 2 {
            url = "\(BaseURL)\(GetContctWebPage)"
            
        } else  {
            let chatDAta = "?token=\(AppHelper.getUserToken())&client_token=\(AppHelper.getClientToken())&rider_id=\(rider_ID)"
            url = "\(BaseURL)\(GetChatWebPage)\(chatDAta)"
            print("url for chat --------------------------->")
            print(url)
        }
        
                
        // Do any additional setup after loading the view, typically from a nib.
        let request = URLRequest(url: URL(string: url)!)
        self.webView.load(request)
        self.webView.addObserver(self, forKeyPath: #keyPath(WKWebView.isLoading), options: .new, context: nil)
        
        // Page Scrolling - false or true
        webView.scrollView.isScrollEnabled = true
        self.lblTitle.text = btnBackTitle
        
    }
    
    static func getInstance() -> AboutViewController {
        
        let storyboard = UIStoryboard(name: "About",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
        
    }
    
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "loading" {
            
            if webView.isLoading {
                
                activityIndicator.startAnimating()
                activityIndicator.isHidden = false
                
            }else {
                
                activityIndicator.stopAnimating()
                activityIndicator.isHidden = true
                
            }
            
        }
    }
}

//
//  API Constants.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 30/03/20.
//  Copyright © 2020 Harjit Singh Mac. All rights reserved.
//

var PROFILE_DATA_GLOBEL = DashboardDataModel()

import Foundation


//    let BaseURL = "http://intellisensetechnologies.com/smartcab/api/"  // test
    let BaseURL =  "https://onlinefood.app/willscab/api/"  // live

    let Tomtom_key = "NmFwApZ9A9aelMHzGS3WdEZA8ggLIFzz"
    let MAP_Key = "AIzaSyD8cTDcRTYh9Guc_-jWrLY82uFhT5yRBNg"
    let MAP_DirectionKey = "AIzaSyCqJwEohRec-Vnl-AwhkgC_DvbmuHs8ywQ"
    let MAP_PlacesKey = "AIzaSyBY3gJ0r0dxesSL8V6jwRhhU1uRuCgrXTQ"


    let IS_DRIVER_ONLINE = "IS_DRIVER_ONLINE"
    let Signup = "driver/signup"
    let SignIn =  "driver/login"
    let Verify = "driver/verify_phone"
    let Logout = "driver/logout"

    let GetRide = "driver/get_ride"
    let GetRide_s = "driver/get_rides"
    let GetAcceptAndRejectRide = "driver/ride_status"
    let GetupdateOnlineStatus = "driver/update_online_status"
    let GetStartRide = "driver/start_ride"
    let GetEndRide = "driver/end_ride"
    let GetProfile = "driver/get_profile"
    let GetUpdateProfile = "driver/update_profile"
    let GetRateRide = "driver/rate_ride"
    let GetCancelRide = "driver/cancel_ride"

    let GetContctWebPage = "common/contact"
    let GetPrivacyPolicyWebPage = "common/privacy_policy"
    let GetTermsAndConditionsWebPage = "common/terms_and_conditions"

    let GetChatWebPage = "driver/chat_history"
    let GetNotificationList = "driver/get_notification_list"
    let GetHistoryList = "driver/ride_history"
    let GetReviewList = "driver/get_reviews"

    let GetAllVehicles = "driver/get_all_vehicles"
    let GetAddNewCehicle = "driver/add_vehicle"
    let GetVehicleModels = "driver/get_vehicle_models"
    let GetVehicleMake = "driver/get_vehicle_make"
    let GetUpdateVehicleStatus = "driver/update_vehicle_status"
    let GetUpdateVehicleDocs = "driver/update_vehicle_docs"
    let GetVehicleDocs = "driver/get_vehicle_docs"

//http://intellisensetechnologies.com/smartcab/api/driver/update_vehicle_docs





    


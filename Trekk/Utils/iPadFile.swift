//
//  iPadFile.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 07/01/20.
//  Copyright © 2020 Harjit Singh. All rights reserved.
//

import Foundation
import UIKit
// Device Constants

let IS_IPAD = UI_USER_INTERFACE_IDIOM() == .pad
let IS_RETINA = UIScreen.main.scale == 2.0

let IS_IPHONE = UI_USER_INTERFACE_IDIOM() == .phone

let IS_IPHONE_4 = IS_IPHONE && UIScreen.main.bounds.size.height == 480.0
let IS_IPHONE_5 = IS_IPHONE && UIScreen.main.bounds.size.height == 568.0
let IS_IPHONE_6 = IS_IPHONE && UIScreen.main.bounds.size.height == 667.0
// let IS_IPHONE_6PLUS = IS_IPHONE && UIScreen.main.nativeScale == 3.0
let IS_IPHONE_6_PLUS = IS_IPHONE && UIScreen.main.bounds.size.height == 736.0

// Iphone X & XS should be same
let IS_IPHONE_X = IS_IPHONE && UIScreen.main.bounds.size.height == 812.0
let IS_IPHONE_XS = IS_IPHONE && UIScreen.main.bounds.size.height == 812.0

// Iphone XR & XS_MAX should be same
let IS_IPHONE_XR = IS_IPHONE && UIScreen.main.bounds.size.height == 896.0
let IS_IPHONE_XS_MAX = IS_IPHONE && UIScreen.main.bounds.size.height == 896.0


// MARK: - SCREEN WIDTH
func screenWidth() -> CGFloat {
    
    return UIScreen.main.bounds.size.width
}

// MARK: - SCREEN HEIGHT
func screenHeight() -> CGFloat {
    
    return UIScreen.main.bounds.size.height
}

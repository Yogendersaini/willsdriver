//
//  UIView+extention.swift
//  Trekk
//
//  Created by Harjit Singh Mac on 29/06/20.
//  Copyright © 2020 Harjit Singh Mac. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func dropShadowT(scale: Bool = true, color : UIColor) {
        backgroundColor = UIColor.white
        layer.shadowColor =  color.cgColor //UIColor.lightGray.cgColor
        layer.shadowOpacity = 5
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 20
        layer.cornerRadius = 15
        }
    
    func addBlurToView() {
        var blurEffect: UIBlurEffect!
        if #available(iOS 13.0, *) {
            blurEffect = UIBlurEffect(style: .dark)
        }else {
            blurEffect = UIBlurEffect(style: .light)
            
        }
        
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.bounds
        blurredEffectView.alpha = 0.5
        blurredEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurredEffectView)
    }
    
    func removeBlurFromView() {
        
        for subview in self.subviews {
            if subview is UIVisualEffectView{
                subview.removeFromSuperview()
            }
        }
    }
    
    
    
    @IBInspectable
    public var cornerRadius: CGFloat
    {
        set (radius) {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = radius > 0
        }
        
        get {
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat
    {
        set (borderWidth) {
            self.layer.borderWidth = borderWidth
        }
        
        get {
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable
    public var borderColor:UIColor?
    {
        set (color) {
            self.layer.borderColor = color?.cgColor
        }
        
        get {
            if let color = self.layer.borderColor
            {
                return UIColor(cgColor: color)
            } else {
                return nil
            }
        }
    }
    
    
}
extension UIFont {
    
    static func robotoLight(size: CGFloat) -> UIFont {
        let cfURL = Bundle.main.url(forResource: "Roboto-Light", withExtension: "ttf")! as CFURL
        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
        return UIFont(name: "Roboto-Light", size: size)!
    }
    
    static func robotoRegular(size: CGFloat) -> UIFont {
        let cfURL = Bundle.main.url(forResource: "Roboto-Regular", withExtension: "ttf")! as CFURL
        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
        return UIFont(name: "Roboto-Regular", size: size)!
    }
    
    static func robotoMedium(size: CGFloat) -> UIFont {
        let cfURL = Bundle.main.url(forResource: "Roboto-Medium", withExtension: "ttf")! as CFURL
        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
        return UIFont(name: "Roboto-Medium", size: size)!
    }
    
    static func robotoBold(size: CGFloat) -> UIFont {
        let cfURL = Bundle.main.url(forResource: "Roboto-Bold", withExtension: "ttf")! as CFURL
        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
        return UIFont(name: "Roboto-Bold", size: size)!
    }
    
    static func robotoBlack(size: CGFloat) -> UIFont {
        let cfURL = Bundle.main.url(forResource: "Roboto-Black", withExtension: "ttf")! as CFURL
        CTFontManagerRegisterFontsForURL(cfURL, CTFontManagerScope.process, nil)
        return UIFont(name: "Roboto-Black", size: size)!
    }
}



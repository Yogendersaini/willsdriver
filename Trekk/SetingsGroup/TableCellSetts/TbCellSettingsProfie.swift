//
//  TbCellSettingsProfie.swift
//  Trekk
//
//  Created by Yogender Saini on 12/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TbCellSettingsProfie: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData() {
        self.lblTitle.text = PROFILE_DATA_GLOBEL.full_name
        self.imgProfile.sd_setImage(with: URL(string: PROFILE_DATA_GLOBEL.image), placeholderImage: UIImage(named: "profile_pic"))
    }
    
}

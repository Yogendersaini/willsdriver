//
//  ProfileSettingVC.swift
//  Trekk
//
//  Created by Yogender Saini on 13/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class ProfileSettingVC: UIViewController {
    
    @IBOutlet weak var tblHistory : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableviewCells()
    }
    
    static func getInstance() -> ProfileSettingVC {
        let storyboard = UIStoryboard(name: "SettingsGroup",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ProfileSettingVC") as! ProfileSettingVC
        
    }
     
    @IBAction func backBtnActinos(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func moveTopickupActinos(_ sender: UIButton) {
        let wVC = PickupViewController.getInstance()
        self.navigationController?.pushViewController(wVC, animated: true)
    }
    
}

    extension ProfileSettingVC : UITableViewDelegate, UITableViewDataSource {
        
        func registerTableviewCells(){
            
            tblHistory.register(UINib(nibName: "NotesTblCell", bundle: nil), forCellReuseIdentifier: "NotesTblCell")
            tblHistory.register(UINib(nibName: "HistoryDetailsTbCell", bundle: nil), forCellReuseIdentifier: "HistoryDetailsTbCell")
            tblHistory.register(UINib(nibName: "TripFairTbCell", bundle: nil), forCellReuseIdentifier: "TripFairTbCell")
            tblHistory.register(UINib(nibName: "TbCellCalling", bundle: nil), forCellReuseIdentifier: "TbCellCalling")

            tblHistory.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)

        }
        
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                return 4
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            if indexPath.row == 0 {
                let historyDetailsTbCell = tblHistory.dequeueReusableCell(withIdentifier: "HistoryDetailsTbCell", for: indexPath) as! HistoryDetailsTbCell
                historyDetailsTbCell.selectionStyle = .none
                historyDetailsTbCell.setuViews(viewCont: self)
                historyDetailsTbCell.viewBackg.layer.backgroundColor = UIColor.white.cgColor
                historyDetailsTbCell.viewBackg1.layer.backgroundColor = UIColor.white.cgColor
                historyDetailsTbCell.viewBackg2.layer.backgroundColor = UIColor.white.cgColor
                return historyDetailsTbCell

            } else  if indexPath.row == 1 {
                let notesTblCell = tblHistory.dequeueReusableCell(withIdentifier: "NotesTblCell", for: indexPath) as! NotesTblCell
                notesTblCell.selectionStyle = .none
                notesTblCell.viewBackg.layer.backgroundColor = UIColor.white.cgColor
                return notesTblCell
                
            } else if indexPath.row == 2 {
                let tripFairTbCell = tblHistory.dequeueReusableCell(withIdentifier: "TripFairTbCell", for: indexPath) as! TripFairTbCell
                tripFairTbCell.selectionStyle = .none
                tripFairTbCell.viewBackg.layer.backgroundColor = UIColor.white.cgColor

                return tripFairTbCell
            } else {
                let tbCellCalling = tblHistory.dequeueReusableCell(withIdentifier: "TbCellCalling", for: indexPath) as! TbCellCalling
                tbCellCalling.selectionStyle = .none
                return tbCellCalling
            }
            
        }

        func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableView.automaticDimension
        }
    }


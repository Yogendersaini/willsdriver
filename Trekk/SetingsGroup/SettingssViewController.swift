//
//  SettingssViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 12/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class SettingssViewController: UIViewController {

    @IBOutlet weak var tblSettings : UITableView!
    @IBOutlet weak var btnBack: UIButton!



    
    let arrTitles = [ (L102Language.AMLocalizedString(key: "Vehicle_Management", value: "")), (L102Language.AMLocalizedString(key: "Document_Management", value: "")), (L102Language.AMLocalizedString(key: "Reviews", value: "")), (L102Language.AMLocalizedString(key: "Language", value: "")), "", (L102Language.AMLocalizedString(key: "Notifications", value: "")), (L102Language.AMLocalizedString(key: "Terms_Privacy_Policy", value: "")), (L102Language.AMLocalizedString(key: "Contact_us", value: ""))]
    
    
    let arrTitlesImages = ["vehicle", "document", "reviews", "language", "", "notification", "terms", "contact"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnBack.setTitle((L102Language.AMLocalizedString(key: "Settings_MenuKey", value: "")), for: .normal)
        self.registerTableviewCells()
    }

    
    static func getInstance() -> SettingssViewController {
        let storyboard = UIStoryboard(name: "SettingsGroup",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SettingssViewController") as! SettingssViewController
        
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

}

extension SettingssViewController : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        
        tblSettings.register(UINib(nibName: "TbCellSettingsProfie", bundle: nil), forCellReuseIdentifier: "TbCellSettingsProfie")
        tblSettings.register(UINib(nibName: "TbCellSettingsOption", bundle: nil), forCellReuseIdentifier: "TbCellSettingsOption")
        tblSettings.register(UINib(nibName: "TbCellHeader", bundle: nil), forCellReuseIdentifier: "TbCellHeader")
        tblSettings.delegate = self
        tblSettings.dataSource = self
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 11
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 1 {
            let histHeaderTbCell = tblSettings.dequeueReusableCell(withIdentifier: "TbCellSettingsProfie", for: indexPath) as! TbCellSettingsProfie
            histHeaderTbCell.selectionStyle = .none
            return histHeaderTbCell
        }
        if indexPath.row == 0 || indexPath.row == 2 || indexPath.row ==  7{
            let tbCellHeader = tblSettings.dequeueReusableCell(withIdentifier: "TbCellHeader", for: indexPath) as! TbCellHeader
            tbCellHeader.selectionStyle = .none
            return tbCellHeader
        }
            let historyDetailsTbCell = tblSettings.dequeueReusableCell(withIdentifier: "TbCellSettingsOption", for: indexPath) as! TbCellSettingsOption
            historyDetailsTbCell.selectionStyle = .none
            historyDetailsTbCell.lblTitle.text = arrTitles[indexPath.row - 3]
            historyDetailsTbCell.imgTitle.image = UIImage(named: self.arrTitlesImages[indexPath.row-3])
            return historyDetailsTbCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
//            let wVC = ProfileSettingVC.getInstance()
//            self.navigationController?.pushViewController(wVC, animated: true)
            let pVC = ProfileViewController.getInstance()
            self.navigationController?.pushViewController(pVC, animated: true)

        }
        if indexPath.row == 3 {
            let wVC = VehicleMangViewController.getInstance()
            self.navigationController?.pushViewController(wVC, animated: true)
        }
        if indexPath.row == 4 {
            let wVC = UploadDocViewController.getInstance()
            self.navigationController?.pushViewController(wVC, animated: true)
        }
        if indexPath.row == 5 {
            let wVC = ReviewsViewController.getInstance()
            self.navigationController?.pushViewController(wVC, animated: true)
        }
        if indexPath.row == 6 {
            let wVC = LanguageViewController.getInstance()
            self.navigationController?.pushViewController(wVC, animated: true)
        }

        if indexPath.row == 8 {
            let wVC = NotificationViewCont.getInstance()
            self.navigationController?.pushViewController(wVC, animated: true)
        }
        if indexPath.row == 9 {
            let wVC = AboutViewController.getInstance()
            wVC.btnBackTitle = (L102Language.AMLocalizedString(key: "Terms_Privacy_Policy", value: ""))
            wVC.index = 1
            //            wVC.rider_ID = self.objAcceptModle.rider_id
            self.navigationController?.pushViewController(wVC, animated: true)
        }
        if indexPath.row == 10 {
            let wVC = AboutViewController.getInstance()
            wVC.btnBackTitle = (L102Language.AMLocalizedString(key: "Contact_us", value: ""))
            wVC.index = 2
//            wVC.rider_ID = self.objAcceptModle.rider_id
            self.navigationController?.pushViewController(wVC, animated: true)

        }

        
    }
    
}


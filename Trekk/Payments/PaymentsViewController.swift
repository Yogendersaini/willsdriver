//
//  PaymentsViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 2/24/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class PaymentsViewController: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func getInstance() -> PaymentsViewController {
        let storyboard = UIStoryboard(name: "Payments",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PaymentsViewController") as! PaymentsViewController
        
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
         }
       

    
}

//
//  VehicleMangViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 16/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit
import SDWebImage
import AssetsLibrary
import Photos
import AKImageCropperView


class VehicleMangViewController: UIViewController {

    @IBOutlet weak var tblInvite : UITableView!
    @IBOutlet weak var lblNodata : UILabel!
    @IBOutlet weak var btnBack: UIButton!

    var arrSelectedRows = [Int]()
    var arrVehicleDetailsData = [[String:Any]]()
    var pageNO = 1
    var objVehicelDataModel = [VehicleDataModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableviewCells()
        
        self.btnBack.setTitle((L102Language.AMLocalizedString(key: "Vehicle_Management", value: "")), for: .normal)
        self.lblNodata.text = (L102Language.AMLocalizedString(key: "No_Vehicle_Found", value: ""))

    }
    override func viewWillAppear(_ animated: Bool) {
        self.apiCalling_GetAllVehicles(pageNO: "\(pageNO)")
    }

    static func getInstance() -> VehicleMangViewController {
        let storyboard = UIStoryboard(name: "VehicleMang",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "VehicleMangViewController") as! VehicleMangViewController
    }

    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAddActinos(_ sender: UIButton) {
        let vc = AddNewVechileVC.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)

    }

}

extension VehicleMangViewController : UITableViewDelegate, UITableViewDataSource {

    func registerTableviewCells(){
        tblInvite.register(UINib(nibName: "TbCellVehicle", bundle: nil), forCellReuseIdentifier: "TbCellVehicle")
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objVehicelDataModel.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let histHeaderTbCell = tblInvite.dequeueReusableCell(withIdentifier: "TbCellVehicle", for: indexPath) as! TbCellVehicle
            histHeaderTbCell.selectionStyle = .none
        histHeaderTbCell.setupData(obj: self.objVehicelDataModel[indexPath.row])

//        if self.arrSelectedRows.contains(indexPath.row) {
//            histHeaderTbCell.btnSelectAction(val: true)
//        } else {
//            histHeaderTbCell.btnSelectAction(val: false)
//        }
            return histHeaderTbCell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let status = self.objVehicelDataModel[indexPath.row].is_activated
        let newStatsu = (status == "1") ? "0" : "1"
        self.apiCalling_GetUpdateVehicleStatus(status: newStatsu, vehicle_id: self.objVehicelDataModel[indexPath.row].id)
        
    }
}

extension VehicleMangViewController {
    // MARK:- Api work
    
    @objc func apiCalling_GetAllVehicles(pageNO : String) {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "page" :  pageNO, "rows" : "10"  ]
                        
        AppHelper.apiCallingForArray(apiName: GetAllVehicles, param: param, viewCont: self, successArray: { (successArray, jsonDict) in
            print(successArray)
            
            self.objVehicelDataModel = [VehicleDataModel]()

            if successArray.count >= 1{
            for i in 0...successArray.count-1 {
                self.objVehicelDataModel.append(VehicleDataModel(with: successArray[i]))
            }
            }
            if self.objVehicelDataModel.count >= 1 {
                self.lblNodata.isHidden=true
            } else {
                self.lblNodata.isHidden=false
            }
            self.tblInvite.reloadData()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
//            self.lblNodata.isHidden=false
        }, messageID: { (messageID, message) in
//            self.lblNodata.isHidden=false
            UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
//            self.lblNodata.isHidden=false
        }
        
    }
    
    
    @objc func apiCalling_GetUpdateVehicleStatus(status : String, vehicle_id : String) {
            
        let param  = ["client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "vehicle_id" :  vehicle_id, "status" : status  ] as [String : Any]

        
        AppHelper.apiCallingForDict(apiName: GetUpdateVehicleStatus, param: param, viewCont: self, successDict: { (successDict, resultDict) in

            self.view.makeToast("Status updated successfully")
            self.objVehicelDataModel = [VehicleDataModel]()
            self.apiCalling_GetAllVehicles(pageNO: "1")
                
            }, fail: { (failErr) in
                print("Login Error:\(failErr.localizedDescription)")
            }, messageID: { (messageID, message) in
                UIAlertController.showAlert(vc: self, title: "", message: message)
            }) { (alertMsg) in
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
            }
            
        }
        
    
    
    
    
        
}


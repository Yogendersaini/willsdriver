//
//  TbCellVehicle.swift
//  Trekk
//
//  Created by Yogender Saini on 16/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TbCellVehicle: UITableViewCell {

    @IBOutlet weak var imgVw : UIImageView!
    @IBOutlet weak var lblNAmes : UILabel!
    @IBOutlet weak var lblColor : UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func btnSelectAction (val : Bool){
        if val {
            imgVw.image = UIImage(named: "radio_btn")
        } else {
            imgVw.image = UIImage(named: "Ellipse 30")
        }
    }
    
    func setupData(obj : VehicleDataModel) {
        self.imgVw.image = UIImage(named: "transaction")
        self.lblNAmes.text = obj.make_name
        self.lblColor.text = obj.license_plate_no
        
        print(obj.is_activated)
        if obj.is_activated == "1" {
            imgVw.image = UIImage(named: "radio_btn")
        } else {
            imgVw.image = UIImage(named: "Ellipse 30")
        }
    }
    
}

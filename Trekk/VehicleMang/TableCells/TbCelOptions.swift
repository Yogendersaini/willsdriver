//
//  TbCelOptions.swift
//  Trekk
//
//  Created by Yogender Saini on 12/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TbCelOptions: UITableViewCell {

    @IBOutlet weak var lblOptinoName : UILabel!
    @IBOutlet weak var lblColor : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
}

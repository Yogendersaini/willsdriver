//
//  TbCellAddVechile.swift
//  Trekk
//
//  Created by Yogender Saini on 16/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TbCellAddVechile: UITableViewCell {

    @IBOutlet weak var lblTtile : UILabel!
    @IBOutlet weak var txtVAlue : UITextField!
    @IBOutlet weak var imgArrow : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(arrTitle : [String], arrValues : [String], index : Int) {
        
        self.lblTtile.text = arrTitle[index]
        
        if index == 0 || index == 1 || index == 2 || index == 4 {
            self.txtVAlue.isUserInteractionEnabled = false
            self.txtVAlue.text = arrValues[index]
            self.imgArrow.isHidden = false
        } else {
            self.txtVAlue.isUserInteractionEnabled = true
            self.txtVAlue.text = ""
            self.imgArrow.isHidden = true
        }
    }
    
}

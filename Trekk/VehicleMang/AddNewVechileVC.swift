//
//  AddNewVechileVC.swift
//  Trekk
//
//  Created by Yogender Saini on 16/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit
import SDWebImage
import AssetsLibrary
import Photos
import AKImageCropperView


class AddNewVechileVC: UIViewController {

    @IBOutlet weak var tblInvite : UITableView!
    @IBOutlet weak var tblOptions : UITableView!
    @IBOutlet weak var viewTableOptions : UIView!
    @IBOutlet weak var profileImgVw: UIImageView!
    var imgData = NSData()
    

    var objVehicleMakeModel = [VehicleMakeModel]()
    var objVehicleTypeModel = [VehicleTypeModel]()
    var listOpenFor = 0

    let arrColorNames = ["Red", "White", "Cyan", "Silver", "Blue", "Gray or Grey", "DarkBlue", "Black", "LightBlue", "Orange", "Purple", "Brown", "Yellow", "Maroon", "Lime", "Green", "Magenta", "Olive"]
    
    let arrColorCodes =  ["#FF0000", "#FFFFFF", "#00FFFF", "#C0C0C0", "#0000FF", "#808080", "#0000A0", "#000000", "#ADD8E6", "#FFA500", "#800080", "#A52A2A", "#FFFF00", "#800000", "#00FF00", "#008000", "#FF00FF", "#808000"]
    
    let arrTitle = ["VEHICLE BRAND", "MODEL", "YEAR", "LICENSE PLATE", "COLOR", "BOOKING TYPE"]
    
    var arrVAluesForIndexPathRow = ["", "", "", "nil", ""]   // this arr for put values in table view
    var arrVAluesForAPI = ["", "", "", "nil", ""]  // this array for put values in api
    var arrYears = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableviewCells()
        apiCalling_GetVehicleMake()
        self.adYearsToArray()
        self.viewTableOptions.isHidden = true
    }

    static func getInstance() -> AddNewVechileVC {
        let storyboard = UIStoryboard(name: "VehicleMang",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "AddNewVechileVC") as! AddNewVechileVC

    }

    @IBAction func backBtnActinos(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancel_Action(_ sender: UIButton) {
        self.viewTableOptions.isHidden = true
    }
    
    @IBAction func btnSave_Action(_ sender: UIButton) {
        self.viewTableOptions.isHidden = true
    }

    @IBAction func btnComplete_Action(_ sender: UIButton) {
        self.checkValidations()
    }
    
    func checkValidations()  {
        
        if self.imgData.length == 0 {
            self.view.makeToast("Please select a car image.")
            return
        }
        
        var isValue = false
        for i in 0...self.arrVAluesForIndexPathRow.count-1{
            if self.arrVAluesForIndexPathRow[i] == "" {
                isValue = true
                self.view.makeToast("Please fill all values.")
                return
            }
        }
        
        var licPlatNo = ""
        var bookingType = ""
        let cell1 = tblInvite.cellForRow(at: IndexPath(row: 3, section: 0)) as! TbCellAddVechile
        let cell2 = tblInvite.cellForRow(at: IndexPath(row: 5, section: 0)) as! TbCellAddVechile
        
        licPlatNo = cell1.txtVAlue.text as? String ?? ""
        bookingType = cell2.txtVAlue.text as? String ?? ""
        
        if licPlatNo == "" || bookingType == "" {
            self.view.makeToast("Pleae enter licency number or booking type")
            return
        }

        print(bookingType)
        print(licPlatNo)
        
        self.apiCalling_GetAddNewCehicle(vehicle_model_id: arrVAluesForAPI[1], mfg_year: arrVAluesForAPI[2], color: arrVAluesForAPI[4], license_plate_no: licPlatNo, booking_type: bookingType, default_vehicle: "1")

    }

}

extension AddNewVechileVC : UITableViewDelegate, UITableViewDataSource {

    func registerTableviewCells(){
        tblInvite.register(UINib(nibName: "TbCellAddVechile", bundle: nil), forCellReuseIdentifier: "TbCellAddVechile")
        tblOptions.register(UINib(nibName: "TbCelOptions", bundle: nil), forCellReuseIdentifier: "TbCelOptions")
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblInvite {
            return 6
        } else {
     
            if self.listOpenFor == 0 {
                return self.objVehicleMakeModel.count
            } else if self.listOpenFor == 1 {
                return self.objVehicleTypeModel.count
            } else if self.listOpenFor == 2 {
                return self.arrYears.count
            } else {
                return self.arrColorNames.count
            }
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblInvite {
            
            let histHeaderTbCell = tblInvite.dequeueReusableCell(withIdentifier: "TbCellAddVechile", for: indexPath) as! TbCellAddVechile
            histHeaderTbCell.selectionStyle = .none
            histHeaderTbCell.lblTtile.text = self.arrTitle[indexPath.row]
            histHeaderTbCell.setData(arrTitle: self.arrTitle, arrValues: self.arrVAluesForIndexPathRow, index: indexPath.row)
            return histHeaderTbCell
            
        } else {
            // to open option table cells.
                let histHeaderTbCell = tblOptions.dequeueReusableCell(withIdentifier: "TbCelOptions", for: indexPath) as! TbCelOptions
            if self.listOpenFor == 0 {
                histHeaderTbCell.lblOptinoName.text = objVehicleMakeModel[indexPath.row].make_name
                histHeaderTbCell.lblColor.backgroundColor = UIColor.clear
            } else if self.listOpenFor == 1 {
                histHeaderTbCell.lblOptinoName.text = objVehicleTypeModel[indexPath.row].model_name
                histHeaderTbCell.lblColor.backgroundColor = UIColor.clear
            } else if self.listOpenFor == 2 {
                histHeaderTbCell.lblOptinoName.text = self.arrYears[indexPath.row]
                histHeaderTbCell.lblColor.backgroundColor = UIColor.clear
            } else {
                histHeaderTbCell.lblOptinoName.text = self.arrColorNames[indexPath.row]
                histHeaderTbCell.lblColor.backgroundColor = hexStringToUIColor(hex: self.arrColorCodes[indexPath.row])
            }
            return histHeaderTbCell
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblInvite {
            // to show custome view for moderl type and year
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 4{
            self.listOpenFor = indexPath.row
            self.viewTableOptions.isHidden = false
            self.tblOptions.reloadData()
        }
        } else {
            // for table options
            //when tap tble option will hide and selected data set in to array
            var value = ""
            if self.listOpenFor == 0 {
                value = objVehicleMakeModel[indexPath.row].make_name
                self.arrVAluesForAPI.remove(at: self.listOpenFor)
                self.arrVAluesForAPI.insert(objVehicleMakeModel[indexPath.row].id, at: self.listOpenFor)
                self.apiCalling_GetVehicleModels(id: objVehicleMakeModel[indexPath.row].id)
            } else if self.listOpenFor == 1 {
                value = objVehicleTypeModel[indexPath.row].model_name
                self.arrVAluesForAPI.insert(objVehicleTypeModel[indexPath.row].id, at: self.listOpenFor)
            } else if self.listOpenFor == 2 {
                value = self.arrYears[indexPath.row]
                self.arrVAluesForAPI.insert(arrYears[indexPath.row], at: self.listOpenFor)
            } else {
                value = self.arrColorNames[indexPath.row]
                self.arrVAluesForAPI.insert(arrColorCodes[indexPath.row], at: self.listOpenFor)
            }
            self.arrVAluesForIndexPathRow.remove(at: self.listOpenFor)
            self.arrVAluesForIndexPathRow.insert(value, at: self.listOpenFor)

            self.viewTableOptions.isHidden = true
            print(arrVAluesForIndexPathRow)
            self.tblInvite.reloadData()
        }
    }
    
}
extension AddNewVechileVC {
    // MARK:- Api work
    
    @objc func apiCalling_GetVehicleModels(id : String) {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "vehicle_brand_id" : id ]
                        
        AppHelper.apiCallingForArray(apiName: GetVehicleModels, param: param, viewCont: self, successArray: { (successArray, jsonDict) in
            print(successArray)
            self.objVehicleTypeModel.removeAll()
            if successArray.count >= 1{
            for i in 0...successArray.count-1 {
                self.objVehicleTypeModel.append(VehicleTypeModel(with: successArray[i]))
                }
            }
                        
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
        }, messageID: { (messageID, message) in
            UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }

        @objc func apiCalling_GetVehicleMake() {
            
            let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken()]
                            
            AppHelper.apiCallingForArray(apiName: GetVehicleMake, param: param, viewCont: self, successArray: { (successArray, jsonDict) in
                print(successArray)
                if successArray.count >= 1{
                for i in 0...successArray.count-1 {
                    self.objVehicleMakeModel.append(VehicleMakeModel(with: successArray[i]))
                    }
                }
                
            }, fail: { (failErr) in
                print("Login Error:\(failErr.localizedDescription)")
            }, messageID: { (messageID, message) in
                UIAlertController.showAlert(vc: self, title: "", message: message)
            }) { (alertMsg) in
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
            }
            
        }
    
    
    func adYearsToArray()  {
        
        let  str = "\(Date())"
        let substt = (str as! NSString).substring(to: 4)

        for i in 1990...2050 {
            self.arrYears.append("\(i)")
            if String(i) == substt {
                break
            }
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func apiCalling_GetAddNewCehicle(vehicle_model_id  : String, mfg_year  : String, color  : String,  license_plate_no  : String,  booking_type  : String, default_vehicle : String)  {

                
        let params = ["token" : AppHelper.getUserToken(), "client_token" : AppHelper.getClientToken(), "vehicle_model_id" : vehicle_model_id, "mfg_year" : mfg_year, "color" : color, "license_plate_no" : license_plate_no, "booking_type" : booking_type, "default_vehicle" : default_vehicle]
        
        
        AppHelper.apiCallingWithImage(apiName: GetAddNewCehicle, param: params, viewCont: self, imgData: self.imgData, successDict: { (successDict) in
                UIAlertController.showAlertWithOkAction(self, title: "Sign Up", message: "Vehicle added successfully", buttonTitle: "Ok", buttonAction: self.backToRoot)
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            
        }, messageID: { (messageID, message) in
            if messageID == "157" {
                
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "incorrect_username_password", value: "")))
                
            } else if messageID == "129" {
                UIAlertController.showAlert(vc: self, title: "", message:(L102Language.AMLocalizedString(key: "verify_email_first", value: "")))
                
            } else {
                
                UIAlertController.showAlert(vc: self, title: "", message: message)
                
            }
            
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
        }
        
    }
    
    @objc func backToRoot() {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension AddNewVechileVC  : UIImagePickerControllerDelegate, UINavigationControllerDelegate, cropImageCustomDelegats {
    // MARK:- GetImage

     @IBAction func onTapProfileBtn(_ sender: UIButton) {
            isForImageBool = true
            self.checkCameraPermissions()
        }
        
        func checkCameraPermissions () {
            
            var alertController = UIAlertController()
            
            let alertTitle = (L102Language.AMLocalizedString(key: "choose_image", value: ""))
            
            if IS_IPAD {
                alertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
            }
            else {
                alertController = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
            }
            
            alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "camera", value: "")), style: .default, handler: { (action:UIAlertAction) in
                
                let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
                
                switch (status) {
                    
                case .authorized:
                    
                    self.camera()
                    
                case .notDetermined:
                    
                    AVCaptureDevice.requestAccess(for: AVMediaType.video) { (granted) in
                        
                        if (granted) {
                            
                            self.camera()
                            
                        } else {
                            
                            self.cameraDenied()
                        }
                    }
                    
                case .denied:
                    
                    self.cameraDenied()
                    
                case .restricted:
                    
                    let alert = UIAlertController(title: "Camera Services Restricted",
                                                  message: "You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.",
                                                  preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction (title: BUTTON_TITLE, style: .default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                @unknown default:
                    fatalError()
                }
            }))
            
            alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "photo_library", value: "")), style: .default, handler: { (action:UIAlertAction) in
                self .photoLibrary()
            }))
            
            alertController.addAction(UIAlertAction (title: (L102Language.AMLocalizedString(key: "cancel", value: "    ")), style: .destructive, handler: { (action:UIAlertAction) in
                
            }))
            
            self.present(alertController, animated: true, completion: nil)
        }
        
        func cameraDenied() {
             
            DispatchQueue.main.async {
                var alertText = "Turn on Camera Services in Settings > Privacy to allow Camera to take pictures"
                
                var alertButton = "Ok"
                var goAction = UIAlertAction(title: alertButton, style: .default, handler: nil)
                
                if UIApplication.shared.canOpenURL(URL(string: UIApplication.openSettingsURLString)!)
                {
                    alertText = "Turn on Camera Services in Settings > Privacy to allow Camera to take pictures"
                    
                    alertButton = "Settings"
                    
                    goAction = UIAlertAction(title: alertButton, style: .default, handler: {(alert: UIAlertAction!) -> Void in
                        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
                    })
                }
                
                let alert = UIAlertController(title: "Camera Services Off", message: alertText, preferredStyle: .alert)
                alert.addAction(goAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        func camera() {
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                DispatchQueue.main.async {
                    
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.delegate = self;
                    imagePickerController.sourceType = .camera
                    imagePickerController.modalPresentationStyle = .fullScreen
                    self.present(imagePickerController, animated: true, completion: nil)

                }
                
            } else {
                
                UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: "Camera not available !")
            }
        }
        
        func photoLibrary() {
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                DispatchQueue.main.async {
                    
                    let imagePickerController = UIImagePickerController()
                    imagePickerController.delegate = self;
                    imagePickerController.sourceType = .photoLibrary
                    
                    let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.blue]
                    UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
                    
                    imagePickerController.modalPresentationStyle = .fullScreen
                    self.present(imagePickerController, animated: true, completion: nil)

                }
            }
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

            if (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil {
                
                let chosenImage: UIImage? = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
                let data: NSData? = chosenImage!.jpegData(compressionQuality: 0.2)! as NSData
                
                 profileImgVw.image = chosenImage
    
                
                    imgData = data!
                
                let cropperViewController = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "CropperViewController") as! CropperViewController
                cropperViewController.image = chosenImage
                cropperViewController.delegate = self
                self.navigationController?.pushViewController(cropperViewController, animated: true)
                picker.dismiss(animated: true, completion: nil)
                    

                  
            } else {
                
                print("BEGIN:YPRRA BUSINESS ======> Something went wrong")
            }
            
           // self.dismiss(animated: true, completion: nil)
            
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            
            let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
            
            self.dismiss(animated: true, completion: nil)
        }
        
        func passImageAfterCrop(img: UIImage) {
            profileImgVw.image = img
        }
    
}

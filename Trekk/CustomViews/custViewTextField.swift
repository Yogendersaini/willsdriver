//
//  custViewTextField.swift
//  WillsSmartCabRider
//
//  Created by Yogender Saini on 2/17/21.
//  Copyright © 2021 Yogender Saini. All rights reserved.
//

import UIKit

class custViewTextField: UIView {
    
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblErrMsg: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var btnShowHIde: UIButton!
    @IBOutlet weak var viewBorder: UIView!

    let nibName = "custViewTextField"
    var contentView: UIView?
    
    @IBAction func buttonTap(_ sender: UIButton) {
        if textField.isSecureTextEntry {
            textField.isSecureTextEntry = false
            btnShowHIde.setImage(UIImage(named: "password-show"), for: .normal)
            

        } else {
            textField.isSecureTextEntry = true
            btnShowHIde.setImage(UIImage(named: "password-hide"), for: .normal)
            

        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view

    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
    }

    func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func borderGreen() {
        self.viewBorder.layer.borderColor = UIColor.green.cgColor
        self.lblErrMsg.text = ""
    }
    
    func borderRed() {
        self.viewBorder.layer.borderColor = UIColor.red.cgColor
    }
    

}

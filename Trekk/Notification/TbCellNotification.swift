//
//  TbCellNotification.swift
//  Trekk
//
//  Created by Yogender Saini on 06/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TbCellNotification: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setupData(obj : NotificationDataModel)  {
        self.lblTitle.text = obj.title
        self.lblMessage.text = obj.message
        self.lblDate.text = obj.created_at
        var imgName = ""
        switch obj.type {
        case "ride_request":
            imgName = "promotion"
        case "ride_accepted":
            imgName = "system"
        case "ride_completed":
            imgName = "transaction"
        case "ride_cancelled":
            imgName = "cross"
        default:
            imgName = "cross"
        }
        self.imgProfile.image = UIImage(named: imgName)
    }
    
}

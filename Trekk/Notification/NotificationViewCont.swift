//
//  NotificationViewCont.swift
//  Trekk
//
//  Created by Yogender Saini on 06/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class NotificationViewCont: UIViewController {

    @IBOutlet weak var tblWallet : UITableView!
    @IBOutlet weak var lblNodata : UILabel!
    var objNotifDM = [NotificationDataModel]()
    
    @IBOutlet weak var btnBack: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableviewCells()
        self.lblNodata.isHidden=true
        self.loclizatino()
        self.apiCalling_GetNotificationList()
    }
    
    func loclizatino()  {
        self.btnBack.setTitle((L102Language.AMLocalizedString(key: "Notifications_MenuKey", value: "")), for: .normal)
        self.lblNodata.text = (L102Language.AMLocalizedString(key: "No_Ride_Found", value: ""))
    }
    
    
    static func getInstance() -> NotificationViewCont {
        let storyboard = UIStoryboard(name: "Notification",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "NotificationViewCont") as! NotificationViewCont
        
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
         }
    
}
extension NotificationViewCont : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        tblWallet.register(UINib(nibName: "TbCellNotification", bundle: nil), forCellReuseIdentifier: "TbCellNotification")
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objNotifDM.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tbCellWalletHeader = tblWallet.dequeueReusableCell(withIdentifier: "TbCellNotification", for: indexPath) as! TbCellNotification
        tbCellWalletHeader.selectionStyle = .none
        tbCellWalletHeader.setupData(obj: self.objNotifDM[indexPath.row])
        return tbCellWalletHeader
        
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension NotificationViewCont {
    // MARK:- Api work
    
    @objc func apiCalling_GetNotificationList() {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken()]
                        
        AppHelper.apiCallingForArray(apiName: GetNotificationList, param: param, viewCont: self, successArray: { (successArray, jsonDict) in
            print(successArray)
            self.objNotifDM.removeAll()
            for i in 0...successArray.count-1 {
                self.objNotifDM.append(NotificationDataModel(with: successArray[i]))
                self.lblNodata.isHidden=true
            }
            
            if successArray.count >= 1 {
                self.lblNodata.isHidden=true
            } else {
                self.lblNodata.isHidden=false
            }
            self.tblWallet.reloadData()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
            self.lblNodata.isHidden=false
        }, messageID: { (messageID, message) in
            self.lblNodata.isHidden=false
//            UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
//            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
            self.lblNodata.isHidden=false
        }
        
    }
        

}




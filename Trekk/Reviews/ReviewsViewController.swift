//
//  ReviewsViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 17/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class ReviewsViewController: UIViewController {

    @IBOutlet weak var lblNodata : UILabel!
    @IBOutlet weak var tblReview : UITableView!
    @IBOutlet weak var btnBack: UIButton!
    
    var objReviewData = [ReviewDataMOdel]()
    var pageN0 = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNodata.isHidden=true
        self.registerTableviewCells()
        self.apiCalling_GetReviewList(pageNO: "\(self.pageN0)")
        self.btnBack.setTitle((L102Language.AMLocalizedString(key: "Reviews", value: "")), for: .normal)
        self.lblNodata.text = (L102Language.AMLocalizedString(key: "No_Reviews_Found", value: ""))

    }
    
    static func getInstance() -> ReviewsViewController {
        let storyboard = UIStoryboard(name: "Reviews",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ReviewsViewController") as! ReviewsViewController
    }

    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAddActinos(_ sender: UIButton) {
        let vc = AddNewVechileVC.getInstance()
        self.navigationController?.pushViewController(vc, animated: true)
    }


}

extension ReviewsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        
        tblReview.register(UINib(nibName: "TbCellReview", bundle: nil), forCellReuseIdentifier: "TbCellReview")
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objReviewData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let historyDetailsTbCell = tblReview.dequeueReusableCell(withIdentifier: "TbCellReview", for: indexPath) as! TbCellReview
            historyDetailsTbCell.selectionStyle = .none
        historyDetailsTbCell.setData(obj: self.objReviewData[indexPath.row])

        if (self.objReviewData.count-2) == indexPath.row {
            self.pageN0 = self.pageN0 + 1
            self.apiCalling_GetReviewList(pageNO: "\(self.pageN0)")
        }
        return historyDetailsTbCell
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let wVC = ReceiptViewController.getInstance()
//        self.navigationController?.pushViewController(wVC, animated: true)
    }
}

extension ReviewsViewController {
    // MARK:- Api work
    
    @objc func apiCalling_GetReviewList(pageNO : String) {
        
        let param  = [ "client_token" : AppHelper.getClientToken(), "token" : AppHelper.getUserToken(), "page" :  pageNO, "rows" : "10"  ]
                        
        AppHelper.apiCallingForArray(apiName: GetReviewList, param: param, viewCont: self, successArray: { (successArray, jsonDict) in
            print(successArray)
//            self.objReviewData.removeAll()
            if successArray.count >= 1{
            for i in 0...successArray.count-1 {
                self.objReviewData.append(ReviewDataMOdel(with: successArray[i]))
            }
            }
            if self.objReviewData.count >= 1 {
                self.lblNodata.isHidden=true
            } else {
                self.lblNodata.isHidden=false
            }
            self.tblReview.reloadData()
            
        }, fail: { (failErr) in
            print("Login Error:\(failErr.localizedDescription)")
//            self.lblNodata.isHidden=false
        }, messageID: { (messageID, message) in
//            self.lblNodata.isHidden=false
            UIAlertController.showAlert(vc: self, title: "", message: message)
        }) { (alertMsg) in
            UIAlertController.showAlert(vc: self, title: ALERT_TITLE, message: alertMsg)
//            self.lblNodata.isHidden=false
        }
        
    }
        

}



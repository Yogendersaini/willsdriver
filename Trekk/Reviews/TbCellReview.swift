//
//  TbCellReview.swift
//  Trekk
//
//  Created by Yogender Saini on 11/05/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TbCellReview: UITableViewCell {

    // Rating View // after payment ride
    @IBOutlet weak var imgRating1: UIImageView!
    @IBOutlet weak var imgRating2: UIImageView!
    @IBOutlet weak var imgRating3: UIImageView!
    @IBOutlet weak var imgRating4: UIImageView!
    @IBOutlet weak var imgRating5: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!

    var starRating = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(obj : ReviewDataMOdel) {
        
        self.lblName.text = obj.rider_name
        self.lblDate.text = obj.created_at
        self.imgProfile.sd_setImage(with: URL(string: obj.rider_image), placeholderImage: UIImage(named: "profile_pic"))
        self.starRating = Int(obj.rating)!
        self.rating(star: self.starRating)

    }

    
    func rating(star : Int)  {
        if star == 1 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_-1")
            imgRating3.image = UIImage(named: "ic_star_-1")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
//            starRating = "1"
        }
        else if star == 2 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_-1")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
//            starRating = "2"
        }
        else if star == 3 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_24px")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
//            starRating = "3"
        }
        else if star == 4 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_24px")
            imgRating4.image = UIImage(named: "ic_star_24px")
            imgRating5.image = UIImage(named: "ic_star_-1")
//            starRating = "4"
        }
        else if star == 5 {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_24px")
            imgRating4.image = UIImage(named: "ic_star_24px")
            imgRating5.image = UIImage(named: "ic_star_24px")
//            starRating = "5"
        }
        else  {
            imgRating1.image = UIImage(named: "ic_star_24px")
            imgRating2.image = UIImage(named: "ic_star_24px")
            imgRating3.image = UIImage(named: "ic_star_-1")
            imgRating4.image = UIImage(named: "ic_star_-1")
            imgRating5.image = UIImage(named: "ic_star_-1")
//            starRating = "2"
        }
        
    }
    

    
}

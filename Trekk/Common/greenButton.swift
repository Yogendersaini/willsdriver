//
//  greenButton.swift
//  Trekk
//
//  Created by Yogender Saini on 05/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//


import Foundation
import UIKit

class greenButton: UIButton {
 
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.layer.backgroundColor = UIColor.init(red: 11/277, green: 84/255, blue: 45/255, alpha: 1).cgColor
//            .setBackgroundImage(UIImage(named: "gradient"), for: .normal)
        self.titleLabel?.font = UIFont.robotoBold(size: 12)
        self.tintColor = UIColor.white
        
        
    }
    
    
}

//
//  CommonButton.swift
//  Trekk
//
//  Created by Yogender Saini on 2/22/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import Foundation
import UIKit

class CommonButton: UIButton {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
//        self.layer.cornerRadius = 25
        self.layer.masksToBounds = true
        self.setBackgroundImage(UIImage(named: "gradient"), for: .normal)
        self.titleLabel?.font = UIFont.robotoBold(size: 18)
        self.tintColor = UIColor.white
        
        
    }
    
    
}

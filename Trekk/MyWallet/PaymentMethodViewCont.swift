//
//  PaymentMethodViewCont.swift
//  Trekk
//
//  Created by Yogender Saini on 06/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class PaymentMethodViewCont: UIViewController {

    @IBOutlet weak var tblWallet : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableviewCells()
    }
    
    static func getInstance() -> PaymentMethodViewCont {
        let storyboard = UIStoryboard(name: "MyWallet",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PaymentMethodViewCont") as! PaymentMethodViewCont
        
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
         }
    
}
extension PaymentMethodViewCont : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        tblWallet.register(UINib(nibName: "TbCellCardInfo", bundle: nil), forCellReuseIdentifier: "TbCellCardInfo")
    }
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tbCellWalletHeader = tblWallet.dequeueReusableCell(withIdentifier: "TbCellCardInfo", for: indexPath) as! TbCellCardInfo
        tbCellWalletHeader.selectionStyle = .none
        return tbCellWalletHeader
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}



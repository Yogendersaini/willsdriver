//
//  TblCellPayMethods.swift
//  Trekk
//
//  Created by Yogender Saini on 06/03/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class TblCellPayMethods: UITableViewCell {

    var viewCont = MyWalletViewController()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func btnPaymentMethActinos(_ sender: UIButton) {
        let payMethVC = PaymentMethodViewCont.getInstance()
        self.viewCont.navigationController?.pushViewController(payMethVC, animated: true)
        
    }
    
    
}



//
//  MyWalletViewController.swift
//  Trekk
//
//  Created by Yogender Saini on 2/24/21.
//  Copyright © 2021 Harjit Singh Mac. All rights reserved.
//

import UIKit

class MyWalletViewController: UIViewController {

    @IBOutlet weak var tblWallet : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableviewCells()
    }
    
    static func getInstance() -> MyWalletViewController {
        let storyboard = UIStoryboard(name: "MyWallet",bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MyWalletViewController") as! MyWalletViewController
        
    }
    
    @IBAction func backBtnActinos(_ sender: UIButton) {
             navigationController?.popViewController(animated: true)
         }
    
}
extension MyWalletViewController : UITableViewDelegate, UITableViewDataSource {
    
    func registerTableviewCells(){
        
        tblWallet.register(UINib(nibName: "TbCellWalletHeader", bundle: nil), forCellReuseIdentifier: "TbCellWalletHeader")

        tblWallet.register(UINib(nibName: "TblCellPayMethods", bundle: nil), forCellReuseIdentifier: "TblCellPayMethods")

        tblWallet.register(UINib(nibName: "TbCellPayHistory", bundle: nil), forCellReuseIdentifier: "TbCellPayHistory")

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else  {
            return 4
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let tbCellWalletHeader = tblWallet.dequeueReusableCell(withIdentifier: "TbCellWalletHeader", for: indexPath) as! TbCellWalletHeader
                tbCellWalletHeader.selectionStyle = .none
                return tbCellWalletHeader
            } else {
                let tblCellPayMethods = tblWallet.dequeueReusableCell(withIdentifier: "TblCellPayMethods", for: indexPath) as! TblCellPayMethods
                tblCellPayMethods.selectionStyle = .none
                tblCellPayMethods.viewCont = self
                return tblCellPayMethods
            }
        } else {
            let tbCellPayHistory = tblWallet.dequeueReusableCell(withIdentifier: "TbCellPayHistory", for: indexPath) as! TbCellPayHistory
            tbCellPayHistory.selectionStyle = .none
            return tbCellPayHistory
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}


